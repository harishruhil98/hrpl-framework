package com.hrpl.framwork;


import android.content.Context;

import com.hrpl.android_core.BaseApplicationClass;


public class ApplicationClass extends BaseApplicationClass {
    @Override
    protected boolean isAvailableForProduction() {
        return false;
    }

    @Override
    protected String baseUrl() {
        return "";
    }

    @Override
    public Context getContext() {
        return ApplicationClass.this;
    }


    @Override
    public BaseApplicationClass getInstance() {
        return ApplicationClass.this;
    }

    @Override
    public int getNoDataFoundImage() {
        return 0;
    }

    public static void restartClients(){
        BaseApplicationClass.restartClients();
    }

}
