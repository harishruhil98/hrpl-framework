import com.android.build.gradle.api.ApplicationVariant
import com.android.build.gradle.api.BaseVariantOutput
import com.android.build.gradle.internal.api.ApkVariantOutputImpl
import java.text.SimpleDateFormat
import java.util.Date

plugins {
    id("com.android.application")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
}
android {
    namespace = "com.hrpl.framwork"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.hrpl.framwork"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"
        multiDexEnabled = true

        //buildConfigField("String", "oneSignalKey", project(":app").property("oneSignalKey").toString())

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            buildConfigField("boolean", "isAvailableForProduction", "true")
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }

        debug {
            isMinifyEnabled = true
            buildConfigField("boolean", "isAvailableForProduction", "false")
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
            )
        }
    }

    applicationVariants.all(object : Action<ApplicationVariant> {
        override fun execute(variant: ApplicationVariant) {
            variant.outputs.all(object : Action<BaseVariantOutput> {
                override fun execute(output: BaseVariantOutput) {
                    setVariantOutputName(output, variant)
                }
            })
        }
    })
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    bundle {

        language {
            enableSplit = false
        }
    }
}

fun setVariantOutputName(output: BaseVariantOutput, variant: ApplicationVariant) {

    val outputVariant = (output as ApkVariantOutputImpl)
    val appName = rootProject.name;
    if (outputVariant.outputFileName.endsWith(".apk")) {
        var formattedDate = SimpleDateFormat("yyyyMMddHHmm").format(Date())
        outputVariant.outputFileName =
            appName + "_(" + variant.versionName + ")_" + formattedDate + ".apk"
    }
}

dependencies {
    implementation(project(":android_core"))
    implementation("androidx.annotation:annotation:1.9.1")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.8.7")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.lifecycle:lifecycle-runtime:2.8.7")
    implementation("androidx.lifecycle:lifecycle-compiler:2.8.7")
    implementation("androidx.lifecycle:lifecycle-process:2.8.7")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.8.7")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.2.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.6.1")

    implementation("androidx.legacy:legacy-support-v4:1.0.0")

    // Custom Libraries
    implementation("androidx.multidex:multidex:2.0.1")

    //Network Image Libraries
    implementation("com.github.bumptech.glide:glide:4.13.1")
    annotationProcessor("com.github.bumptech.glide:compiler:4.13.1")
    implementation("com.squareup.picasso:picasso:2.71828")

    //Data Encryption
    implementation("androidx.security:security-crypto:1.1.0-alpha06")

    //Design Libraries
    implementation("androidx.appcompat:appcompat:1.7.0")
    implementation("com.intuit.sdp:sdp-android:1.0.6")
    implementation("androidx.constraintlayout:constraintlayout:2.2.0")
    implementation("com.google.android.material:material:1.12.0")
    implementation("androidx.cardview:cardview:1.0.0")
    implementation("com.tbuonomo:dotsindicator:5.0")
    implementation("androidx.viewpager2:viewpager2:1.1.0")
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")

    //Export Excel
    //implementation("com.github.elirehema:worksheet:0.0.1")

    //Location
    implementation("com.google.android.gms:play-services-location:18.0.0")
    //SMS Auto Read
    implementation("com.google.android.gms:play-services-auth:19.0.0")
    implementation("com.google.android.gms:play-services-auth-api-phone:17.5.1")

    //room
    annotationProcessor("android.arch.persistence.room:compiler:1.1.1")
    implementation("android.arch.persistence.room:runtime:1.1.1")
    annotationProcessor("android.arch.persistence.room:compiler:1.1.1")
}
