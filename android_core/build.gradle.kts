import com.android.build.gradle.api.ApplicationVariant
import com.android.build.gradle.api.BaseVariantOutput
import com.android.build.gradle.internal.api.ApkVariantOutputImpl
import java.text.SimpleDateFormat
import java.util.Date

plugins {
    id("com.android.library")
}
android {
    namespace = "com.hrpl.android_core"
    compileSdk = 34

    defaultConfig {
        minSdk = 24
        targetSdk = 34
        multiDexEnabled = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        // Specify proguard rules files
        consumerProguardFiles("consumer-rules.pro", "proguard-rules.pro")

        // Set the base name for archives
        setProperty("archivesBaseName", "android_core")

        // Enable vector drawables support
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

fun setVariantOutputName(output: BaseVariantOutput, variant: ApplicationVariant) {

    val outputVariant = (output as ApkVariantOutputImpl)
    val appName = rootProject.name;
    if (outputVariant.outputFileName.endsWith(".apk")) {
        var formattedDate = SimpleDateFormat("yyyyMMddHHmm").format(Date())
        outputVariant.outputFileName =
            appName + "_(" + variant.versionName + ")_" + formattedDate + ".apk"
    }
}

dependencies {

    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.2.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.6.1")
    implementation("androidx.annotation:annotation:1.9.1")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.8.7")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.lifecycle:lifecycle-runtime:2.8.7")
    //noinspection LifecycleAnnotationProcessorWithJava8
    implementation("androidx.lifecycle:lifecycle-compiler:2.8.7")
    implementation("androidx.lifecycle:lifecycle-process:2.8.7")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.8.7")

    

    // Custom Libraries
    implementation("androidx.multidex:multidex:2.0.1")

    //Network Image Libraries
    implementation("com.github.bumptech.glide:glide:4.13.1")
    annotationProcessor("com.github.bumptech.glide:compiler:4.13.1")
    implementation("com.squareup.picasso:picasso:2.71828")

    //Network Libraries
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
    implementation("com.google.code.gson:gson:2.10.1")
    implementation("com.squareup.okhttp3:okhttp:5.0.0-alpha.7")
    implementation("com.squareup.okhttp3:logging-interceptor:5.0.0-alpha.7")
    implementation("com.squareup.okhttp3:okhttp-urlconnection:5.0.0-alpha.7")

    //Data Encryption
    implementation("androidx.security:security-crypto:1.1.0-alpha06")

    //Design Libraries
    implementation("androidx.appcompat:appcompat:1.7.0")
    implementation("com.intuit.sdp:sdp-android:1.0.6")
    implementation("androidx.constraintlayout:constraintlayout:2.2.0")
    implementation("com.google.android.material:material:1.12.0")
    implementation("androidx.cardview:cardview:1.0.0")
    implementation("androidx.viewpager2:viewpager2:1.1.0")
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")

    //Location
    implementation("com.google.android.gms:play-services-location:21.2.0")
    //SMS Auto Read
    implementation("com.google.android.gms:play-services-auth:21.1.1")
    implementation("com.google.android.gms:play-services-auth-api-phone:18.0.2")

    //Export Excel
//    implementation("com.github.elirehema:worksheet:0.0.1")

    //room
    annotationProcessor("android.arch.persistence.room:compiler:1.1.1")
    implementation("android.arch.persistence.room:runtime:1.1.1")
    annotationProcessor("android.arch.persistence.room:compiler:1.1.1")
}
