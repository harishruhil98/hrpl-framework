package com.hrpl.android_core.ShowCaseView;


public interface IDetachedListener {
    void onShowcaseDetached(MaterialShowcaseView showcaseView, boolean wasDismissed, boolean wasSkipped);
}
