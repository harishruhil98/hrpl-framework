package com.hrpl.android_core.Services;

import android.app.Activity;
import android.content.Context;

import com.google.gson.Gson;
import com.hrpl.android_core.BaseApplicationClass;
import com.hrpl.android_core.CustomViews.CustomAlertDialog;
import com.hrpl.android_core.Models.DataModel;
import com.hrpl.android_core.NetworkCalls.NetworkReceiver;
import com.hrpl.android_core.Utility.UtilityClass;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseApiServices {

    private static BaseApiServices apiCallServices;

    public static synchronized BaseApiServices getInstance() {
        if (apiCallServices == null) {
            apiCallServices = new BaseApiServices();
        }

        return apiCallServices;
    }

    public <T> void callApi(Context activity, Call<DataModel> call, int requestCode, CallListener callListener, onSuccessListener<T> onSuccessListener) {
        if (NetworkReceiver.isConnected(activity)) {
            call.enqueue(new Callback<DataModel>() {
                @Override
                public void onResponse(@NotNull Call<DataModel> call, @NotNull Response<DataModel> dataModel) {
                    if (dataModel.isSuccessful()) {
                        String header = dataModel.raw().request().headers().toString();
                        String url = dataModel.raw().request().url().toString();
                        RequestBody requestBody = dataModel.raw().request().body();

                        if (dataModel.body() == null || dataModel.body().getResponseMsgModel() == null || dataModel.body().getResponseData() == null) {
                            callListener.onHttpErrorCode(requestCode, ResponseCode.UNSUPPORTED_MEDIA_TYPE, "Unsupported media type");
                        } else if (dataModel.body().getResponseMsgModel().isError()) {
                            callListener.onError(requestCode, UtilityClass.getInstance().nullChecker(dataModel.body().getResponseMsgModel().getErrorMessage()), false);
                        } else if (dataModel.body().getResponseMsgModel().isWarning()) {
                            callListener.onError(requestCode, UtilityClass.getInstance().nullChecker(dataModel.body().getResponseMsgModel().getWarningMessage()), true);
                        } else if (dataModel.body().isDoLogOut()) {
                            onSuccessListener.onDoLogout(requestCode);
                        } else if (dataModel.body().getResponseMsgModel().isEmptyCollection()) {
                            onSuccessListener.isEmptyCollection(requestCode);
                        } else if (dataModel.code() == 200 && !dataModel.body().getResponseMsgModel().isError() && !dataModel.body().getResponseMsgModel().isWarning()) {

                            Type typeClass = null;

                            Type[] genericInterfaces = onSuccessListener.getClass().getGenericInterfaces();
                            for (Type genericInterface : genericInterfaces) {
                                if (genericInterface instanceof ParameterizedType) {
                                    Type[] genericTypes = ((ParameterizedType) genericInterface).getActualTypeArguments();
                                    for (Type genericType : genericTypes) {
                                        typeClass = genericType;
                                    }
                                }
                            }
                            if (typeClass == null) {
                                callListener.onError(requestCode, "Casting error", false);
                            } else {
                                onSuccessListener.onSuccess(requestCode, new Gson().fromJson(new Gson().toJson(dataModel.body().getResponseData().get("data")), typeClass));
                            }
                        }
                    } else if (dataModel.code() == 404) {
                        callListener.apiNotFound(requestCode);
                    } else if (dataModel.code() == 500) {
                        callListener.connectionBroken(requestCode);
                    } else if (dataModel.code() == 401) {
                        callListener.unauthorizedAccess(requestCode);
                    } else if (dataModel.code() == 405) {
                        callListener.methodNotAllowed(requestCode);
                    } else if (dataModel.code() == 408) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.REQUEST_TIMEOUT, "Request Time out error");
                    } else if (dataModel.code() == 409) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.CONFLICT_IN_REQUEST_BODY, "Conflict in requested body or header or query params.");
                    } else if (dataModel.code() == 413) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.PAYLOAD_TOO_LARGE, "Body is too large");
                    } else if (dataModel.code() == 415) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.UNSUPPORTED_MEDIA_TYPE, "Unsupported media type");
                    } else if (dataModel.code() == 502) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.BAD_GATEWAY, "Bad gateway");
                    } else {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.OTHER_ERROR, "Http error code : " + dataModel.code());
                    }
                }

                @Override
                public void onFailure(Call<DataModel> call, Throwable t) {
                    callListener.onError(requestCode, t.getLocalizedMessage(), false);
                }
            });
        } else {
            callListener.onNoConnectivity(requestCode);
        }
    }

    public <T> void callApiWithMessage(Context activity, Call<DataModel> call, int requestCode, CallListener callListener, onSuccessListenerWithMsg<T> onSuccessListener) {
        if (NetworkReceiver.isConnected(activity)) {
            call.enqueue(new Callback<DataModel>() {
                @Override
                public void onResponse(@NotNull Call<DataModel> call, @NotNull Response<DataModel> dataModel) {
                    if (dataModel.isSuccessful()) {
                        String header = dataModel.raw().request().headers().toString();
                        String url = dataModel.raw().request().url().toString();
                        RequestBody requestBody = dataModel.raw().request().body();

                        if (dataModel.body() == null || dataModel.body().getResponseMsgModel() == null || dataModel.body().getResponseData() == null) {
                            callListener.onHttpErrorCode(requestCode, ResponseCode.UNSUPPORTED_MEDIA_TYPE, "Unsupported media type");
                        } else if (dataModel.body().getResponseMsgModel().isError()) {
                            callListener.onError(requestCode, UtilityClass.getInstance().nullChecker(dataModel.body().getResponseMsgModel().getErrorMessage()), false);
                        } else if (dataModel.body().getResponseMsgModel().isWarning()) {
                            callListener.onError(requestCode, UtilityClass.getInstance().nullChecker(dataModel.body().getResponseMsgModel().getWarningMessage()), true);
                        } else if (dataModel.body().isDoLogOut()) {
                            onSuccessListener.onDoLogout(requestCode);
                        } else if (dataModel.body().getResponseMsgModel().isEmptyCollection()) {
                            onSuccessListener.isEmptyCollection(requestCode);
                        } else if (dataModel.code() == 200 && !dataModel.body().getResponseMsgModel().isError() && !dataModel.body().getResponseMsgModel().isWarning()) {

                            Type typeClass = null;

                            Type[] genericInterfaces = onSuccessListener.getClass().getGenericInterfaces();
                            for (Type genericInterface : genericInterfaces) {
                                if (genericInterface instanceof ParameterizedType) {
                                    Type[] genericTypes = ((ParameterizedType) genericInterface).getActualTypeArguments();
                                    for (Type genericType : genericTypes) {
                                        typeClass = genericType;
                                    }
                                }
                            }
                            if (typeClass == null) {
                                callListener.onError(requestCode, "Casting error", false);
                            } else {
                                onSuccessListener.onSuccess(requestCode, dataModel.body().getResponseMsgModel().getSuccessMessage(), new Gson().fromJson(new Gson().toJson(dataModel.body().getResponseData().get("data")), typeClass));
                            }
                        }
                    } else if (dataModel.code() == 404) {
                        callListener.apiNotFound(requestCode);
                    } else if (dataModel.code() == 500) {
                        callListener.connectionBroken(requestCode);
                    } else if (dataModel.code() == 401) {
                        callListener.unauthorizedAccess(requestCode);
                    } else if (dataModel.code() == 405) {
                        callListener.methodNotAllowed(requestCode);
                    } else if (dataModel.code() == 408) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.REQUEST_TIMEOUT, "Request Time out error");
                    } else if (dataModel.code() == 409) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.CONFLICT_IN_REQUEST_BODY, "Conflict in requested body or header or query params.");
                    } else if (dataModel.code() == 413) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.PAYLOAD_TOO_LARGE, "Body is too large");
                    } else if (dataModel.code() == 415) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.UNSUPPORTED_MEDIA_TYPE, "Unsupported media type");
                    } else if (dataModel.code() == 502) {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.BAD_GATEWAY, "Bad gateway");
                    } else {
                        callListener.onHttpErrorCode(requestCode, ResponseCode.OTHER_ERROR, "Http error code : " + dataModel.code());
                    }
                }

                @Override
                public void onFailure(Call<DataModel> call, Throwable t) {
                    callListener.onError(requestCode, t.getLocalizedMessage(), false);
                }
            });
        } else {
            callListener.onNoConnectivity(requestCode);
        }
    }

    public <T> void callApi(Context activity, Call<DataModel> call, int requestCode, onResponseListener<T> callback) {
        if (NetworkReceiver.isConnected(activity)) {
            call.enqueue(new Callback<DataModel>() {
                @Override
                public void onResponse(@NotNull Call<DataModel> call, @NotNull Response<DataModel> dataModel) {
                    if (dataModel.isSuccessful()) {
                        if (dataModel.body() == null || dataModel.body().getResponseMsgModel() == null || dataModel.body().getResponseData() == null) {
                            callback.onError(requestCode, "Unsupported media type", false, ResponseCode.UNSUPPORTED_MEDIA_TYPE);
                        } else if (dataModel.body().getResponseMsgModel().isError()) {
                            callback.onError(requestCode, UtilityClass.getInstance().nullChecker(dataModel.body().getResponseMsgModel().getErrorMessage()), false, ResponseCode.ON_ERROR);
                        } else if (dataModel.body().getResponseMsgModel().isWarning()) {
                            callback.onError(requestCode, UtilityClass.getInstance().nullChecker(dataModel.body().getResponseMsgModel().getErrorMessage()), true, ResponseCode.ON_WARNING);
                        } else if (dataModel.body().isDoLogOut()) {
                            callback.onDoLogout(requestCode);
                        } else if (dataModel.body().getResponseMsgModel().isEmptyCollection()) {
                            callback.onEmptyCollection(requestCode);
                        } else if (dataModel.code() == 200 && !dataModel.body().getResponseMsgModel().isError() && !dataModel.body().getResponseMsgModel().isWarning()) {

                            Type typeClass = null;

                            Type[] genericInterfaces = callback.getClass().getGenericInterfaces();
                            for (Type genericInterface : genericInterfaces) {
                                if (genericInterface instanceof ParameterizedType) {
                                    Type[] genericTypes = ((ParameterizedType) genericInterface).getActualTypeArguments();
                                    for (Type genericType : genericTypes) {
                                        typeClass = genericType;
                                    }
                                }
                            }
                            if (typeClass == null) {
                                callback.onError(requestCode, "Casting Error", false, ResponseCode.CASTING_ERROR);
                            } else {
                                callback.onSuccess(requestCode, new Gson().fromJson(new Gson().toJson(dataModel.body().getResponseData().get("data")), typeClass));
                            }
                        }
                    } else if (dataModel.code() == 404) {
                        callback.onError(requestCode, "Api Not found", false, ResponseCode.API_NOT_FOUND);
                    } else if (dataModel.code() == 500) {
                        callback.onError(requestCode, "Server Error", false, ResponseCode.CONNECTION_BROKEN);
                    } else if (dataModel.code() == 405) {
                        callback.onError(requestCode, "Method not allowed", false, ResponseCode.METHOD_NOT_ALLOWED);
                    } else if (dataModel.code() == 401) {
                        callback.onError(requestCode, "Un-Authorized Access", false, ResponseCode.UNAUTHORIZED_ACCESS);
                    } else if (dataModel.code() == 408) {
                        callback.onError(requestCode, "Request Time out error", false, ResponseCode.REQUEST_TIMEOUT);
                    } else if (dataModel.code() == 409) {
                        callback.onError(requestCode, "Conflict in requested body or header or query params.", false, ResponseCode.CONFLICT_IN_REQUEST_BODY);
                    } else if (dataModel.code() == 413) {
                        callback.onError(requestCode, "Body is too large", false, ResponseCode.PAYLOAD_TOO_LARGE);
                    } else if (dataModel.code() == 415) {
                        callback.onError(requestCode, "Unsupported media type", false, ResponseCode.UNSUPPORTED_MEDIA_TYPE);
                    } else if (dataModel.code() == 502) {
                        callback.onError(requestCode, "Bad gateway", false, ResponseCode.BAD_GATEWAY);
                    } else {
                        callback.onError(requestCode, "Http error code : " + dataModel.code(), false, ResponseCode.OTHER_ERROR);
                    }
                }

                @Override
                public void onFailure(Call<DataModel> call, Throwable t) {
                    callback.onError(requestCode, t.getLocalizedMessage(), false, ResponseCode.ON_FAILURE);
                }
            });
        } else {
            callback.onError(requestCode, "No Internet Connectivity", false, ResponseCode.NO_INTERNET_CONNECTIVITY);
        }
    }

    public <T> void callApi(T bodyModel, Activity activity, Call<DataModel> call, int requestCode, CallListener callListener, onSuccessListener<T> onSuccessListener) {
        CustomAlertDialog.showDialogForJSONView(activity, BaseApplicationClass.getLibSharedPrefClient().getJSONViewerDisplayStatus(), call, bodyModel, new CustomAlertDialog.JSONViewListener<T>() {
            @Override
            public void onStop(T requestModel, String msg) {
                callListener.onError(requestCode, "Request stopped By user" , false);
            }

            @Override
            public void onProceed(Call<DataModel> dataModelCall, T requestModel) {
                callApi(activity, call, requestCode, callListener, onSuccessListener);
            }
        });
    }

    public <T> void callApiWithMessage(T bodyModel, Activity activity, Call<DataModel> call, int requestCode, CallListener callListener, onSuccessListenerWithMsg<T> onSuccessListener) {
        CustomAlertDialog.showDialogForJSONView(activity, BaseApplicationClass.getLibSharedPrefClient().getJSONViewerDisplayStatus(), call, bodyModel, new CustomAlertDialog.JSONViewListener<T>() {
            @Override
            public void onStop(T requestModel, String msg) {
                callListener.onError(requestCode, "Request stopped By user" , false);
            }

            @Override
            public void onProceed(Call<DataModel> dataModelCall, T requestModel) {
                callApiWithMessage(activity, call, requestCode, callListener, onSuccessListener);
            }
        });
    }

    public <U,V> void callApiWithDiffResponse(U bodyModel, Activity activity, Call<DataModel> call, int requestCode, CallListener callListener, onSuccessListener<V> onSuccessListener) {
        CustomAlertDialog.showDialogForJSONView(activity, BaseApplicationClass.getLibSharedPrefClient().getJSONViewerDisplayStatus(), call, bodyModel, new CustomAlertDialog.JSONViewListener<U>() {
            @Override
            public void onStop(U requestModel, String msg) {
                callListener.onError(requestCode, "Request stopped By user" , false);
            }

            @Override
            public void onProceed(Call<DataModel> dataModelCall, U requestModel) {
                callApi(activity, call, requestCode, callListener, onSuccessListener);
            }
        });
    }

    public <U,V> void callApiWithMessageDiffResponse(U bodyModel, Activity activity, Call<DataModel> call, int requestCode, CallListener callListener, onSuccessListenerWithMsg<V> onSuccessListener) {
        CustomAlertDialog.showDialogForJSONView(activity, BaseApplicationClass.getLibSharedPrefClient().getJSONViewerDisplayStatus(), call, bodyModel, new CustomAlertDialog.JSONViewListener<U>() {
            @Override
            public void onStop(U requestModel, String msg) {
                callListener.onError(requestCode, "Request stopped By user" , false);
            }

            @Override
            public void onProceed(Call<DataModel> dataModelCall, U requestModel) {
                callApiWithMessage(activity, call, requestCode, callListener, onSuccessListener);
            }
        });
    }

    public interface CallListener {

        void onNoConnectivity(int requestCode);

        void apiNotFound(int requestCode);

        void connectionBroken(int requestCode);

        void unauthorizedAccess(int requestCode);

        void methodNotAllowed(int requestCode);

        void onHttpErrorCode(int requestCode, ResponseCode responseCode, String msg);

        void onError(int requestCode, String error, boolean isWarning);
    }

    public interface onSuccessListener<T> {
        void onSuccess(int requestCode, T dataModel);

        void onDoLogout(int requestCode);

        void isEmptyCollection(int requestCode);
    }

    public interface onSuccessListenerWithMsg<T> {
        void onSuccess(int requestCode, @NotNull String msg, T dataModel);

        void onDoLogout(int requestCode);

        void isEmptyCollection(int requestCode);
    }

    public interface onResponseListener<T> {
        void onSuccess(int requestCode, T dataModel);

        void onDoLogout(int requestCode);

        void onEmptyCollection(int requestCode);

        void onError(int requestCode, String error, boolean isWarning, ResponseCode responseCode);
    }

    public enum ResponseCode {
        NO_INTERNET_CONNECTIVITY,
        API_NOT_FOUND,
        CONNECTION_BROKEN,
        UNAUTHORIZED_ACCESS,
        PAYLOAD_TOO_LARGE,
        UNSUPPORTED_MEDIA_TYPE,
        CONFLICT_IN_REQUEST_BODY,
        REQUEST_TIMEOUT,
        ON_ERROR,
        BAD_GATEWAY,
        METHOD_NOT_ALLOWED,
        ON_FAILURE,
        ON_WARNING,
        DO_LOGOUT,
        CASTING_ERROR,
        OTHER_ERROR,
        IS_EMPTY_COLLECTION
    }
}
