package com.hrpl.android_core.LocaleChanger;

/**
 * Enum that represents the preferred Locale to be set between a supported and a system Locale.
 */
public enum LocalePreference {
    PreferSupportedLocale,
    PreferSystemLocale
}