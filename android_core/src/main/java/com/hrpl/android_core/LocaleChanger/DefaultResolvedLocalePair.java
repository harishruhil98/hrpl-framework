package com.hrpl.android_core.LocaleChanger;
import java.util.Locale;

class DefaultResolvedLocalePair {
    private Locale supportedLocale;
    private Locale resolvedLocale;

    DefaultResolvedLocalePair(Locale supportedLocale, Locale resolvedLocale) {
        this.supportedLocale = supportedLocale;
        this.resolvedLocale = resolvedLocale;
    }

    Locale getSupportedLocale() {
        return supportedLocale;
    }

    Locale getResolvedLocale() {
        return resolvedLocale;
    }
}