package com.hrpl.android_core.LocaleChanger.base;

import android.app.Application;
import android.content.res.Configuration;

import androidx.annotation.NonNull;

import com.hrpl.android_core.LocaleChanger.LocaleChanger;

/**
 * Base {@link Application} class to inherit from with all needed configuration.
 */
public abstract class LocaleChangerBaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeLocaleChanger();
    }


    public abstract void initializeLocaleChanger();


    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleChanger.onConfigurationChanged();
    }
}