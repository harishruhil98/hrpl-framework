package com.hrpl.android_core.Listeners;

import androidx.annotation.NonNull;

public interface ISC_String2 {

    void onSuccess(@NonNull String deviceToken);
    void onError(@NonNull String error, boolean isWarning);

}
