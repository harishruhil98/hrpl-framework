package com.hrpl.android_core.Listeners;

public interface ISC_ReturnListener<T> {

    void onSuccess(T dataModel);

    void onError(String error, boolean isWarning);

    void doLogout();
}
