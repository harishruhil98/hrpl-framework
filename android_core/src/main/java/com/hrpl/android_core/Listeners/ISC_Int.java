package com.hrpl.android_core.Listeners;

import androidx.annotation.NonNull;

public interface ISC_Int {

    void onSuccess( int count);
    void onError(@NonNull String error);

}
