package com.hrpl.android_core.Listeners;



public interface SMSListener {
    void onSMSReceive(String OTP);
    void onTimeOut();
    void onError(String error);
}
