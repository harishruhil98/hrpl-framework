package com.hrpl.android_core.Listeners;

public interface ISC_ValidateUser {
    void onSuccess(boolean isMobileNoValid);
    void onError(String error, boolean isWarning);
}
