package com.hrpl.android_core.Listeners;

public interface WidthHeightChangeListener {
    void onMeasure(int width, int height);
}
