package com.hrpl.android_core.Listeners;

public interface AddressListener {
    void onReturnString(String HNo, String StreetAdd, String City, String Province, String ProvinceCode);
}