package com.hrpl.android_core.Listeners;

public interface ISC_ReturnListenerWithMsg<T> {

    void onSuccess(String msg, T dataModel);

    void onError(String error, boolean isWarning);

    void doLogout();
}
