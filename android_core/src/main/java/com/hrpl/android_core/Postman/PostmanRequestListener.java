package com.hrpl.android_core.Postman;

public interface PostmanRequestListener {
    void onStop(String msg);

    void onProceed();
}
