package com.hrpl.android_core.Postman;

import android.app.Activity;

import retrofit2.Call;
import retrofit2.Callback;

public class PostmanCall {

    private static PostmanBuilder postmanBuilder;
    private static PostmanCall postmanCallBuilder;

    public static PostmanCall build(){
        if (postmanBuilder == null) {
            postmanBuilder = PostmanBuilder.build();
        }

        if (postmanCallBuilder == null){
            postmanCallBuilder = new PostmanCall();
        }

        return postmanCallBuilder;
    }

    private PostmanCall() {
    }

    /**
     * This function is supposed to use alike postman to view request body and response at runtime.
     * It will take five params which is
     *
     * @param activity      This will provide activity context to function
     * @param showPostman   This will use to show/hide postman popup
     * @param requestBody   You have to pass request body object if api is having body
     * @param dataModelCall This is your as usual request call Object
     * @param callback      This is callback event handler which is used to provide request events like onSuccess, onFailure
     */

    public <T, S> void callRequestRestApi(Activity activity, boolean showPostman, S requestBody, Call<T> dataModelCall, Callback<T> callback) {
        postmanBuilder.callRequestRestApi(activity, showPostman, requestBody, dataModelCall, callback);
    }


}
