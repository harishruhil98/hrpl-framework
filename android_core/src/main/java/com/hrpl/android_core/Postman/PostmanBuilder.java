package com.hrpl.android_core.Postman;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hrpl.android_core.R;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostmanBuilder {

    private static PostmanBuilder builder;

    public static PostmanBuilder build() {
        if (builder == null) {
            builder = new PostmanBuilder();
        }
        return builder;
    }

    private PostmanBuilder() {
    }

    /**
     * This function is supposed to use alike postman to view request body and response at runtime.
     * It will take five params which is
     *
     * @param activity      This will provide activity context to function
     * @param showPostman   This will use to show/hide postman popup
     * @param requestBody   You have to pass request body object if api is having body
     * @param dataModelCall This is your as usual request call Object
     * @param callback      This is callback event handler which is used to provide request events like onSuccess, onFailure
     */
    protected <T, S> void callRequestRestApi(Activity activity, boolean showPostman, S requestBody, Call<T> dataModelCall, Callback<T> callback) {
        callRequestRestApiBase(activity, showPostman, requestBody, dataModelCall, callback);
    }

    private <T, S> void callRequestRestApiBase(Activity activity, boolean showPostman, S requestBody, Call<T> dataModelCall, Callback<T> callback) {
        if (showPostman) {
            postmanRequest(activity, true, dataModelCall, requestBody, new PostmanRequestListener() {
                @Override
                public void onStop(String msg) {
                    callback.onFailure(null, new Throwable("Request Stopped By User"));
                }

                @Override
                public void onProceed() {
                    dataModelCall.enqueue(new Callback<T>() {
                        @Override
                        public void onResponse(Call<T> call, Response<T> response) {
                            postmanResponse(activity, true, dataModelCall, requestBody, response.body(), () -> callback.onResponse(call, response));
                        }

                        @Override
                        public void onFailure(Call<T> call, Throwable t) {
                            callback.onFailure(call, t);
                        }
                    });
                }
            });

        } else {
            dataModelCall.enqueue(callback);
        }


    }

    @SuppressLint("SetTextI18n")
    private <T, Y> void postmanRequest(Activity activity, boolean isShow, Call<Y> dataModelCall, T model, PostmanRequestListener postmanRequestListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        @SuppressLint("InflateParams")
        View view = ((LayoutInflater) Objects.requireNonNull(activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE))).inflate(R.layout.layout_file, null, false);

        TextView tvDialogTitle = view.findViewById(R.id.tvTitle);
        ImageView ivCopyToClipBtn = view.findViewById(R.id.ivCopy);
        ImageView ivShare = view.findViewById(R.id.ivShare);
        TextView tvJsonData = view.findViewById(R.id.tvJsonData);
        TextView tvBtnStopRequest = view.findViewById(R.id.tvBtnStopRequest);
        TextView tvBtnProceedRequest = view.findViewById(R.id.tvPositiveBtn);

        builder.setView(view);

        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;

        Rect displayRectangle = new Rect();
        Window window = (activity).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.95);
        int height = (int) (displayRectangle.height() * 1);


        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        tvDialogTitle.setText("Postman View");
        setRequestData(tvJsonData, dataModelCall, model, null);

        //CLick Listener
        ivCopyToClipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("JSON Data", tvJsonData.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(activity, "data copied to clipboard.", Toast.LENGTH_SHORT).show();
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, tvJsonData.getText().toString());
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, "Share JSON via ...");
                activity.startActivity(shareIntent);
            }
        });
        tvBtnStopRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                dataModelCall.cancel();
                postmanRequestListener.onStop("Request Stopped by user");
            }
        });
        tvBtnProceedRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                postmanRequestListener.onProceed();
            }
        });

        if (isShow) {
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        } else {
            postmanRequestListener.onProceed();
        }
    }

    @SuppressLint("SetTextI18n")
    private <T, U, Y> void postmanResponse(Activity activity, boolean isShow, Call<T> dataModelCall, Y model, U returnResponse, PostmanResponseListener jsonViewListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        @SuppressLint("InflateParams")
        View view = ((LayoutInflater) Objects.requireNonNull(activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE))).inflate(R.layout.layout_2, null, false);

        TextView tvDialogTitle = view.findViewById(R.id.tvTitle);
        ImageView ivCopyToClipBtn = view.findViewById(R.id.ivCopy);
        ImageView ivShare = view.findViewById(R.id.ivShare);
        TextView tvJsonData = view.findViewById(R.id.tvJsonData);
        TextView tvBtnProceedRequest = view.findViewById(R.id.tvPositiveBtn);

        builder.setView(view);

        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;

        setRequestData(tvJsonData, dataModelCall, model, returnResponse);

        Rect displayRectangle = new Rect();
        Window window = (activity).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.95);
        int height = (int) (displayRectangle.height() * 1);


        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        tvDialogTitle.setText("Postman View");



        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, tvJsonData.getText().toString());
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, "Share JSON via ...");
                activity.startActivity(shareIntent);
            }
        });

        //CLick Listener
        ivCopyToClipBtn.setOnClickListener(view1 -> {
            ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("JSON Data", tvJsonData.getText().toString());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(activity, "data copied to clipboard.", Toast.LENGTH_SHORT).show();
        });
        tvBtnProceedRequest.setOnClickListener(view13 -> {
            alertDialog.dismiss();
            jsonViewListener.onDismiss();
        });

        if (isShow) {
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }
    }

    private<T, Y, U> void setRequestData(@NotNull TextView tvData, @NotNull Call<T> dataModelCall, @Nullable Y model, @Nullable U returnResponse) {
        String Url = dataModelCall.request().url().toString();
        String HTTPMethod = dataModelCall.request().method().toUpperCase().toString();
        String headers = dataModelCall.request().headers().toString();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String body = "";
        if (model != null) {
            body = gson.toJson(model);
        } else {
            body = "Null Body";
        }

        String returnResponseBody = "";
        if (returnResponse != null) {
            returnResponseBody = gson.toJson(returnResponse);
        } else {
            returnResponseBody = "Null Response";
        }

        StringBuilder stringBuilder = new StringBuilder();
        if (dataModelCall != null) {
            stringBuilder.append("Url: \n" + Url + "\n\n");
            stringBuilder.append("Headers: \n" + headers + "\n\n");
            stringBuilder.append("HTTP Method: \n" + HTTPMethod + "\n\n");
            if (model != null) {
                stringBuilder.append("Request Body: \n" + body + "\n\n\n");
            }
            if (returnResponse != null) {
                stringBuilder.append("Request Response: \n" + returnResponseBody + "\n\n\n");
            }
        }

        tvData.setText(stringBuilder.toString());
    }
}
