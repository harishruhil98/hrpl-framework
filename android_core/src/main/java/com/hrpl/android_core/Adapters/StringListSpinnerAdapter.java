package com.hrpl.android_core.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hrpl.android_core.R;

import java.util.ArrayList;

public class StringListSpinnerAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> arrayList;
    private String color;

    public StringListSpinnerAdapter(Context context, ArrayList<String> arrayList, String color) {
        this.context = context;
        this.arrayList = arrayList;
        this.color = color;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.custom_string_list_item, parent, false);
        }

        final TextView textView = convertView.findViewById(R.id.custom_login_register_list_area_item_TextvButton);

        textView.setText(arrayList.get(position));
//        textView.setTextColor(Color.parseColor(color));

        return convertView;
    }
}
