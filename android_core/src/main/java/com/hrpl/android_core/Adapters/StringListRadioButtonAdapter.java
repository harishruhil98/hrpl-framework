package com.hrpl.android_core.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.hrpl.android_core.Models.SelectedStringModel;
import com.hrpl.android_core.R;

import java.util.ArrayList;

public class StringListRadioButtonAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SelectedStringModel> arrayList;
    private LayoutInflater layoutInflater;
    private SelectionModeListener listner;
    private int color;

    public StringListRadioButtonAdapter(Context context, ArrayList<SelectedStringModel> arrayList, int color, SelectionModeListener listner) {
        this.context = context;
        this.listner = listner;
        this.color = color;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.custom_string_radio_button_list_item, parent, false);
        }

        final RadioButton radioButton = convertView.findViewById(R.id.custom_login_register_list_area_item_radioButton);


        final SelectedStringModel stringModel = arrayList.get(position);

        if (stringModel.getIsActive()){
            radioButton.setEnabled(true);
        }else {
            radioButton.setEnabled(false);
        }


        if (stringModel.getIsActive() && position == 0) {
            radioButton.setText(stringModel.getStringValue());
        }else {
            if (!stringModel.getIsActive()) {
                String text2 = stringModel.getStringValue();
                radioButton.setText(text2);
            }else {
                radioButton.setText(stringModel.getStringValue());
            }
        }

        radioButton.setTextColor(context.getResources().getColor(R.color.colorText2));

        radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked && !arrayList.get(position).getIsSelected()){
                radioButton.setTextColor(context.getResources().getColor(color));
                arrayList.get(position).setIsSelected(true);

                for (int a = 0; a < arrayList.size(); a++){
                    if (!(position == a)){
                        arrayList.get(a).setIsSelected(false);
                    }
                }

                listner.onClick(arrayList.get(position), position);
                notifyDataSetChanged();

            }else {
                radioButton.setTextColor(context.getResources().getColor(R.color.colorText2));
            }
        });

        radioButton.setChecked(arrayList.get(position).getIsSelected());


        return convertView;
    }

    public interface SelectionModeListener {
        void onClick(SelectedStringModel areaModel, int Position);
    }
}
