package com.hrpl.android_core.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.hrpl.android_core.Models.LanguageModel;
import com.hrpl.android_core.R;

import java.util.ArrayList;

public class LanguageListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<LanguageModel> arrayList;
    private LayoutInflater layoutInflater;
    private SelectionModeListener listner;

    public LanguageListAdapter(Context context, ArrayList<LanguageModel> arrayList, SelectionModeListener listner) {
        this.context = context;
        this.listner = listner;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.custom_string_list_item_layout, parent, false);
        }

        final RadioButton radioButton = convertView.findViewById(R.id.CustomStringItem_rbItem);

        final LanguageModel languageModel = arrayList.get(position);

        if (languageModel.isSelected()) {
            radioButton.setEnabled(true);
        } else {
            radioButton.setEnabled(false);
        }

        radioButton.setText(languageModel.getLanguageName());

        radioButton.setTextColor(context.getResources().getColor(R.color.colorBlack));

        radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked && !arrayList.get(position).isSelected()) {
                radioButton.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                arrayList.get(position).setSelected(true);

                for (int a = 0; a < arrayList.size(); a++) {
                    if (!(position == a)) {
                        arrayList.get(a).setSelected(false);
                    }
                }

                if (listner != null) {
                    listner.onClick(arrayList.get(position), position);
                }
                notifyDataSetChanged();

            } else {
                radioButton.setTextColor(context.getResources().getColor(R.color.colorBlack));
            }
        });

        radioButton.setChecked(arrayList.get(position).isSelected());


        return convertView;
    }

    public interface SelectionModeListener {
        void onClick(LanguageModel languageModel, int Position);
    }

    public LanguageModel getSelectedModel() {
        LanguageModel model = new LanguageModel();
        for (LanguageModel item : arrayList) {
            if (item.isSelected()) {
                model = item;
                break;
            }
        }

        return model;
    }
}
