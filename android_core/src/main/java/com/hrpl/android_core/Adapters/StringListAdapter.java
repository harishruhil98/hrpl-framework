package com.hrpl.android_core.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hrpl.android_core.Models.SelectedStringModel;
import com.hrpl.android_core.R;

import java.util.ArrayList;

public class StringListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SelectedStringModel> arrayList;
    private String color;

    public StringListAdapter(Context context, ArrayList<SelectedStringModel> arrayList, String color) {
        this.context = context;
        this.arrayList = arrayList;
        this.color = color;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.custom_string_list_item, parent, false);
        }

        final TextView textView = convertView.findViewById(R.id.custom_login_register_list_area_item_TextvButton);


        final SelectedStringModel stringModel = arrayList.get(position);
        textView.setText(stringModel.getStringValue());
//        textView.setTextColor(Color.parseColor(color));

        return convertView;
    }
}
