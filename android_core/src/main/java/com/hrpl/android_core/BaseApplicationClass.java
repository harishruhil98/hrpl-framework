package com.hrpl.android_core;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.hrpl.android_core.DataStorage.LibSharedPreference;
import com.hrpl.android_core.DataStorage.RoomDatabase.DAO;
import com.hrpl.android_core.DataStorage.RoomDatabase.MyDatabase;
import com.hrpl.android_core.LocaleChanger.LocaleChanger;
import com.hrpl.android_core.Utility.ConstantClass;
import com.hrpl.android_core.Utility.PushDownAnimation.ClickEvent;
import com.hrpl.android_core.Utility.UtilityClass;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public abstract class BaseApplicationClass extends Application {

    protected Context context;
    @SuppressLint("StaticFieldLeak")
    protected static BaseApplicationClass mInstance;
    private static Retrofit retrofit = null;

    protected abstract boolean isAvailableForProduction();

    protected abstract String baseUrl();
    private static String baseUrl = "";

    public abstract Context getContext();

    public abstract BaseApplicationClass getInstance();

    public abstract int getNoDataFoundImage();

    public static final List<Locale> SUPPORTED_LOCALES =
            Arrays.asList(
                    new Locale("en", "US"),
                    new Locale("hi", "IN")
            );


    @Override
    public void onCreate() {
        super.onCreate();
        context = getContext();
        mInstance = getInstance();

        // Setup handler for uncaught exceptions.
        if (isAvailableForProduction()) {
            Thread.setDefaultUncaughtExceptionHandler(this::handleUncaughtException);
        }


//        // OneSignal Initialization
//        OneSignal.initWithContext(this);
//        OneSignal.setAppId(ConstantClass.OneSignalAppId);
//        OneSignal.unsubscribeWhenNotificationsAreDisabled(false);

        ConstantClass.noDataFoundImagePath = getNoDataFoundImage();

        LibSharedPreference.getInstance(mInstance).setDeviceId(Settings.Secure.getString(mInstance.getContentResolver(), Settings.Secure.ANDROID_ID));

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        LibSharedPreference.getInstance(context).setURL(baseUrl());

        baseUrl = LibSharedPreference.getInstance(context).getURL();
        LocaleChanger.initialize(getApplicationContext(), SUPPORTED_LOCALES);

        LocaleChanger.setLocale((Locale)BaseApplicationClass.SUPPORTED_LOCALES.get(LibSharedPreference.getInstance(mInstance).getLCode().equalsIgnoreCase("hi") ? 1 : 0));
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleChanger.onConfigurationChanged();
    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically
        System.exit(1); // kill off the crashed app
    }

    public static void setURL(@NonNull  String url){
        baseUrl = UtilityClass.getInstance().nullChecker(url);
        LibSharedPreference.getInstance(mInstance).setURL(UtilityClass.getInstance().nullChecker(url));
        restartClients();
        new Handler().postDelayed(BaseApplicationClass::getRetrofitObjClient, 200);
    }

    public static synchronized String getDeviceId(Activity activity){
        return Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static synchronized String getUserAgent() {
        return Build.MANUFACTURER + " / " + Build.MODEL + " / " + Build.VERSION.SDK + " / " + Build.VERSION.RELEASE;
    }

    public static synchronized BaseApplicationClass getBaseApplicationContext() {
        return mInstance;
    }

    // synchronized is because only one thread can access this object which makes app fast
    protected static synchronized Retrofit getRetrofitObjClient() {
        if (retrofit == null) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.MINUTES)
                    .readTimeout(10, TimeUnit.MINUTES)
                    .addInterceptor(chain -> {
                        Request request = chain.request().newBuilder()
                                .addHeader("content-type", ConstantClass.contentTye)
                                .addHeader("User-Agent", getUserAgent())
                                .addHeader("Custom-Key", LibSharedPreference.getInstance(mInstance).getCustomKey())
                                .addHeader("Authorization", LibSharedPreference.getInstance(mInstance).getDeviceToken())
                                .build();
                        return chain.proceed(request);
                    })
                    .writeTimeout(20, TimeUnit.MINUTES).build();
            retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).client(client).build();
        }
        return retrofit;
    }

    /**
     * //Services Api Functions
     * <p>
     * /**Constant Variables
     */
    private static ClickEvent clickEvent = null;

    public static synchronized ClickEvent getClickEventClient() {

        if (clickEvent == null) {
            clickEvent = new ClickEvent();
        }
        return clickEvent;
    }

    private static LibSharedPreference preference;
    public static synchronized LibSharedPreference getLibSharedPrefClient() {

        if (preference == null) {
            preference = LibSharedPreference.getInstance(mInstance);
        }
        return preference;
    }


    private static DAO dao = null;

    public static synchronized DAO getDBObject() {
        if (dao == null) {
            dao = MyDatabase.getDBObject(mInstance);
        }
        return dao;
    }
    protected static void restartClients() {
        retrofit = null;
    }

    /*private static Activity activity;
    public static Activity getCurrentActivity(){
        return activity;
    }

    public static void setCurrentActivityObject(Activity activity){
        this.activity = activity;
    }*/
}