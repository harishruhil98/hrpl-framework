package com.hrpl.android_core.CustomViews.AutoScrollVP;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.Interpolator;

import androidx.core.view.MotionEventCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.hrpl.android_core.R;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

public class AutoScrollVP extends ViewPager {

    private static final String TAG = "AutoScrollVP";

    private static final int DEFAULT_SLIDE_INTERVAL = 5000;

    private int slideInterval = DEFAULT_SLIDE_INTERVAL;

    private static final int DEFAULT_SLIDE_DURATION = 800;

    private int slideDuration = DEFAULT_SLIDE_DURATION;


    public static final int DIRECTION_RIGHT = 1;

    public static final int DIRECTION_LEFT = 0;
    private int direction = DIRECTION_RIGHT;

    private boolean stopWhenTouch = true;


    private static final int MSG_SCROLL = 1;


    private boolean cycle;


    private boolean isAutoScroll;

    private boolean isStopedWhenTouch;

    private MyHandler mHandler;
    private CustomDurationScroller mScroller;

    public AutoScrollVP(Context context) {
        super(context);
        init(context, null);
    }

    public AutoScrollVP(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     *
     * @param context
     * @param attrs
     */
    private void init(Context context, AttributeSet attrs) {
        mHandler = new MyHandler(this);
        setScroller();
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.AutoScrollViewPager, 0, 0);
            try {
                slideInterval = array.getInt(R.styleable.AutoScrollViewPager_slideInterval,
                        DEFAULT_SLIDE_INTERVAL);
                direction = array.getInt(R.styleable.AutoScrollViewPager_slideDirection,
                        DIRECTION_RIGHT);
                stopWhenTouch = array.getBoolean(R.styleable.AutoScrollViewPager_stopWhenTouch,
                        true);
                cycle = array.getBoolean(R.styleable.AutoScrollViewPager_cycle, false);
                slideDuration = array.getInt(R.styleable.AutoScrollViewPager_slideDirection,
                        DEFAULT_SLIDE_DURATION);
            } finally {
                array.recycle();
            }
        }
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {
        super.setAdapter(adapter);
        if (adapter != null && adapter instanceof InfinitePagerAdapter && adapter.getCount() > 1) {
            int midPos = ((adapter.getCount() - 2) / 2) - (adapter.getCount() - 2) / 2 % ((InfinitePagerAdapter) adapter).getItemCount() + 1;
            setCurrentItem(midPos);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        addOnPageChangeListener(mOnPageChangeListener);
    }

    /**
     * ViewPager
     */
    private void scroll() {
        PagerAdapter adapter = getAdapter();
        int currentItem = getCurrentItem();
        int totalCount;
        if (adapter == null || (totalCount = adapter.getCount()) <= 1) {
            return;
        }
        int nextItem = (direction == DIRECTION_RIGHT) ? ++currentItem : --currentItem;
        if (!(getAdapter() instanceof InfinitePagerAdapter) && cycle) {
            if (nextItem < 0) {
                setCurrentItem(totalCount - 1, true);
            } else if (nextItem == totalCount) {
                setCurrentItem(0, true);
            } else {
                setCurrentItem(nextItem, true);
            }
        } else {
            setCurrentItem(nextItem, true);
        }
    }

    /**
     * scroller ViewPager
     */
    private void setScroller() {
        try {
            Field scrollerField = ViewPager.class.getDeclaredField("mScroller");
            scrollerField.setAccessible(true);
            Field interpolatorField = ViewPager.class.getDeclaredField("sInterpolator");
            interpolatorField.setAccessible(true);
            mScroller = new CustomDurationScroller(getContext(), (Interpolator) interpolatorField.get(null));
            mScroller.setDuration(slideDuration);
            scrollerField.set(this, mScroller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void startAutoScroll() {
        if (getAdapter().getCount() <= 1) {
            return;
        }
        isAutoScroll = true;
        sendScrollMsg(slideInterval);
    }

    /**
     * @param delayTime
     */
    public void startAutoScroll(int delayTime) {
        isAutoScroll = true;
        slideInterval = delayTime;
        setScroller();
        sendScrollMsg(delayTime);
    }

    public void stopAutoScroll() {
        isAutoScroll = false;
        mHandler.removeMessages(MSG_SCROLL);
    }

    private void sendScrollMsg(long delayTime) {
        mHandler.removeMessages(MSG_SCROLL);
        mHandler.sendEmptyMessageDelayed(MSG_SCROLL, delayTime);
    }

    private static class MyHandler extends Handler {
        private WeakReference<AutoScrollVP> hostWeakReference;

        public MyHandler(AutoScrollVP host) {
            hostWeakReference = new WeakReference<AutoScrollVP>(host);
        }

        @Override
        public void handleMessage(Message msg) {
            AutoScrollVP host = hostWeakReference.get();
            if (host != null) {
                host.scroll();
                host.sendScrollMsg(host.slideInterval);
            }
        }
    }

    /**
     * ViewPager
     */
    private SimpleOnPageChangeListener mOnPageChangeListener = new SimpleOnPageChangeListener() {
        @Override
        public void onPageScrollStateChanged(int state) {
            super.onPageScrollStateChanged(state);
            if (state == SCROLL_STATE_IDLE) {
                if (getAdapter() != null && getAdapter() instanceof InfinitePagerAdapter) {
                    int cur = getCurrentItem();
                    // 获取最后一个真实的item
                    int lastReal = getAdapter().getCount() - 2;
                    if (cur == 0) {
                        setCurrentItem(lastReal, false);
                    } else if (cur > lastReal) {
                        setCurrentItem(1, false);
                    }
                }
            }
        }
    };

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int action = MotionEventCompat.getActionMasked(ev);
        if (stopWhenTouch) {
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    if (isAutoScroll) {
                        isStopedWhenTouch = true;
                        stopAutoScroll();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_OUTSIDE:
                    if (isStopedWhenTouch) {
                        startAutoScroll();
                    }
                    break;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeOnPageChangeListener(mOnPageChangeListener);
    }

    public int getSlideInterval() {
        return slideInterval;
    }

    /**
     * @param slideInterval
     */
    public void setSlideInterval(int slideInterval) {
        this.slideInterval = slideInterval;
        setScroller();
    }

    public int getDirection() {
        return direction;
    }

    /**
     * @param direction
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    public boolean isStopWhenTouch() {
        return stopWhenTouch;
    }

    /**
     * @param stopWhenTouch
     */
    public void setStopWhenTouch(boolean stopWhenTouch) {
        this.stopWhenTouch = stopWhenTouch;
    }

    public boolean isCycle() {
        return cycle;
    }

    /**
     * @param cycle
     */
    public void setCycle(boolean cycle) {
        this.cycle = cycle;
    }

    /**
     * @param slideDuration
     */
    public void setSlideDuration(int slideDuration) {
        this.slideDuration = slideDuration;
    }

}