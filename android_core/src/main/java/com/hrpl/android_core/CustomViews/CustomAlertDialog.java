package com.hrpl.android_core.CustomViews;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hrpl.android_core.Adapters.StringListRadioButtonAdapter;
import com.hrpl.android_core.BaseApplicationClass;
import com.hrpl.android_core.Models.DataModel;
import com.hrpl.android_core.Models.SelectedStringModel;
import com.hrpl.android_core.R;
import com.hrpl.android_core.Utility.ConstantClass;
import com.hrpl.android_core.Utility.FileUtils;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;

public class CustomAlertDialog {


    public static androidx.appcompat.app.AlertDialog showDialogForInternetConnectivity(Context context, InternetConnectivityListener internetConnectivityListener) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_internet_connectivity_failled_dialog, null, false);
        ImageView ivBtnClose = view.findViewById(R.id.custom_internetCConnectivityDialog_iv_BtnClose);
        ImageView ivIcon = view.findViewById(R.id.custom_internetCConnectivityDialog_ivIcon);
        ImageView ivReloadBtn = view.findViewById(R.id.custom_internetCConnectivityDialog_iv_BtnReload);
        builder.setView(view);

        if (ConstantClass.noDataFoundImagePath > 0) {
            ivIcon.setImageResource(ConstantClass.noDataFoundImagePath);
        }

        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;
//        alertDialog.show();

//        int width = StaticClass.getScreenWidth() - StaticClass.convert_DP_to_Pixel(30, context);
//        int height = StaticClass.getScreenHeight() - StaticClass.convert_DP_to_Pixel(50, context);
//        alertDialog.getWindow().setLayout(width, height);

        ivBtnClose.setOnClickListener(v -> alertDialog.dismiss());
//        ivBtnClose.setVisibility(View.GONE);

        ivReloadBtn.setOnClickListener(v -> alertDialog.dismiss());
        ivReloadBtn.setOnClickListener(v -> internetConnectivityListener.onReloadClick());

        return alertDialog;
    }

    public static void showDialogForSelectString(final Context context, String title, ArrayList<String> stringArrayList, String preSelectedString, int color, final SelectStringListener selectStringListener) {
        ArrayList<SelectedStringModel> arrayList = new ArrayList<>();
        for (String s : stringArrayList) {
            arrayList.add(new SelectedStringModel(s, false, true));
        }

        if (!UtilityClass.getInstance().nullChecker(preSelectedString).isEmpty()) {
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList.get(i).setIsActive(arrayList.get(i).getStringValue().equalsIgnoreCase(preSelectedString));
            }
        }

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_select_string_layout, null, false);
        ((TextView) view.findViewById(R.id.CustomSelectString_tvTitle)).setText(title);
        ListView listView = view.findViewById(R.id.CustomSelectString_lvListArea);
        LinearLayout llRootView = view.findViewById(R.id.CustomSelectString_cvRootView);

        TextView tvConfirm = view.findViewById(R.id.CustomAlertDialog_tvPositiveBtn);
        TextView tvCancel = view.findViewById(R.id.CustomAlertDialog_tvNegativeBtn);

        if (stringArrayList.size() < 5) {
//            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) llRootView.getLayoutParams();
//            lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            llRootView.setLayoutParams(new LinearLayout.LayoutParams(CardView.LayoutParams.MATCH_PARENT, CardView.LayoutParams.WRAP_CONTENT));
        }

        builder.setView(view);

        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;

        alertDialog.show();

        Rect displayRectangle = new Rect();
        Window window = ((Activity) context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.95f);
        int height = (int) (displayRectangle.height() * 1);

        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        final String[] SelectedProvince = {""};
        SelectedProvince[0] = preSelectedString;
        StringListRadioButtonAdapter listAdapter = new StringListRadioButtonAdapter(context, arrayList, color, (areaModel1, Position) -> SelectedProvince[0] = areaModel1.getStringValue());
        listView.setAdapter(listAdapter);

        BaseApplicationClass.getClickEventClient().setClickListener(tvConfirm, 3f, v -> {
            selectStringListener.onSelected(SelectedProvince[0]);
            alertDialog.dismiss();
        });
        BaseApplicationClass.getClickEventClient().setClickListener(tvCancel, 3f, v -> alertDialog.dismiss());
    }

    public static void showPdfFilePreview(Context context, String title, Uri fileURI, PdfPreviewListener pdfPreviewListener) throws IOException {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_pdf_preview, null, false);

        LinearLayout llContainerView = view.findViewById(R.id.containerPdf);
        TextView tvTitle = view.findViewById(R.id.CustomHeaderView_tvTitle);
        tvTitle.setText(title);

        TextView tvBtnCancel = view.findViewById(R.id.custom_pdf_preview_tv_BtnCancel);
        CardView cvConfirm = view.findViewById(R.id.custom_pdf_preview_cv_BtnConfirm);

        FileUtils fileUtils = new FileUtils((Activity) context);

        ArrayList<Bitmap> bitmapArrayList = fileUtils.getPdfAsBitmapList(fileUtils.getFileObject(fileURI));

        fileUtils = null;

        for (int a = 0; a < bitmapArrayList.size(); a++) {
            ImageView imageView = new ImageView(context);
            imageView.setImageBitmap(bitmapArrayList.get(a));
            llContainerView.addView(imageView);
        }

        builder.setView(view);

        bitmapArrayList = null;

        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        alertDialog.getWindow().setGravity(Gravity.BOTTOM);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomOpen;
        alertDialog.show();
        Rect displayRectangle = new Rect();
        Window window = ((Activity) context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

//        int width = (int) (displayRectangle.width() * widthScale);
//        int height = (int) (displayRectangle.height() * heightScale);

        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnCancel, v -> {
            alertDialog.dismiss();
            pdfPreviewListener.onCancel();
        });

        BaseApplicationClass.getClickEventClient().setClickListener(cvConfirm, v -> {
            alertDialog.dismiss();
            pdfPreviewListener.onConfirm(fileURI);
        });
    }

    public static void showAlertMessageDialog(final Context context, String message, AlertType alertType, ButtonType buttonType, AlertDialogListener alertDialogListener) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_alert_dialog_new, null, false);

        ImageView ivIcon = view.findViewById(R.id.CustomAlertDialog_ivIcon);
        ((TextView) view.findViewById(R.id.CustomAlertDialog_tvMessage)).setText(message);

        LinearLayout llBtnLayout = view.findViewById(R.id.CustomAlertDialog_llBtnLayout);

        LinearLayout llBtnPositive = view.findViewById(R.id.CustomAlertDialog_llPositiveBtn);
        TextView tvBtnPositive = view.findViewById(R.id.CustomAlertDialog_tvPositiveBtn);

        LinearLayout llBtnNegative = view.findViewById(R.id.CustomAlertDialog_llNegativeBtn);
        TextView tvBtnNegative = view.findViewById(R.id.CustomAlertDialog_tvNegativeBtn);


        if (alertType == AlertType.Success) {
            ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_success_primary));
        } else if (alertType == AlertType.Info) {
            ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_info_primary));
        } else if (alertType == AlertType.Error) {
            ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_alert_primary));
        }

        if (buttonType == ButtonType.ConfirmBtnOnly) {
            llBtnNegative.setVisibility(View.GONE);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) llBtnPositive.getLayoutParams();
            layoutParams.weight = 2;
            llBtnPositive.setLayoutParams(layoutParams);
        } else if (buttonType == ButtonType.CancelBtnOnly) {
            llBtnNegative.setVisibility(View.GONE);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) llBtnPositive.getLayoutParams();
            layoutParams.weight = 2;
            llBtnPositive.setLayoutParams(layoutParams);
            tvBtnPositive.setText("Dismiss");

        } else {
            llBtnNegative.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) llBtnPositive.getLayoutParams();
            layoutParams.weight = 1;
            llBtnPositive.setLayoutParams(layoutParams);
        }

        Rect displayRectangle = new Rect();
        Window window = ((Activity) context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.85f);
        int height = (int) (displayRectangle.height() * 1.0f);


        builder.setView(view);
        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.setCancelable(false);

        alertDialog.show();
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;
        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);


        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnPositive, 3f, v -> {
            if (alertDialogListener != null) {
                alertDialogListener.onConfirm();
            }
            alertDialog.dismiss();
        });
        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnNegative, 3f, v -> {
            if (alertDialogListener != null) {
                alertDialogListener.onCancel();
            }
            alertDialog.dismiss();
        });
    }

    public static void showAlertMessageDialog(final Context context, String message, AlertType alertType, ButtonType buttonType, String positiveBtnText, String negativeBtnText, AlertDialogListener alertDialogListener) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_alert_dialog_new, null, false);

        ImageView ivIcon = view.findViewById(R.id.CustomAlertDialog_ivIcon);
        ((TextView) view.findViewById(R.id.CustomAlertDialog_tvMessage)).setText(message);

        LinearLayout llBtnLayout = view.findViewById(R.id.CustomAlertDialog_llBtnLayout);

        LinearLayout llBtnPositive = view.findViewById(R.id.CustomAlertDialog_llPositiveBtn);
        TextView tvBtnPositive = view.findViewById(R.id.CustomAlertDialog_tvPositiveBtn);

        LinearLayout llBtnNegative = view.findViewById(R.id.CustomAlertDialog_llNegativeBtn);
        TextView tvBtnNegative = view.findViewById(R.id.CustomAlertDialog_tvNegativeBtn);


        if (alertType == AlertType.Success) {
            ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_success_primary));
        } else if (alertType == AlertType.Info) {
            ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_info_primary));
        } else if (alertType == AlertType.Error) {
            ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_alert_primary));
        }

        if (buttonType == ButtonType.ConfirmBtnOnly) {
            llBtnNegative.setVisibility(View.GONE);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) llBtnPositive.getLayoutParams();
            layoutParams.weight = 2;
            llBtnPositive.setLayoutParams(layoutParams);
            tvBtnPositive.setText(UtilityClass.getInstance().nullChecker(positiveBtnText).isEmpty() ? "Confirm" : positiveBtnText);
        } else if (buttonType == ButtonType.CancelBtnOnly) {
            llBtnNegative.setVisibility(View.GONE);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) llBtnPositive.getLayoutParams();
            layoutParams.weight = 2;
            llBtnPositive.setLayoutParams(layoutParams);
            tvBtnPositive.setText("Dismiss");
            tvBtnNegative.setText(UtilityClass.getInstance().nullChecker(negativeBtnText).isEmpty() ? "Dismiss" : negativeBtnText);
        } else {
            llBtnNegative.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) llBtnPositive.getLayoutParams();
            layoutParams.weight = 1;
            llBtnPositive.setLayoutParams(layoutParams);
            tvBtnPositive.setText(UtilityClass.getInstance().nullChecker(positiveBtnText).isEmpty() ? "Confirm" : positiveBtnText);
            tvBtnNegative.setText(UtilityClass.getInstance().nullChecker(negativeBtnText).isEmpty() ? "Dismiss" : negativeBtnText);
        }

        Rect displayRectangle = new Rect();
        Window window = ((Activity) context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.85f);
        int height = (int) (displayRectangle.height() * 1.0f);


        builder.setView(view);
        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.setCancelable(false);

        alertDialog.show();
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;
        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);


        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnPositive, 3f, v -> {
            if (alertDialogListener != null) {
                alertDialogListener.onConfirm();
            }
            alertDialog.dismiss();
        });
        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnNegative, 3f, v -> {
            if (alertDialogListener != null) {
                alertDialogListener.onCancel();
            }
            alertDialog.dismiss();
        });
    }

    public static void showMsgConfirmDialog(final Context context, String msg, onActionResult actionResult) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_show_msg_confirm, null, false);
        TextView tvMsg = view.findViewById(R.id.CustomShowMsg_tvMsg);
        TextView tvBtnCancel = view.findViewById(R.id.CustomShowMsg_tvCancelBtn);
        TextView tvBtnConfirm = view.findViewById(R.id.CustomShowMsg_tvConfirmBtn);

        tvMsg.setText(UtilityClass.getInstance().nullChecker(msg));

        builder.setView(view);
        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setGravity(Gravity.BOTTOM);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomOpen;
        alertDialog.show();

        Rect displayRectangle = new Rect();
        Window window = ((Activity) context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.95);
        int height = (int) (displayRectangle.height() * 0.9);

//        alertDialog.getWindow().setLayout(width, height);

        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnCancel, 3f, v -> {
            alertDialog.dismiss();
            actionResult.onCancel();
        });

        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnConfirm, 3f, v -> {
            alertDialog.dismiss();
            actionResult.onConfirm();
        });

    }

    public static void showMsgConfirmDialog(final Context context, String message, AlertDialogListener actionResult) {
        showMsgConfirmDialog(context, message, new onActionResult() {
            @Override
            public void onConfirm() {
                actionResult.onConfirm();
            }

            @Override
            public void onCancel() {
                actionResult.onCancel();
            }
        });
    }

    public static void DocGeneratedDialog(final Context context, String msg, int msgColor, int positiveBtnColor, final DocGenerateListener listener) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_doc_genrate_layout, null, false);

        TextView tvTitle = view.findViewById(R.id.CustomHeaderView_tvTitle);
        TextView tvMsg = view.findViewById(R.id.CustomDocGenerated_tvMsg);
        if (msgColor > 0) {
            tvMsg.setTextColor(msgColor);
        }

        if (!UtilityClass.getInstance().nullChecker(msg).isEmpty()) {
            tvMsg.setText(UtilityClass.getInstance().nullChecker(msg));
        }

        TextView tvBtnPositiveBtn = view.findViewById(R.id.CustomDocGenerated_tvBtnPreview);
        LinearLayout llBtnPositiveBtn = view.findViewById(R.id.CustomDocGenerated_llBtnPreview);
        TextView tvBtnShare = view.findViewById(R.id.CustomDocGenerated_tvBtnShare);

        tvTitle.setText("Alert !!!");

        if (positiveBtnColor > 0) {
            llBtnPositiveBtn.setBackgroundColor(positiveBtnColor);
            tvBtnShare.setTextColor(positiveBtnColor);
        }

        builder.setView(view);
        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogZoominZoomOutAnimation;
        alertDialog.show();

        Rect displayRectangle = new Rect();
        Window window = ((Activity) context).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.92);
        int height = (int) (displayRectangle.height() * 1);

        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        tvBtnPositiveBtn.setOnClickListener(v -> {
            alertDialog.dismiss();
            listener.onPreview();
        });

        tvBtnShare.setOnClickListener(v -> {
            alertDialog.dismiss();
            listener.onShare();
        });
    }

    public interface onActionResult {
        void onConfirm();

        void onCancel();
    }


    public interface InternetConnectivityListener {
        void onReloadClick();
    }

    public interface SelectStringListener {
        void onSelected(String string);
    }

    public interface PdfPreviewListener {
        void onCancel();

        void onConfirm(Uri uri);
    }

    public interface AlertDialogListener {
        void onConfirm();

        void onCancel();
    }

    public static enum AlertType {
        Info,
        Error,
        Success
    }

    public static enum ButtonType {
        ConfirmBtnOnly,
        CancelBtnOnly,
        CancelWithConfirm
    }

    public interface DocGenerateListener {
        void onShare();

        void onPreview();
    }

    @SuppressLint("SetTextI18n")
    public static <T> void showDialogForJSONView(Activity activity, boolean isShow, Call<DataModel> dataModelCall, T model, JSONViewListener<T> jsonViewListener) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
        @SuppressLint("InflateParams")
        View view = ((LayoutInflater) Objects.requireNonNull(activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE))).inflate(R.layout.custom_json_view, null, false);

        TextView tvDialogTitle = view.findViewById(R.id.CustomJSONView_tvTitle);
        ImageView ivCopyToClipBtn = view.findViewById(R.id.CustomJSONView_ivCopy);
        ImageView ivShare = view.findViewById(R.id.CustomJSONView_ivShare);
        TextView tvJsonData = view.findViewById(R.id.CustomJSONView_tvJsonData);
        TextView tvBtnStopRequest = view.findViewById(R.id.CustomJSONView_tvBtnStopRequest);
        TextView tvBtnProceedRequest = view.findViewById(R.id.CustomAlertDialog_tvPositiveBtn);

        builder.setView(view);

        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;

        Rect displayRectangle = new Rect();
        Window window = (activity).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.95);
        int height = (int) (displayRectangle.height() * 1);


        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        String Url = dataModelCall.request().url().toString();
        String HTTPMethod = dataModelCall.request().method().toUpperCase().toString();
        String headers = dataModelCall.request().headers().toString();

        tvDialogTitle.setText("JSON Data View");

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String body = "";
        if (model != null) {
            body = gson.toJson(model);
        } else {
            body = "Null Body";
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Url: " + Url + "\n\n");
        stringBuilder.append("Headers: " + headers + "\n\n");
        stringBuilder.append("HTTP Method: " + HTTPMethod + "\n\n");
        stringBuilder.append("Body: " + body + "\n\n");

        tvJsonData.setText(stringBuilder.toString());

        //CLick Listener
        BaseApplicationClass.getClickEventClient().setClickListener(ivCopyToClipBtn, 3f, view1 -> {
            ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("JSON Data", tvJsonData.getText().toString());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(activity, "data copied to clipboard.", Toast.LENGTH_SHORT).show();
        });

        BaseApplicationClass.getClickEventClient().setClickListener(ivShare, 3f, view1 -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, tvJsonData.getText().toString());
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, "Share JSON via ...");
            activity.startActivity(shareIntent);
        });
        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnStopRequest, 3f, view12 -> {
            alertDialog.dismiss();
            dataModelCall.cancel();
            jsonViewListener.onStop(model, "Request Stopped by user");
        });
        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnProceedRequest, 3f, view12 -> {
            alertDialog.dismiss();
            jsonViewListener.onProceed(dataModelCall, model);
        });

        if (isShow) {
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        } else {
            jsonViewListener.onProceed(dataModelCall, model);
        }

    }

    @SuppressLint("SetTextI18n")
    public static <T, U> void showDialogForJSONViewWithResponse(Activity activity, boolean isShow, Call<DataModel> dataModelCall, T model, U returnResponse) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
        @SuppressLint("InflateParams")
        View view = ((LayoutInflater) Objects.requireNonNull(activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE))).inflate(R.layout.custom_json_view_with_return_response, null, false);

        TextView tvDialogTitle = view.findViewById(R.id.CustomJSONView_tvTitle);
        ImageView ivCopyToClipBtn = view.findViewById(R.id.CustomJSONView_ivCopy);
        TextView tvJsonData = view.findViewById(R.id.CustomJSONView_tvJsonData);
        TextView tvBtnProceedRequest = view.findViewById(R.id.CustomAlertDialog_tvPositiveBtn);

        builder.setView(view);

        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;

        Rect displayRectangle = new Rect();
        Window window = (activity).getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.95);
        int height = (int) (displayRectangle.height() * 1);


        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        String Url = dataModelCall.request().url().toString();
        String HTTPMethod = dataModelCall.request().method().toUpperCase().toString();
        String headers = dataModelCall.request().headers().toString();

        tvDialogTitle.setText("JSON Data View");

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String body = "";
        if (model != null) {
            body = gson.toJson(model);
        } else {
            body = "Null Body";
        }

        String returnResponseBody = gson.toJson(returnResponse);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Url: \n" + Url + "\n\n");
        stringBuilder.append("Headers: \n" + headers + "\n\n");
        stringBuilder.append("HTTP Method: \n" + HTTPMethod + "\n\n");
        stringBuilder.append("Body: \n" + body + "\n\n\n");
        stringBuilder.append("Response: \n" + returnResponseBody + "\n\n\n");

        tvJsonData.setText(stringBuilder.toString());

        //CLick Listener
        BaseApplicationClass.getClickEventClient().setClickListener(ivCopyToClipBtn, 3f, view1 -> {
            ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("JSON Data", tvJsonData.getText().toString());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(activity, "data copied to clipboard.", Toast.LENGTH_SHORT).show();
        });
        BaseApplicationClass.getClickEventClient().setClickListener(tvBtnProceedRequest, 3f, view12 -> {
            alertDialog.dismiss();
        });

        if (isShow) {
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }

    }

    public interface JSONViewListener<T> {
        void onStop(T requestModel, String msg);

        void onProceed(Call<DataModel> dataModelCall, T requestModel);
    }
}
