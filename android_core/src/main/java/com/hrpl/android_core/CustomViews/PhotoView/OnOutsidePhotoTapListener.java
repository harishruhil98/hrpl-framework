package com.hrpl.android_core.CustomViews.PhotoView;

import android.widget.ImageView;

public interface OnOutsidePhotoTapListener {

    /**
     * The outside of the photo has been tapped
     */
    void onOutsidePhotoTap(ImageView imageView);
}
