package com.hrpl.android_core.CustomViews;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class CustomKeyBoard {

    private static CustomKeyBoard customKeyBoard;

    public static CustomKeyBoard getInstance(){
        if (customKeyBoard == null) {
            customKeyBoard =  new CustomKeyBoard();
        }
        return customKeyBoard;
    }

    private CustomKeyBoard() {
    }

    public void openKeyBoard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }

    public void closeKeyBoard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }
}
