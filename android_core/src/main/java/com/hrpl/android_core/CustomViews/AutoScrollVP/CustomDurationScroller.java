package com.hrpl.android_core.CustomViews.AutoScrollVP;
import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;


public class CustomDurationScroller  extends Scroller {

    private int mDuration;

    public CustomDurationScroller(Context context) {
        super(context);
    }

    public CustomDurationScroller(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        startScroll(startX, startY, dx, dy, mDuration);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, mDuration);
    }


    public int getMduration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }
}