package com.hrpl.android_core.CustomViews;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.material.snackbar.Snackbar;
import com.hrpl.android_core.R;
import com.hrpl.android_core.Utility.UtilityClass;

public class CustomSnackBar {


    public interface onCLickListener {
        void onCLick();
    }

    private static CustomSnackBar customSnackBar;

    public static CustomSnackBar getInstance() {
        if (customSnackBar == null) {
            customSnackBar = new CustomSnackBar();
        }
        return customSnackBar;
    }

    private CustomSnackBar() {
    }

    public void showSnackBar(Activity activity, String msg, String btnLabel, onCLickListener onCLickListener, int color) {

        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), UtilityClass.getInstance().nullChecker(msg), Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(activity.getResources().getColor(color));
        snackbar.setAction(btnLabel, v -> onCLickListener.onCLick());
        if (!UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            snackbar.show();
        }
    }

    public void showSnackBar(Activity activity, String msg, String btnLabel, int duration, onCLickListener onCLickListener, int color) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), UtilityClass.getInstance().nullChecker(msg), Snackbar.LENGTH_LONG);
        snackbar.setDuration(duration);
        snackbar.setActionTextColor(activity.getResources().getColor(color));
        snackbar.setAction(btnLabel, v -> onCLickListener.onCLick());
        if (!UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            snackbar.show();
        }
    }

    public void showSnackBar(Activity activity, String msg, int color) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), UtilityClass.getInstance().nullChecker(msg), Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(activity.getResources().getColor(color));
        if (!UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            snackbar.show();
        }
    }

    public void showSnackBar(Activity activity, String msg, int duration, int color) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), UtilityClass.getInstance().nullChecker(msg), Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(activity.getResources().getColor(color));
        snackbar.setDuration(duration);
        if (!UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            snackbar.show();
        }
    }

    public void showSnackBar(Activity activity, String msg, boolean error) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), UtilityClass.getInstance().nullChecker(msg), Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setDuration(Snackbar.LENGTH_LONG);
        if (!UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            snackbar.show();
        }
    }

    public void showSnackBar(Activity activity, String msg, Boolean isfinish) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), UtilityClass.getInstance().nullChecker(msg), Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setDuration(Snackbar.LENGTH_LONG);
        if (!UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            snackbar.show();
        }
        if (isfinish) {
            new Handler().postDelayed(() -> {
                activity.finish();
            }, 1500);
        }
    }


    public void showSnackToast(Activity activity, @NonNull String msg, boolean isError, boolean isWarning) {
        if (isWarning) {
            isError = false;
        } else if (isError) {
            isWarning = false;
        }

        ToastViewHolder viewHolder = new ToastViewHolder(activity.getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) activity.findViewById(R.id.CustomToast_rlRootView)));

        viewHolder.tvMsg.setText(UtilityClass.getInstance().nullChecker(msg));


//        viewHolder.setActionButtonText(btnLabel, onCLickListener);

        viewHolder.isWarning(isWarning);
        viewHolder.isError(isError);

        if (isError){
            activity.onBackPressed();
        }

        //Creating the Toast object
        Toast toast = new Toast(activity);
        toast.setDuration(Toast.LENGTH_LONG);
        /*if (snackBarGravity != -5) {
            toast.setGravity(snackBarGravity, 0, 0);
        } else {*/
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
//        }
        toast.setView(viewHolder.itemView);

        if (!UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            toast.show();
        }
    }

    public void showSnackToast(Activity activity, @NonNull String msg) {
        showSnackToast(activity, msg, false, false);
    }


    public class ToastViewHolder {

        private RelativeLayout rlRootView;
        private TextView tvMsg;
        private Button actionButton;
        private ImageView ivWarning;
        private View itemView;

        public ToastViewHolder(@NonNull View itemView) {
            this.itemView = itemView;
            rlRootView = itemView.findViewById(R.id.CustomToast_rlRootView);
            tvMsg = itemView.findViewById(R.id.CustomToast_tvMsg);
            ivWarning = itemView.findViewById(R.id.CustomToast_ivWarning);
            actionButton = itemView.findViewById(R.id.CustomToast_btnButton);

            tvMsg.setTextColor(Color.WHITE);
            rlRootView.setBackgroundColor(Color.parseColor("#474646"));
            ivWarning.setVisibility(View.GONE);
            actionButton.setVisibility(View.GONE);
        }

        public void setActionButtonText(@NonNull String actionButtonText, onCLickListener onCLickListener) {
//            actionButton.setText(StaticClass.getInstance().nullChecker(actionButtonText));
//            if (!StaticClass.getInstance().nullChecker(actionButtonText).isEmpty() || onCLickListener != null) {
//                actionButton.setVisibility(View.VISIBLE);
//            } else {
//                actionButton.setVisibility(View.GONE);
//            }
//
//            if (onCLickListener != null) {
//                ApplicationClass.getClickEventClient().setClickListener(actionButton, v -> onCLickListener.onCLick());
//            }
        }

        public void setActionButtonText(@NonNull String actionButtonText) {
            setActionButtonText(actionButtonText, null);
        }

        public void isError(boolean isError) {
            if (isError) {
                ivWarning.setVisibility(View.VISIBLE);
                ivWarning.setImageResource(R.drawable.ic_warning_white);
                rlRootView.setBackgroundColor(Color.parseColor("#F15D2A"));
            }
        }

        public void isWarning(boolean isWarning) {
            if (isWarning) {
                ivWarning.setVisibility(View.VISIBLE);
                ivWarning.setImageResource(R.drawable.ic_warning);
            }
        }

    }
}
