package com.hrpl.android_core.NetworkCalls;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.hrpl.android_core.Listeners.SMSListener;

public class SMSReceiver extends BroadcastReceiver {

    private SMSListener mListener; //Debug Key = vx7AjqR4t6a

    @Override
    public void onReceive(Context context, Intent intent) {

        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            switch (status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS: {
                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    String OTP= message.replaceAll("[^0-9]", "").substring(0,4);
                    if (mListener != null) {
                        mListener.onSMSReceive(OTP);
                    }
                    break;
                }

                case CommonStatusCodes.TIMEOUT: {
                    // TimeOut is five minutes
                    if (mListener != null) {
                        mListener.onTimeOut();
                    }
                    break;
                }

                case CommonStatusCodes.API_NOT_CONNECTED: {

                    if (mListener != null) {
                        mListener.onError("API NOT CONNECTED");
                    }

                    break;
                }

                case CommonStatusCodes.NETWORK_ERROR: {

                    if (mListener != null) {
                        mListener.onError("NETWORK ERROR");
                    }

                    break;
                }

                case CommonStatusCodes.ERROR: {

                    if (mListener != null) {
                        mListener.onError("Network issue");
                    }

                    break;
                }
            }
        }

    }

    public void bindListener(SMSListener listener) {
        this.mListener = listener;
    }


}
