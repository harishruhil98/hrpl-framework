package com.hrpl.android_core.DataStorage.RoomDatabase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.hrpl.android_core.Utility.DateUtilsBase;

import java.util.List;

@Dao
public abstract class DAO {

    @Transaction
    public long insert(DB_LogTable logTable){
//        tableLimit();
        logTable.CreatedAt = DateUtilsBase.getInstance().getCurrentDate(DateUtilsBase.dateFormatServer2);

//        if (getLogByName(logTable.RequestName).size() > 0){
//            List<DB_LogTable> list = getLogByName(logTable.RequestName);
//            for (DB_LogTable table : list){
//                deleteLog(table);
//            }
//            return insertTodo(logTable);
//        }else {
//            return insertTodo(logTable);
//        }

        return insertTodo(logTable);
    }

    @Transaction
    public int updateLogTableRow(DB_LogTable logTable){
        logTable.CreatedAt = DateUtilsBase.getInstance().getCurrentDate(DateUtilsBase.dateFormatServer2);
        return updateLog(logTable);
    }

    @Query("SELECT * FROM " + MyDatabase.TB_LogTable + " WHERE RequestName = :RequestName ORDER BY ID DESC")
    public abstract List<DB_LogTable> getLogByName(String RequestName);

    @Query("SELECT * FROM " + MyDatabase.TB_LogTable + " ORDER BY ID DESC")
    public abstract List<DB_LogTable> getAllLogs();

    @Query("SELECT * FROM " + MyDatabase.TB_LogTable + " ORDER BY ID DESC LIMIT 1")
    public abstract DB_LogTable getTopOneModel();

    @Query("SELECT * FROM " + MyDatabase.TB_LogTable + " WHERE RequestController = :RequestController")
    public abstract List<DB_LogTable> getListByController(String RequestController);

    @Query("SELECT * FROM " + MyDatabase.TB_LogTable + " WHERE Id = :Id")
    public abstract DB_LogTable getLogById(int Id);

    @Query("DELETE  FROM " + MyDatabase.TB_LogTable)
    public abstract int emptyTable();


    @Update
    protected abstract int updateLog(DB_LogTable logTable);

    @Query("DELETE FROM " + MyDatabase.TB_LogTable + " WHERE ID NOT IN ( SELECT ID from " + MyDatabase.TB_LogTable + " ORDER BY ID DESC LIMIT 19)")
    protected abstract int tableLimit();

    @Insert
    protected abstract long insertTodo(DB_LogTable logTable);

    @Delete
    protected abstract int deleteLog(DB_LogTable logTable);

}
