package com.hrpl.android_core.DataStorage.RoomDatabase;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {DB_LogTable.class}, version = 2, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {

    private static MyDatabase myDatabase = null;

    public static final String DB_NAME = "logger_dev_db";
    public static final String TB_LogTable = "LogTable";

    private static synchronized MyDatabase getMyDatabase(Context context){
        if (myDatabase == null){
            myDatabase = Room.databaseBuilder(context.getApplicationContext(), MyDatabase.class, MyDatabase.DB_NAME).fallbackToDestructiveMigration().allowMainThreadQueries().build();
        }
        return myDatabase;
    }

    public static synchronized DAO getDBObject(Context context){
        return getMyDatabase(context).dao();
    }

    public abstract DAO dao();

}
