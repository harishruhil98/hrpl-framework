package com.hrpl.android_core.DataStorage.RoomDatabase;



import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = MyDatabase.TB_LogTable)
public class DB_LogTable implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int Id;

    @SerializedName("RequestName")
    @Expose()
    public String RequestName;

    @SerializedName("RequestParam")
    @Expose()
    public String RequestParam;

    @SerializedName("RequestBody")
    @Expose()
    public String RequestBody;

    @SerializedName("RequestDescription")
    @Expose()
    public String RequestDescription;

    @SerializedName("RequestResponse")
    @Expose()
    public String RequestResponse;

    @SerializedName("RequestErrorResponse")
    @Expose()
    public String RequestErrorResponse;

    @SerializedName("RequestController")
    @Expose()
    public String RequestController;

    @SerializedName("ErrorStackTree")
    @Expose()
    public String ErrorStackTree;

    @SerializedName("CreatedAt")
    @Expose()
    public String CreatedAt;

    @SerializedName("Type")
    @Expose()
    public int Type;

    public DB_LogTable() {
    }

    public DB_LogTable(String requestController) {
        RequestController = requestController;
    }
}
