package com.hrpl.android_core.DataStorage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hrpl.android_core.Models.DefaultFormatModel;
import com.hrpl.android_core.Utility.ConstantClass;
import com.hrpl.android_core.Utility.UtilityClass;

import java.lang.reflect.Type;

public class LibSharedPreference {

    private final String DATABASE = "com.google.androdw.gns.appiad";
    private final String System_DATABASE = "com.google.andrwod.gns.appiad.system";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences sharedPreferencesSystem;
    private static LibSharedPreference libSharedPreference;
    private Context context;

    public LibSharedPreference(Context context){
        this.context = context;
    }

    public static LibSharedPreference getInstance(Context context){
        if (libSharedPreference == null) {
            return new LibSharedPreference(context);
        }
        return  libSharedPreference;
    }

    private SharedPreferences buildSharedPreference(){
        if (sharedPreferences == null) {
            try {
                String masterKeyAlias = MasterEncryption.getOrCreate(MasterKeys.AES256_GCM_SPEC);
                sharedPreferences = EncryptedSharedPreferences.create(
                        DATABASE,
                        masterKeyAlias,
                        context,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return sharedPreferences;
    }

    public boolean isDevMode() {
        return getBoolean("isDevMode");
    }

    public void setDevMode(boolean isDevMode){
        setBoolean("isDevMode", isDevMode);
    }

    private SharedPreferences buildSystemSharedPreference(){
        if (sharedPreferencesSystem == null) {
            try {
                String masterKeyAlias = MasterEncryption.getOrCreate(MasterKeys.AES256_GCM_SPEC);
                sharedPreferencesSystem = EncryptedSharedPreferences.create(
                        System_DATABASE,
                        masterKeyAlias,
                        context,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return sharedPreferencesSystem;
    }

    protected SharedPreferences initializeSharedPreference() {
        return buildSharedPreference();
    }

    protected SharedPreferences initializeSystemSharedPreference() {
        return buildSystemSharedPreference();
    }

    public void setLoginStatus(boolean status) {
        initializeSharedPreference().edit().putBoolean("loginStatus" + DATABASE, status).apply();
    }

    public boolean getLoginStatus() {
        return initializeSharedPreference().getBoolean("loginStatus" + DATABASE, false);
    }

    public void logout(Activity activity, Class<?> destinationClass) {
        setLoginStatus(false);
        new Handler().postDelayed(() -> {
            activity.startActivity(new Intent(activity, destinationClass));
            ((Activity)activity).finish();
        }, 100);
    }

    public void setJSONViewerDisplayStatus(boolean status) {
        initializeSharedPreference().edit().putBoolean("JSONViewerDisplayStatus" + DATABASE, status).apply();
    }

    public boolean getJSONViewerDisplayStatus() {
        return getUserJsonViewerEnabled() && getLoginStatus() && initializeSharedPreference().getBoolean("JSONViewerDisplayStatus" + DATABASE, false);
    }

    public void setUserJsonViewerEnabled(boolean status) {
        initializeSharedPreference().edit().putBoolean("UserJsonViewer" + DATABASE, status).apply();
    }

    public boolean getUserJsonViewerEnabled() {
        return initializeSharedPreference().getBoolean("UserJsonViewer" + DATABASE, true);
    }

    public void setGuideLineViewStatus(boolean status) {
        initializeSharedPreference().edit().putBoolean("GuideLineViewStatus" + DATABASE, status).apply();
    }

    public boolean getProfileCompleteStatus() {
        return initializeSharedPreference().getBoolean("ProfileCompleteStatus" + DATABASE, false);
    }

    public void setURL(String url) {
        initializeSharedPreference().edit().putString("urlValues" + DATABASE, UtilityClass.getInstance().nullChecker(url)).apply();
    }

    public String getURL() {
        return initializeSharedPreference().getString("urlValues" + DATABASE, ConstantClass.domainUrl);
    }

    public void setCustomKey(String CustomKey) {
        initializeSharedPreference().edit().putString("customKey" + DATABASE, UtilityClass.getInstance().nullChecker(CustomKey)).apply();
    }

    public String getCustomKey() {
        return initializeSharedPreference().getString("customKey" + DATABASE, "");
    }

    public void setIsRegisteredStatus(boolean status) {
        initializeSystemSharedPreference().edit().putBoolean("IsRegisteredStatus" + DATABASE, status).apply();
    }

    public boolean getIsRegisteredStatus() {
        return initializeSystemSharedPreference().getBoolean("IsRegisteredStatus" + DATABASE, false);
    }

    public void setLoginMobileId(String loginId) {
        initializeSystemSharedPreference().edit().putString("LoginMobileId" + DATABASE, loginId).apply();
    }

    public String getLoginMobileId() {
        return initializeSystemSharedPreference().getString("LoginMobileId" + DATABASE, "");
    }

    public void setPassword(String Password) {
        initializeSystemSharedPreference().edit().putString("Password" + DATABASE, Password).apply();
    }

    public String getPassword() {
        return initializeSystemSharedPreference().getString("Password" + DATABASE, "");
    }

    public void setLCode(String LCode) {
        initializeSystemSharedPreference().edit().putString("LCode" + DATABASE, LCode).apply();
    }

    public String getLCode() {
        return initializeSystemSharedPreference().getString("LCode" + DATABASE, "en");
    }

    public void setUserId(int userId) {
        initializeSharedPreference().edit().putInt("userId" + DATABASE, userId).apply();
    }

    public int getUserId() {
        return initializeSharedPreference().getInt("userId" + DATABASE, 0);
    }

    public void setDeviceId(String deviceId) {
        initializeSharedPreference().edit().putString("deviceId" + DATABASE, UtilityClass.getInstance().nullChecker(deviceId)).apply();
    }

    public String getDeviceId() {
        return initializeSharedPreference().getString("deviceId" + DATABASE, "");
    }

    public void setDeviceToken(String deviceToken) {
        initializeSharedPreference().edit().putString("DeviceToken" + DATABASE, UtilityClass.getInstance().nullChecker(deviceToken)).apply();
    }

    public String getDeviceToken() {
        String token = initializeSharedPreference().getString("DeviceToken" + DATABASE, "");

        if (UtilityClass.getInstance().nullChecker(token).isEmpty()) {
            return "";
        } else {
            token = "Bearer " + token;
            return token;
        }
    }

    public void clearAllValues() {
        String phoneNo = getMobileNo();
        String password = getPassword();
        context.getSharedPreferences(DATABASE, 0).edit().clear().apply();
        setMobileNo(phoneNo);
        setPassword(password);
    }

    public void clearAllSystemValues() {
        context.getSharedPreferences(System_DATABASE, 0).edit().clear().apply();
    }

    public void setLoginId(String username) {
        initializeSharedPreference().edit().putString("username" + DATABASE, UtilityClass.getInstance().nullChecker(username)).apply();
    }

    public String getLoginId() {
        return initializeSharedPreference().getString("username" + DATABASE, "");
    }

    public void setName(String name) {
        initializeSharedPreference().edit().putString("name" + DATABASE, UtilityClass.getInstance().nullChecker(name)).apply();
    }

    public String getName() {
        return initializeSharedPreference().getString("name" + DATABASE, "");
    }

    public void setMobileNo(String MobileNo) {
        initializeSharedPreference().edit().putString("MobileNo" + DATABASE, UtilityClass.getInstance().nullChecker(MobileNo)).apply();
    }

    public String getMobileNo() {
        return initializeSharedPreference().getString("MobileNo" + DATABASE, "");
    }

    public void setEmailId(String EmailId) {
        initializeSharedPreference().edit().putString("EmailId" + DATABASE, UtilityClass.getInstance().nullChecker(EmailId)).apply();
    }

    public String getEmailId() {
        return initializeSharedPreference().getString("EmailId" + DATABASE, "");
    }

    public void setUserImageUrl(String UserImageUrl) {
        initializeSharedPreference().edit().putString("UserImageUrl" + DATABASE, UtilityClass.getInstance().nullChecker(UserImageUrl)).apply();
    }

    public String getUserImageUrl() {
        return initializeSharedPreference().getString("UserImageUrl" + DATABASE, "");
    }

    public void setPlayerId(String PlayerId) {
        initializeSharedPreference().edit().putString("PlayerId" + DATABASE, UtilityClass.getInstance().nullChecker(PlayerId)).apply();
    }

    public String getPlayerId() {
        return initializeSharedPreference().getString("PlayerId" + DATABASE, "");
    }

    public void setLanguage(String LanguageCode) {
        initializeSystemSharedPreference().edit().putString("Language" + DATABASE, UtilityClass.getInstance().nullChecker(LanguageCode)).apply();
    }

    public String getLanguage() {
        return initializeSystemSharedPreference().getString("Language" + DATABASE, "");
    }

    public void setProfileId(int profileId) {
        initializeSharedPreference().edit().putInt("profileId" + DATABASE, profileId).apply();
    }

    public int getProfileId() {
        return initializeSharedPreference().getInt("profileId" + DATABASE, 0);
    }
    public void setBoolean(@NonNull String key, boolean value){
        initializeSharedPreference().edit().putBoolean( UtilityClass.getInstance().nullChecker(key) + DATABASE, value).apply();
    }

    public boolean getBoolean(@NonNull String key){
        return initializeSharedPreference().getBoolean(key + DATABASE, false);
    }

    public void setInteger(@NonNull String key, int value){
        initializeSharedPreference().edit().putInt( UtilityClass.getInstance().nullChecker(key) + DATABASE, value).apply();
    }

    public int getInteger(@NonNull String key){
        return initializeSharedPreference().getInt(key + DATABASE, 0);
    }

    public void setString(@NonNull String key, @NonNull String value){
        initializeSharedPreference().edit().putString( UtilityClass.getInstance().nullChecker(key) + DATABASE, UtilityClass.getInstance().nullChecker(value)).apply();
    }

    public String getString(@NonNull String key){
        return initializeSharedPreference().getString(key + DATABASE, "");
    }

    public <T> void setBody(@NonNull String key, T body){
        Gson gson = new Gson();
        if (body != null) {
            String json = gson.toJson(body);
            setString(key, json);
        }
    }

    public <T> T getBody(String key, T defaultValue) {
        String json = getString(key);
        Type type = new TypeToken<T>() {}.getType();
        Gson gson = new Gson();
        T t;

        if (!UtilityClass.getInstance().nullChecker(json).isEmpty() && !UtilityClass.getInstance().nullChecker(json).equalsIgnoreCase("null")) {
            t = gson.fromJson(json, type);
        } else {
            t = defaultValue;
        }
        return t;

    }

    /*public <T> getBody(String key, T t){

        String json = initializeSharedPreference().getString(key, null);
        Type type = new TypeToken<DefaultFormatModel>() {
        }.getType();
        DefaultFormatModel defaultFormatModel;
        Gson gson = new Gson();
        if (!nullChecker(json).isEmpty() && !nullChecker(json).equalsIgnoreCase("null")) {
            defaultFormatModel = gson.fromJson(json, type);
        } else {
            defaultFormatModel = new DefaultFormatModel();
        }
        return defaultFormatModel;
    }
*/

}
