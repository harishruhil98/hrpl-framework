package com.hrpl.android_core.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.Serializable;
import java.util.ArrayList;



public class DefaultFormatModel extends DefaultFormatModelParent implements Serializable {

    @SerializedName("equipments")
    @Expose()
    private ArrayList<String> equipments;

    @SerializedName("provinces")
    @Expose()
    private ArrayList<String> provinces;

    @SerializedName("webviewLink")
    @Expose()
    private String webViewLink;

    @SerializedName("adminNumbers")
    @Expose()
    private String adminNumbers;

    @SerializedName("documentBaseURL")
    @Expose()
    private String documentBaseURL;

    @SerializedName("licenseBaseURL")
    @Expose()
    private String licenseBaseURL;

    @SerializedName("ticketBaseURL")
    @Expose()
    private String ticketBaseURL;

    @SerializedName("isActiveLiveLoad")
    @Expose()
    private boolean isActiveLiveLoad;

    @SerializedName("isActiveLiveTruck")
    @Expose()
    private boolean isActiveLiveTruck;

    @SerializedName("showPublicAlerts")
    @Expose()
    private boolean showPublicAlerts;

    @SerializedName("adminMarque")
    @Expose()
    private String adminMarque;

    public String getLicenseBaseURL() {
        return UtilityClass.getInstance().nullChecker(licenseBaseURL);
    }

    public String getDocumentBaseURL() {
        return UtilityClass.getInstance().nullChecker(documentBaseURL);
    }

    public ArrayList<String> getEquipments() {
        return equipments == null ? new ArrayList<>() : equipments;
    }

    public void setEquipments(ArrayList<String> equipments) {
        this.equipments = equipments == null ? new ArrayList<>() : equipments;
    }

    public ArrayList<String> getProvince() {
        return provinces == null ? new ArrayList<>() : provinces;
    }

    public void setProvince(ArrayList<String> provinces) {
        this.provinces = provinces == null ? new ArrayList<>() : provinces;
    }

    public String getTicketBaseURL() {
        return UtilityClass.getInstance().nullChecker(ticketBaseURL);
    }

    public void setTicketBaseURL(String ticketBaseURL) {
        this.ticketBaseURL = UtilityClass.getInstance().nullChecker(ticketBaseURL);
    }

    public String getAdminMarque() {
        return UtilityClass.getInstance().nullChecker(adminMarque);
    }

    public void setAdminMarque(String adminMarque) {
        this.adminMarque = UtilityClass.getInstance().nullChecker(adminMarque);
    }

    public boolean getIsActiveLiveLoad() {
//        return true;
        return isActiveLiveLoad;
    }

    public void setIsActiveLiveLoad(boolean isActiveLiveLoad) {
        this.isActiveLiveLoad = isActiveLiveLoad;
    }

    public boolean getIsActiveLiveTruck() {
//        return true;
        return isActiveLiveTruck;
    }

    public void setIsActiveLiveTruck(boolean isActiveLiveTruck) {
        this.isActiveLiveTruck = isActiveLiveTruck;
    }

    public boolean getIsShowPublicAlerts() {
//        return true;
        return showPublicAlerts;
    }

    public void setIsShowPublicAlerts(boolean showPublicAlerts) {
        this.isActiveLiveTruck = showPublicAlerts;
    }
}
