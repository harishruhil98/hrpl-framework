package com.hrpl.android_core.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.Serializable;

public class LanguageModel implements Serializable {

    @SerializedName("language_id")
    @Expose()
    private int languageId;

    @SerializedName("language_name")
    @Expose()
    private String languageName;

    @SerializedName("language_code")
    @Expose()
    private String languageCode;

    @SerializedName("is_selected")
    @Expose()
    private boolean isSelected;

    public LanguageModel() {
    }

    public LanguageModel(int languageId, String languageName, String languageCode, boolean isSelected) {
        this.languageId = languageId;
        this.languageName = UtilityClass.getInstance().nullChecker(languageName);
        this.languageCode = UtilityClass.getInstance().nullChecker(languageCode);
        this.isSelected = isSelected;
    }

    public LanguageModel(int languageId, String languageName, String languageCode) {
        this.languageId = languageId;
        this.languageName = UtilityClass.getInstance().nullChecker(languageName);
        this.languageCode = UtilityClass.getInstance().nullChecker(languageCode);
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    public String getLanguageName() {
        return UtilityClass.getInstance().nullChecker(languageName);
    }

    public void setLanguageName(String languageName) {
        this.languageName = UtilityClass.getInstance().nullChecker(languageName);
    }

    public String getLanguageCode() {
        return UtilityClass.getInstance().nullChecker(languageCode);
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = UtilityClass.getInstance().nullChecker(languageCode);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
