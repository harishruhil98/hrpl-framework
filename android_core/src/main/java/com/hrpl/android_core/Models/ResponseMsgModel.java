package com.hrpl.android_core.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.Serializable;



public class ResponseMsgModel implements Serializable {

    @SerializedName("isError")
    @Expose()
    private boolean isError;

    @SerializedName("errorMessage")
    @Expose()
    private String errorMessage;

    @SerializedName("isWarning")
    @Expose()
    private boolean isWarning;

    @SerializedName("isEmptyCollection")
    @Expose()
    private boolean isEmptyCollection;

    @SerializedName("warningMessage")
    @Expose()
    private String warningMessage;

    @SerializedName("successMessage")
    @Expose()
    private String successMessage;

    public boolean isError() {
        return isError;
    }

    public String getErrorMessage() {
        return UtilityClass.getInstance().nullChecker(errorMessage);
    }

    public boolean isWarning() {
        return isWarning;
    }

    public boolean isEmptyCollection() {
        return isEmptyCollection;
    }

    public String getWarningMessage() {
        return UtilityClass.getInstance().nullChecker(warningMessage);
    }

    public String getSuccessMessage() {
        return UtilityClass.getInstance().nullChecker(successMessage);
    }
}
