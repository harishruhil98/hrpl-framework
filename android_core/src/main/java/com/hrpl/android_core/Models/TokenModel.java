package com.hrpl.android_core.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.Serializable;



public class TokenModel implements Serializable {

    @SerializedName("token")
    @Expose()
    private String token;

    @SerializedName("token_created_on")
    @Expose()
    private String token_created_on;

    @SerializedName("token_expired_on")
    @Expose()
    private String token_expired_on;

    @SerializedName("token_validity_hours")
    @Expose()
    private int token_validity_hours;

    @SerializedName("referer_url")
    @Expose()
    private String referer_url;

    public String getToken() {
        return UtilityClass.getInstance().nullChecker(token);
    }

    public String getToken_created_on() {
        return UtilityClass.getInstance().nullChecker(token_created_on);
    }

    public String getToken_expired_on() {
        return UtilityClass.getInstance().nullChecker(token_expired_on);
    }

    public int getToken_validity_hours() {
        return token_validity_hours;
    }

    public String getReferer_url() {
        return UtilityClass.getInstance().nullChecker(referer_url);
    }
}
