package com.hrpl.android_core.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;



public class ResponseDataModel implements Serializable {

    @SerializedName("data")
    @Expose()
    public Object data;

    @SerializedName("isObject")
    @Expose()
    public boolean isObject;

    @SerializedName("isCollection")
    @Expose()
    public boolean isCollection;

    @SerializedName("responseDataType")
    @Expose()
    public String responseDataType;

}
