package com.hrpl.android_core.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrpl.android_core.Utility.DateUtilsBase;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.Serializable;
import java.util.Date;

public class CommonPropertiesModel implements Serializable {

    public void setIsActive(boolean IsActive) {
        this.is_active = IsActive;
    }

    public void setCreatedOn(String createdOn, String inputFormat) {
        this.created_on = DateUtilsBase.getInstance().formatDate(createdOn, inputFormat, DateUtilsBase.dateFormatServer2);
    }

    public void setCreatedBy(String createdBy) {
        this.created_by = UtilityClass.getInstance().nullChecker(createdBy);
    }

    public void setModifiedOn(String modified_on, String inputFormat) {
        this.modified_on = DateUtilsBase.getInstance().formatDate(modified_on, inputFormat, DateUtilsBase.dateFormatServer2);
    }

    public void setModifiedBy(String modifiedBy) {
        this.modified_by = UtilityClass.getInstance().nullChecker(modifiedBy);
    }

    public boolean getIsActive() {
        return is_active;
    }

    public String getCreatedOn(String outputFormat) {
        return DateUtilsBase.getInstance().formatDate(UtilityClass.getInstance().nullChecker(created_on), DateUtilsBase.dateFormatServer2, outputFormat);
    }

    public Date getCreatedOn() {
        return DateUtilsBase.getInstance().formatDate(created_on, DateUtilsBase.dateFormatServer2);
    }

    public String getCreatedBy() {
        return UtilityClass.getInstance().nullChecker(created_by);
    }

    public String getModifiedOn(String outputFormat) {
        return DateUtilsBase.getInstance().formatDate(UtilityClass.getInstance().nullChecker(modified_on), DateUtilsBase.dateFormatServer2, outputFormat);
    }

    public Date getModifiedOn() {
        return DateUtilsBase.getInstance().formatDate(modified_on, DateUtilsBase.dateFormatServer2);
    }

    public String getModifiedBy() {
        return UtilityClass.getInstance().nullChecker(modified_by);
    }

    public String getUserIp() {
        return UtilityClass.getInstance().nullChecker(user_ip);
    }

    public String getGUID() {
        return UtilityClass.getInstance().nullChecker(guid);
    }

    @SerializedName("is_active")
    @Expose()
    private boolean is_active;

    @SerializedName("created_on")
    @Expose()
    private String created_on;

    @SerializedName("created_by")
    @Expose()
    private String created_by;

    @SerializedName("modified_on")
    @Expose()
    private String modified_on;

    @SerializedName("modified_by")
    @Expose()
    private String modified_by;

    @SerializedName("user_ip")
    @Expose()
    private String user_ip;

    @SerializedName("guid")
    @Expose()
    private String guid;
}
