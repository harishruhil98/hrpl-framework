package com.hrpl.android_core.Models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.Serializable;

public class SelectedStringModel implements Serializable {

    @SerializedName("stringValue")
    @Expose()
    private String stringValue;

    @SerializedName("isSelected")
    @Expose()
    private boolean isSelected;

    @SerializedName("isActive")
    @Expose()
    private boolean isActive;

    public String getStringValue() {
        return UtilityClass.getInstance().nullChecker(stringValue);
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public SelectedStringModel(String stringValue, boolean isSelected, boolean isActive) {
        this.stringValue = UtilityClass.getInstance().nullChecker(stringValue);
        this.isSelected = isSelected;
        this.isActive = isActive;
    }

    public SelectedStringModel(String stringValue) {
        this.stringValue = UtilityClass.getInstance().nullChecker(stringValue);
    }

    public void setStringValue(@NonNull  String stringValue) {
        this.stringValue = UtilityClass.getInstance().nullChecker(stringValue);
    }

    public void setIsSelected(boolean selected) {
        isSelected = selected;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }
}
