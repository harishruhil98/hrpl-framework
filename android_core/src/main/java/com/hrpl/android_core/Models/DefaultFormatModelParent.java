package com.hrpl.android_core.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.Serializable;
import java.util.ArrayList;



public class DefaultFormatModelParent implements Serializable {

    @SerializedName("dateFormat")
    @Expose()
    private String dateFormat;

    @SerializedName("timeFormat")
    @Expose()
    private String timeFormat;

    @SerializedName("available_date_formats")
    @Expose()
    private ArrayList<String> availableDateFormatsList;

    @SerializedName("available_time_formats")
    @Expose()
    private ArrayList<String> availableTimeFormats;

    @SerializedName("years")
    @Expose()
    private ArrayList<String> years;

    @SerializedName("paymentModes")
    @Expose()
    private ArrayList<String> paymentsModes;

    @SerializedName("phoneFormat")
    @Expose()
    private String phoneFormat;

    @SerializedName("currency")
    @Expose()
    private String currency;

    @SerializedName("currencyFormat")
    @Expose()
    private String currencyFormat;

    @SerializedName("localStartOfWeek")
    @Expose()
    private int localStartOfWeek;

    @SerializedName("dealsOfferVisible")
    @Expose()
    private boolean dealsOfferVisible;

    @SerializedName("showCustomNotification")
    @Expose()
    private boolean showCustomNotification;

    @SerializedName("notificationType")
    @Expose()
    private String notificationType;

    @SerializedName("notificationValues")
    @Expose()
    private String notificationValues;

    @SerializedName("notificationImageUrl")
    @Expose()
    private String notificationImageUrl;

    @SerializedName("customerCareNumber")
    @Expose()
    private String customerCareNumber;

    @SerializedName("isRefreshImageCache")
    @Expose()
    private String isRefreshImageCache;

    @SerializedName("enableMultiCategory")
    @Expose()
    private int enableMultiCategory;

    @SerializedName("baseURL")
    @Expose()
    private String baseURL;

    public String getBaseURL() {
        return UtilityClass.getInstance().nullChecker(baseURL);
    }

    public String getIsRefreshImageCache() {
        if (UtilityClass.getInstance().nullChecker(isRefreshImageCache).contains("0") || UtilityClass.getInstance().nullChecker(isRefreshImageCache).contains("1")) {
            return isRefreshImageCache;
        } else {
            return "0";
        }
    }


    public String getDateFormat() {
        return UtilityClass.getInstance().nullChecker(dateFormat);
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = UtilityClass.getInstance().nullChecker(dateFormat);
    }


    public String getTimeFormat() {
        return UtilityClass.getInstance().nullChecker(timeFormat);
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = UtilityClass.getInstance().nullChecker(timeFormat);
    }


    public String getPhoneFormat() {
        return UtilityClass.getInstance().nullChecker(phoneFormat);
    }

    public void setPhoneFormat(String phoneFormat) {
        this.phoneFormat = UtilityClass.getInstance().nullChecker(phoneFormat);
    }


    public String getCurrency() {
        return UtilityClass.getInstance().nullChecker(currency);
    }

    public void setCurrency(String currency) {
        this.currency = UtilityClass.getInstance().nullChecker(currency);
    }


    public String getCurrencyFormat() {
        return UtilityClass.getInstance().nullChecker(currencyFormat);
    }

    public void setCurrencyFormat(String currencyFormat) {
        this.currencyFormat = UtilityClass.getInstance().nullChecker(currencyFormat);
    }


    public int getLocalStartOfWeek() {
        return localStartOfWeek;
    }

    public void setLocalStartOfWeek(int localStartOfWeek) {
        this.localStartOfWeek = localStartOfWeek;
    }


    public boolean isShowCustomNotification() {
        return showCustomNotification;
    }

    public void setShowCustomNotification(boolean showCustomNotification) {
        this.showCustomNotification = showCustomNotification;
    }


    public String getNotificationType() {
        return UtilityClass.getInstance().nullChecker(notificationType);
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = UtilityClass.getInstance().nullChecker(notificationType);
    }


    public String getNotificationValues() {
        return UtilityClass.getInstance().nullChecker(notificationValues);
    }

    public void setNotificationValues(String notificationValues) {
        this.notificationValues = UtilityClass.getInstance().nullChecker(notificationValues);
    }


    public boolean getDealsOfferVisibility() {
        return dealsOfferVisible;
    }

    public void setDealsOfferVisibility(boolean dealsOfferVisibility) {
        this.dealsOfferVisible = dealsOfferVisibility;
    }


    public String getNotificationImageUrl() {
        return UtilityClass.getInstance().nullChecker(notificationImageUrl);
    }

    public void setNotificationImageUrl(String notificationImageUrl) {
        this.notificationImageUrl = UtilityClass.getInstance().nullChecker(notificationImageUrl);
    }


    public String getCustomerCareNumber() {
        return UtilityClass.getInstance().nullChecker(customerCareNumber);
    }

    public void setCustomerCareNumber(String customerCareNumber) {
        this.customerCareNumber = UtilityClass.getInstance().nullChecker(customerCareNumber);
    }


    public ArrayList<String> getAvailableTimeFormats() {
        return availableTimeFormats == null ? new ArrayList<>() : availableTimeFormats;
    }

    public ArrayList<String> getAvailableDateFormatsList() {
        return availableDateFormatsList == null ? new ArrayList<>() : availableDateFormatsList;
    }


    public ArrayList<String> getYears() {
        return years == null ? new ArrayList<>() : years;
    }

    public void setYears(ArrayList<String> years) {
        this.years = years == null ? new ArrayList<>() : years;
    }


    public ArrayList<String> getPaymentsModes() {
        return paymentsModes == null ? new ArrayList<>() : paymentsModes;
    }

    public void setPaymentsModes(ArrayList<String> paymentsModes) {
        this.paymentsModes = paymentsModes == null ? new ArrayList<>() : paymentsModes;
    }
}
