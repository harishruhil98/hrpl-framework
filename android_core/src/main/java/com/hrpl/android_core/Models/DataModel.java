package com.hrpl.android_core.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrpl.android_core.Utility.UtilityClass;

import java.io.Serializable;
import java.util.TreeMap;

public class DataModel implements Serializable {

    @SerializedName("doLogOut")
    @Expose()
    private boolean doLogOut;

    @SerializedName("languageCode")
    @Expose()
    private String languageCode;

    @SerializedName("responseData")
    @Expose()
    private TreeMap<String, Object> responseData;

    @SerializedName("responseMsg")
    @Expose()
    private ResponseMsgModel responseMsgModel;

    public boolean isDoLogOut() {
        return doLogOut;
    }

    public String getLanguageCode() {
        return UtilityClass.getInstance().nullChecker(languageCode);
    }

    public TreeMap<String, Object> getResponseData() {
        return responseData == null ? new TreeMap<>() : responseData;
    }

    public ResponseMsgModel getResponseMsgModel() {
        return responseMsgModel == null ? new ResponseMsgModel() : responseMsgModel;
    }
}

