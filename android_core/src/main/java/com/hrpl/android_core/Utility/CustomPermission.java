package com.hrpl.android_core.Utility;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

import static android.os.Build.VERSION.SDK_INT;

public class CustomPermission {

    public CustomPermission() {
    }

    private static CustomPermission customPermission;

    public static CustomPermission getInstance() {
        if (customPermission == null) {
            customPermission = new CustomPermission();
        }
        return customPermission;
    }

    public boolean checkStoragePermissionAccess(Activity activity, int requestCode) {
        //Check Permission for android 11
        if (SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                try {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                    intent.addCategory("android.intent.category.DEFAULT");
                    intent.setData(Uri.parse(String.format("package:%s", activity.getApplicationContext().getPackageName())));
                    activity.startActivityForResult(intent, requestCode);
                } catch (Exception e) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                    activity.startActivityForResult(intent, requestCode);
                }
                return false;
            } else {
                return true;
            }
            //Check permission for below Android 11
        } else if (ContextCompat.checkSelfPermission((Activity) activity, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission((Activity) activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) activity, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale((Activity) activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Snackbar.make(activity.findViewById(android.R.id.content), "Please Grant Permissions for smooth flow", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE", v ->
                        activity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode)
                ).show();
            } else {
                activity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
            }
            return false;
        } else {
            return true;
        }
    }
}