package com.hrpl.android_core.Utility;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationUtility {

    private Activity activity;
    private ProgressDialog progressDialog;
    private LocationCallback mLocationCallback;
    private LocationUpdateCallback locationUpdateCallback;

    private final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;

    public LocationUtility(Activity activity, LocationUpdateCallback locationUpdateCallback) {
        this.activity = activity;
        this.locationUpdateCallback = locationUpdateCallback;

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        checkPermission();
    }

    @SuppressLint("MissingPermission")
    private void getAccurateLocation() {
        progressDialog.show();

        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        SettingsClient mSettingsClient = LocationServices.getSettingsClient(activity);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                progressDialog.dismiss();
                Location mCurrentLocation = locationResult.getLastLocation();

                Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1);

                    String address ="", city = "", state= "", country="", postalCode= "", knownName = "";

                    if (addresses.size() > 0 ) {
                        address = addresses.get(0).getAddressLine(0);
                        city = addresses.get(0).getLocality();
                        state = addresses.get(0).getAdminArea();
                        country = addresses.get(0).getCountryName();
                        postalCode = addresses.get(0).getPostalCode();
                        knownName = addresses.get(0).getFeatureName();
                    }

                    locationUpdateCallback.onSuccess(new LocationMaster(address, city, state, country, postalCode, knownName,mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));

                } catch (IOException e) {
                    e.printStackTrace();
                    locationUpdateCallback.onError(e.getLocalizedMessage());
                }

                mFusedLocationClient.removeLocationUpdates(mLocationCallback).addOnCompleteListener(activity, task -> { });
            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest).addOnSuccessListener(
                activity, locationSettingsResponse -> mFusedLocationClient.requestLocationUpdates(
                        mLocationRequest, mLocationCallback, Looper.myLooper())
        ).addOnFailureListener(activity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException rae = (ResolvableApiException) e;
                            rae.startResolutionForResult(activity, 100);
                        } catch (IntentSender.SendIntentException sie) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings.";
                        Log.e("TAG", errorMessage);
                }
                locationUpdateCallback.onError(e.getMessage());
            }
        });
    }

    public interface LocationUpdateCallback{
        void onSuccess(LocationMaster location);
        void onError(String msg);
    }

    public class LocationMaster  {
        private String address, city, state, country, postalCode, knownName;
        private double latitude, longitude;

        public LocationMaster(String address, String city, String state, String country, String postalCode, String knownName, double latitude, double longitude) {
            this.address = UtilityClass.getInstance().nullChecker(address);
            this.city = UtilityClass.getInstance().nullChecker(city);
            this.state = UtilityClass.getInstance().nullChecker(state);
            this.country = UtilityClass.getInstance().nullChecker(country);
            this.postalCode = UtilityClass.getInstance().nullChecker(postalCode);
            this.knownName = UtilityClass.getInstance().nullChecker(knownName);
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public LocationMaster() {
        }

        public String getAddress() {
            return nullChecker(address);
        }

        public String getCity() {
            return nullChecker(city);
        }

        public String getState() {
            return nullChecker(state);
        }

        public String getCountry() {
            return nullChecker(country);
        }

        public String getPostalCode() {
            return nullChecker(postalCode);
        }

        public String getKnownName() {
            return nullChecker(knownName);
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }


    private String nullChecker(String string) {

        if (string == null) {
            string = "";
        }
        return string;
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) + ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Snackbar.make((activity).findViewById(android.R.id.content), "Please Grant Permissions for smooth flow", Snackbar.LENGTH_INDEFINITE).setAction("ENABLE", v -> checkPermission()).show();
            } else {
                activity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1212);
            }
        } else {
            getAccurateLocation();
        }
    }
}
