package com.hrpl.android_core.Utility;

import android.content.Context;

import com.hrpl.android_core.BaseApplicationClass;
import com.hrpl.android_core.DataStorage.LibSharedPreference;

public class ConstantClass {
    public static String domainUrl = "";
    public static String devApiUrl = "";
    public static String tokenKey = "";

    public static String OneSignalAppId = "";
    public static final String noInterNetConnection = "No internet connectivity";
    public static final String ApiNotFound = "Api Not found";
    public static final String ConnectionBroken = "Connection broken, Please check your internet connectivity";
    public static final String UnauthorizedAccess = "Unauthorized Access";
    public static final String MethodNotAllowed = "Method not allowed";
    public static final String OtherHttpError = "Other Http Error";

    public static final String defaultErrorMsg = "Seems like communication channels are slow.\n" + "Please try again in a while";

    public static String getUrl(Context context) {
        return LibSharedPreference.getInstance(context).getURL();
    }

    public static String getBaseUrl(Context context) {
        return LibSharedPreference.getInstance(context).getURL() + "api/";
    }

    public static String getBaseUrlImages(Context context) {
        return LibSharedPreference.getInstance(context).getURL() + "assets/";
    }

    public static int noDataFoundImagePath = 0;

    public static int digitBeforeDecimal = 5;
    public static int digitAfterDecimal = 2;

    public static final int Role_Admin = 100;
    public static final int Role_User = 101;

    public static final String PDF_Path = "PDF_Path";
    public static final String PDF_Title = "PDF_Title";

    public static final int REQUEST_CAMERA = 4343;
    public static final int REQUEST_Gallery = 4344;
    public static final int REQUEST_Contacts = 4348;
    public static final int REQUEST_ExternalStorage = 140;
    public static final int REQUEST_PhoneCall = 139;
    public static final int cropImageSizeX = UtilityClass.getInstance().getScreenWidth();
    public static final int cropImageSizeY = UtilityClass.getInstance().getScreenWidth() / 2;

    public static final String contentTye = "application/json";

    public static final String ParentActivity = "ParentActivity";
    public static final String Parent_DashboardActivity = "Parent_DashboardActivity";

    public static final String status_OK = "ok";
    public static final String status_Not_found = "not_found";
    public static final String status_found = "found";
    public static final String type = "type";
    public static final String intent_action = "action";
    public static final String intent_object = "object";
    public static final String intent_flag = "flag";
    public static final String intent_msg = "intentMsg";
    public static final String position = "Position";

    public static final String regexNumberOnly = "[0-9]";

    // The given argument to compile() method
    // is regular expression. With the help of
    // regular expression we can validate mobile
    // number.
    // 1) Begins with 0 or 91
    // 2) Then contains 7 or 8 or 9.
    // 3) Then contains 9 digits
    public static final String regexValidMobileNo = "(0/91)?[7-9][0-9]{9}";
    public static final String regexValidPrefixCount = "(1/2/3/4/5/6/7/8/9)?[0-9]";
        public static final String regexValidEmail = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2})$";
//    public static final String regexValidEmail = "^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(com|uk|in||name))$";
//    public static final String regexValidAlphaNumericOnly = "^[a-zA-Z0-9]*$";
    public static final String regexValidAlphaNumericOnly = "^[a-zA-Z0-9_ ]+$";
    public static final String regexValidAlphaNumericOnlywithDot = "^[a-zA-Z0-9_. ]+$";
    public static final String regexValidName = "^[\\p{L} .'-]+$";
    public static final String regexValidAddress = "^[\\p{L} .'-]+$";


    private static long lastClickTime = 0;
    private static final long delay = 1500;

}
