package com.hrpl.android_core.Utility;

import android.text.InputFilter;
import android.text.Spanned;

public class CustomInputFilter {

    public static CustomInputFilter getInstance() {
        return new CustomInputFilter();
    }

    public InputFilter getDecimalInputFilter(int beforeDecimal, int afterDecimal) {
        InputFilter filter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                StringBuilder builder = new StringBuilder(dest);
                if (afterDecimal > 0 || source.toString().isEmpty()) {
                    builder.replace(dstart, dend, source.subSequence(start, end).toString());
                } else if (afterDecimal == 0 && !source.toString().isEmpty() && source.toString().contains(".")) {
                    return "";
                }
                if (!builder.toString().matches("(([1-9]{1})([0-9]{0," + (afterDecimal == 0 ? beforeDecimal - 2 : beforeDecimal - 1) + "})?)?(\\.[0-9]{0," + afterDecimal + "})?")) {
                    if (source.length() == 0) {
                        return dest.subSequence(dstart, dend);
                    } else {
                        return "";
                    }
                }

                return null;

            }
        };

        return filter;
    }

    public InputFilter getDecimalInputFilterWithZero(int beforeDecimal, int afterDecimal) {
        InputFilter filter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                StringBuilder builder = new StringBuilder(dest);
                if (afterDecimal > 0 || source.toString().isEmpty()) {
                    builder.replace(dstart, dend, source.subSequence(start, end).toString());
                } else if (afterDecimal == 0 && !source.toString().isEmpty() && source.toString().contains(".")) {
                    return "";
                }
                if (!builder.toString().matches("(([0-9]{1})([0-9]{0," + (afterDecimal == 0 ? beforeDecimal - 2 : beforeDecimal - 1) + "})?)?(\\.[0-9]{0," + afterDecimal + "})?")) {
                    if (source.length() == 0) {
                        return dest.subSequence(dstart, dend);
                    } else {
                        return "";
                    }
                }

                return null;

            }
        };

        return filter;
    }

    public InputFilter getTextOnlyInputFilter(boolean isCapChar) {
        return getTextOnlyInputFilter(isCapChar, 0, false, false);
    }

    public InputFilter getTextOnlyInputFilter(boolean isCapChar, int maxLength, boolean isAllowedSpecialChars, boolean isSpaceRequired) {
        return getCustomInputFilter(isCapChar, true, true, isAllowedSpecialChars, false, maxLength);
    }

    public InputFilter getTextOnlyInputFilterWithSpace(boolean isCapChar) {
        return getTextOnlyInputFilter(isCapChar, 0, false, true);
    }

    public InputFilter getDigitOnlyInputFilter(boolean isSpaceRequired, boolean isAllowedSpecialChars) {
        return getDigitOnlyInputFilter(isSpaceRequired, isAllowedSpecialChars, 0);
    }

    public InputFilter getDigitOnlyInputFilter(boolean isSpaceRequired, boolean isAllowedSpecialChars, int maxLength) {
        return getCustomInputFilter(false, isSpaceRequired, false, isAllowedSpecialChars, true, maxLength);
    }

    public InputFilter getDigitOnlyInputFilterWithHyphen(boolean isSpaceRequired, boolean isHyphenAllowed, int maxLength) {
        return getCustomInputFilter(false, isSpaceRequired, false, false, true, true, maxLength);
    }

    public synchronized InputFilter getCustomInputFilter(boolean isAllCaps, boolean isSpaceAllowed, boolean isCharacter, boolean isAllowedSpecialChar, boolean isDigitsAllowed, int length) {
        return getCustomInputFilter(isAllCaps, isSpaceAllowed, isCharacter, isAllowedSpecialChar, false, isDigitsAllowed, length);
    }

    public synchronized InputFilter getCustomInputFilter(boolean isAllCaps, boolean isSpaceAllowed, boolean isCharacter, boolean isAllowedSpecialChar, boolean isOnlyHyphen, boolean isDigitsAllowed, int length) {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            if (source.equals("")) {
                return source;
            }
            if ((isOnlyHyphen && isSpaceAllowed && isCharacter && isAllowedSpecialChar && !isDigitsAllowed && source.toString().matches("^[ A-Za-z-]*$")) ||
                    (isOnlyHyphen && isSpaceAllowed && isCharacter && isDigitsAllowed && source.toString().matches("^[ A-Za-z0-9-]*$")) ||
                    (isOnlyHyphen && !isSpaceAllowed && isCharacter && isDigitsAllowed && source.toString().matches("^[A-Za-z0-9-]*$")) ||
                    (isOnlyHyphen && !isSpaceAllowed && !isCharacter && isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("^[0-9-]*$")) ||
                    (isOnlyHyphen && isSpaceAllowed && !isCharacter && isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("^[ 0-9-]*$")) ||

                    (isSpaceAllowed && isCharacter && !isAllowedSpecialChar && !isDigitsAllowed && source.toString().matches("[a-zA-Z ]+")) ||
                    (isSpaceAllowed && isCharacter && isAllowedSpecialChar && !isDigitsAllowed && source.toString().matches("^[ A-Za-z.&'/-]*$")) ||
                    (isSpaceAllowed && isCharacter && !isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("[ a-zA-Z0-9]+")) ||
                    (isSpaceAllowed && isCharacter && isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("^[ A-Za-z0-9.&'/-]*$")) ||

                    (!isSpaceAllowed && isCharacter && !isAllowedSpecialChar && !isDigitsAllowed && source.toString().matches("[a-zA-Z]+")) ||
                    (!isSpaceAllowed && isCharacter && isAllowedSpecialChar && !isDigitsAllowed && source.toString().matches("^[A-Za-z.&'/-]*$")) ||
                    (!isSpaceAllowed && isCharacter && !isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("[a-zA-Z0-9]+")) ||
                    (!isSpaceAllowed && isCharacter && isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("^[A-Za-z0-9.&'/-]*$")) ||

                    (!isSpaceAllowed && !isCharacter && !isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("[0-9]+")) ||
                    (!isSpaceAllowed && !isCharacter && isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("^[0-9.&'/-]*$")) ||
                    (isSpaceAllowed && !isCharacter && !isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("[ 0-9]+")) ||
                    (isSpaceAllowed && !isCharacter && isAllowedSpecialChar && isDigitsAllowed && source.toString().matches("^[ 0-9.&'/-]*$"))

            ) {
                CharSequence charSequence = isAllCaps ? source.toString().toUpperCase() : source;
                if (length == 0) {
                    return charSequence;
                } else if (length > 0 && (dend > 0 || end > 0) && dend < length) {
                    return charSequence;
                } else {
                    return source.subSequence(start, end - 1);
                }
            } else if (start != end) {
                return source.subSequence(start, end - 1);
            } else {
                return "";
            }
        };
        return filter;
    }

    public synchronized InputFilter getEmailInputFilter(boolean isAllCaps, int length) {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            if (source.equals("")) {
                return source;
            }
            if (source.toString().matches("^[A-Za-z0-9._-]*$")) {
                CharSequence charSequence = isAllCaps ? source.toString().toUpperCase() : source;
                if (length == 0) {
                    return charSequence;
                } else if (length > 0 && (dend > 0 || end > 0) && dend < length) {
                    return charSequence;
                } else {
                    return source.subSequence(start, end - 1);
                }
            } else if (start != end) {
                return source.subSequence(start, end - 1);
            } else {
                return "";
            }
        };
        return filter;
    }

    public synchronized InputFilter getCustomInputFilter(boolean isAllCaps, String regex, int length) {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            if (source.equals("")) {
                return source;
            }
            if ((source.toString().matches(regex))

            ) {
                CharSequence charSequence = isAllCaps ? source.toString().toUpperCase() : source;
                if (length == 0) {
                    return charSequence;
                } else if (length > 0 && (dend > 0 || end > 0) && dend < length) {
                    return charSequence;
                } else {
                    return source.subSequence(start, end - 1);
                }
            } else if (start != end) {
                return source.subSequence(start, end - 1);
            } else {
                return "";
            }
        };
        return filter;
    }

    public InputFilter getNumberOnlyInputFilter() {
        return getNumberOnlyInputFilter(0);
    }

    public InputFilter getNumberOnlyInputFilter(int Maxlength) {
        return getDigitOnlyInputFilter(false, false, Maxlength);
    }

    public InputFilter getNumberOnlyInputFilter(int Maxlength, boolean isHyphenAllowed) {
        return getDigitOnlyInputFilterWithHyphen(false, isHyphenAllowed, Maxlength);
    }

    public String getFirstLastNameLetters(String name, boolean isSecondWordInclude) {
        String letters = "T";

        if (!UtilityClass.getInstance().nullChecker(name).isEmpty()) {
            String[] nameArray = name.split(" ");
            int length = nameArray.length;

            if (length == 1 && name.trim().length() > 1 && nameArray[0].length() > 0) {
                letters = Character.toString(nameArray[0].charAt(0));
            } else if (length > 1) {
                int tempIndex = isSecondWordInclude ? 1 : length - 1;
                if (nameArray[0].length() > 0 && nameArray[tempIndex].length() > 0) {
                    letters = Character.toString(nameArray[0].charAt(0)) + Character.toString(nameArray[tempIndex].charAt(0));
                } else if (nameArray[0].length() > 0) {
                    letters = Character.toString(nameArray[0].charAt(0));
                }
            }
        }
        return letters.toUpperCase();
    }
}
