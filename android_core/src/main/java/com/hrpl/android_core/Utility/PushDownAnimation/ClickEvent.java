package com.hrpl.android_core.Utility.PushDownAnimation;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.view.View;

import static com.hrpl.android_core.Utility.PushDownAnimation.PushDownAnim.MODE_SCALE;
import static com.hrpl.android_core.Utility.PushDownAnimation.PushDownAnim.MODE_STATIC_DP;

@SuppressLint("ClickableViewAccessibility")
public class ClickEvent {

    private int delay = 300;

    public interface onClickEnabledView {
        void onClickEnabled(View v);
    }

    public interface onClick {
        void onClick(View view);

        void onLongClick(View view);
    }

    public void setClickListener(View view, boolean enableCountDown, onClickEnabledView clickListener) {
        PushDownAnim.setPushDownAnimTo(view)
                .setScale(MODE_SCALE, 0.93f)
                .setDurationPush(PushDownAnim.DEFAULT_PUSH_DURATION)
                .setDurationRelease(PushDownAnim.DEFAULT_RELEASE_DURATION)
                .setInterpolatorPush(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setInterpolatorRelease(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setOnClickListener(v -> {
                    if (enableCountDown) {
                        new CountDownTimer(delay, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                v.setEnabled(true);
                            }
                        }.start();
                        v.setEnabled(false);
                    }
                    clickListener.onClickEnabled(view);
                });
    }

    public void setClickListener(View view, boolean enableCountDown, float scale, onClickEnabledView clickListener) {
        PushDownAnim.setPushDownAnimTo(view)
                .setScale(MODE_STATIC_DP, scale)
                .setDurationPush(PushDownAnim.DEFAULT_PUSH_DURATION)
                .setDurationRelease(PushDownAnim.DEFAULT_RELEASE_DURATION)
                .setInterpolatorPush(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setInterpolatorRelease(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setOnClickListener(v -> {
                    if (enableCountDown) {
                        new CountDownTimer(delay, 3000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                v.setEnabled(true);
                            }
                        }.start();
                        v.setEnabled(false);
                    }
                    clickListener.onClickEnabled(view);
                });
    }

    public void setClickListener(View view, onClickEnabledView clickListener) {
        setClickListener(view, false, clickListener);
    }

    public void setClickListener(View view, float scale, onClickEnabledView clickListener) {
        setClickListener(view, false, scale, clickListener);
    }
    
    public void setClickListener(View view, onClick clickListener) {
        PushDownAnim.setPushDownAnimTo(view)
                .setScale(MODE_SCALE, 0.89f)
                .setDurationPush(PushDownAnim.DEFAULT_PUSH_DURATION)
                .setDurationRelease(PushDownAnim.DEFAULT_RELEASE_DURATION)
                .setInterpolatorPush(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setInterpolatorRelease(PushDownAnim.DEFAULT_INTERPOLATOR)
                .setOnClickListener(v -> {
                    new CountDownTimer(delay, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            v.setEnabled(true);
                        }
                    }.start();
                    v.setEnabled(false);
                    clickListener.onClick(view);
                })
                .setOnLongClickListener(v -> {
                    clickListener.onLongClick(v);
                    return true;
                });


    }

    public interface onClickEnabled {
        void onClickEnabled();
    }

}
