package com.hrpl.android_core.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.google.gson.Gson;
import com.hrpl.android_core.Listeners.ISC_String2;
import com.hrpl.android_core.Listeners.WidthHeightChangeListener;
import com.hrpl.android_core.R;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class UtilityClass {

    public UtilityClass() {
    }

    private static UtilityClass utilityClass;

    public static UtilityClass getInstance() {
        if (utilityClass == null) {
            utilityClass = new UtilityClass();
        }
        return utilityClass;
    }



    public String nullChecker(String string) {

        if (string == null) {
            string = "";
        }
        return string;
    }

    public String nullCheckerDash(String string) {

        if (string == null || string.equalsIgnoreCase("-")) {
            string = "";
        }
        return string;
    }

    public String nullCheckerNumber(String string) {

        if (string == null || string.equalsIgnoreCase("0")) {
            string = "";
        }
        return string;
    }

    public String nullCheckerNA(String string) {

        if (string == null || string.isEmpty()) {
            string = "- NA -";
        }
        return string;
    }

    public void setFont(Context context, TextView textView, int fontName) {
        Typeface type = ResourcesCompat.getFont(context, fontName);
        textView.setTypeface(type);
    }

    public int convert_DP_to_Pixel(float dp, Context context) {
        Resources r = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    public int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        bitmap = Bitmap.createScaledBitmap(bitmap, 1000, 500, true);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    public Bitmap convertUriToBitmap(Context context, Uri uri) throws IOException {
        return MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
    }

    public String convertUriToBase64(Context context, Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        String base64 = getEncoded64ImageStringFromBitmap(bitmap);
        return base64;
    }

    public String convertFileUriToBase64(Context context, Uri uri) {
        InputStream iStream = null;
        try {
            iStream = context.getContentResolver().openInputStream(uri);

            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            int len = 0;
            while ((len = iStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            byte[] inputData = byteBuffer.toByteArray();
            String encoded = Base64.encodeToString(inputData, Base64.NO_WRAP);
            return encoded;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void getLogCatData(Context context, ISC_String2 callback) {
        try {
            Process process = Runtime.getRuntime().exec(new String[]{"logcat", "-e"});
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()), 4 * 1024);

            StringBuilder log = new StringBuilder();
            String separator = System.getProperty("line.separator");
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line);
                log.append(separator);
            }
            String logCat = log.toString();
            callback.onSuccess(nullChecker(logCat));
        } catch (IOException e) {
            callback.onError(nullChecker(e.getMessage().toString()), false);
        }
    }

    public void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;

            for (int idx = 0; idx < group.getChildCount(); idx++) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }

    public <T> void getDataFromRawFolder(Context context, int raw_folder_file_path, onSuccessListener<T> callback) throws IOException {
        InputStream is = context.getResources().openRawResource(raw_folder_file_path);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
            is.close();
        }

        String data = writer.toString();

        Type typeClass = null;

        Type[] genericInterfaces = callback.getClass().getGenericInterfaces();
        for (Type genericInterface : genericInterfaces) {
            if (genericInterface instanceof ParameterizedType) {
                Type[] genericTypes = ((ParameterizedType) genericInterface).getActualTypeArguments();
                for (Type genericType : genericTypes) {
                    typeClass = genericType;
                }
            }
        }
        if (typeClass == null) {
            callback.onError("Casting Error");
        } else {
            callback.onSuccess(new Gson().fromJson(data, typeClass));
        }
    }

    public String getCountryDialCode(Activity activity) {
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager telephonyMngr = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);

        CountryID = telephonyMngr.getSimCountryIso().toUpperCase();
        String[] arrContryCode = activity.getResources().getStringArray(R.array.DialingCountryCode);
        for (int i = 0; i < arrContryCode.length; i++) {
            String[] arrDial = arrContryCode[i].split(",");
            if (arrDial[1].trim().equals(CountryID.trim())) {
                CountryZipCode = arrDial[0];
                break;
            }
        }
        return CountryZipCode;
    }

    public interface onSuccessListener<T> {
        void onSuccess(T dataModel);

        void onError(String error);
    }

    public void getViewWidth(View view, WidthHeightChangeListener listener){
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                listener.onMeasure(view.getMeasuredWidth(), view.getMeasuredHeight());
            }
        });
    }



}
