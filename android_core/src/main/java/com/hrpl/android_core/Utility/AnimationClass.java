package com.hrpl.android_core.Utility;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.hrpl.android_core.R;

public class AnimationClass {

    public static void startAnimation(ImageView imageView, ImageView imageView2) {
        final ValueAnimator animator = ValueAnimator.ofFloat(1.0f, 0.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new
                LinearInterpolator());
        animator.setDuration(7000L);
        animator.addUpdateListener(animation -> {
            final float progress = (float) animation.getAnimatedValue();
            final float width = imageView.getWidth();
            final float translationX = width * progress;
            imageView.setTranslationX(translationX);
            imageView2.setTranslationX(translationX - width);
        });
        animator.start();
    }

    @SuppressLint("MissingPermission")
    public static void wobbleAnimation(Context context, View view) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.shake_or_wobble);
        view.startAnimation(animation);
    }

}
