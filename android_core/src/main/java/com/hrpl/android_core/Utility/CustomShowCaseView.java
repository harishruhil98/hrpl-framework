package com.hrpl.android_core.Utility;

import android.app.Activity;
import android.view.View;

import com.hrpl.android_core.ShowCaseView.MaterialShowcaseSequence;
import com.hrpl.android_core.ShowCaseView.MaterialShowcaseView;
import com.hrpl.android_core.ShowCaseView.ShowcaseConfig;

public class CustomShowCaseView {
    private ShowcaseConfig config;
    private MaterialShowcaseSequence sequence;
    private int delay = 500;
    private String sequenceId = "1";
    private Activity activity;
    private View rootView;

    public CustomShowCaseView(Activity activity, int delay, String sequenceId, View rootView) {
        this.activity = activity;
        this.delay = delay;
        this.sequenceId = sequenceId;
        this.rootView = rootView;
        init(activity, delay, sequenceId);
    }

    private void init(Activity activity, int delay, String sequenceId) {
        config = new ShowcaseConfig();
        config.setDelay(delay);// half second between each showcase view
        sequence = new MaterialShowcaseSequence(activity, sequenceId);
        sequence.setConfig(config);
        sequence.singleUse(sequenceId);
    }

    public void addItem(View view, String description) {
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(activity)
                        .setSkipText("SKIP")
                        .setTarget(view)
                        .setDismissText("GOT IT")
                        .setContentText(description)
                        .build());
    }

    public void startShowcaseView(){
        sequence.setOnCompleteListener(b -> UtilityClass.getInstance().enableDisableView(rootView, true));
//        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT));
//        this.setClickable(false);
//
//        ((ViewGroup) ((Activity) getContext()).getWindow().getDecorView()).addView(this);
//        AlphaAnimation startAnimation = new AlphaAnimation(0.0f, 1.0f);
//        startAnimation.setDuration(APPEARING_ANIMATION_DURATION);
//        startAnimation.setFillAfter(true);
//        this.startAnimation(startAnimation);
//        mIsShowing = true;

        if (!sequence.hasFired()) {
            UtilityClass.getInstance().enableDisableView(rootView, false);
            sequence.start();
        }
    }
}
