package com.hrpl.android_core.Utility;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.hrpl.android_core.CustomViews.CustomAlertDialog;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

public class FileUtils {

    private Activity activity;
    private ProgressDialog progressDialog;

    public FileUtils(Activity activity) {
        this.activity = activity;
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Pdf is generating, Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public FileUtils(Activity activity, boolean isShowProgressDialog) {
        this.activity = activity;
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Pdf is generating, Please wait...");
        progressDialog.setCancelable(false);
        if (isShowProgressDialog) {
            progressDialog.show();
        }
    }

    private void generatePdf(String fileName, ArrayList<Bitmap> bitmapArrayList, boolean openPdf, boolean saveIntoCache, String app_name, String msg, int msgColor, int positiveBtnColor, CreatePdfListener createPdfListener) {
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        //  Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = (int) displaymetrics.heightPixels;
        int width = (int) displaymetrics.widthPixels;

        PdfDocument document = new PdfDocument();

        for (int a = 0; a < bitmapArrayList.size(); a++) {
            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(width, height, a + 1).create();
            PdfDocument.Page page = document.startPage(pageInfo);
            Canvas canvas = page.getCanvas();
            Paint paint = new Paint();
            canvas.drawPaint(paint);

            Bitmap bb = Bitmap.createScaledBitmap(bitmapArrayList.get(a), width, height, true);

            paint.setColor(Color.BLUE);
            canvas.drawBitmap(bb, 0, 0, null);
            document.finishPage(page);
        }

        String timeStamp = (UtilityClass.getInstance().nullChecker(fileName).isEmpty() ? new Date().getTime() : fileName) + ".pdf";

        File filePath = null;

        if (saveIntoCache) {
            filePath = new File(activity.getCacheDir(), timeStamp);
        } else {
            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), app_name);
            folder.mkdirs();
            if (!folder.exists()) {
                if (!folder.mkdirs()) {
                    filePath = new File(folder.getPath() + "/" + timeStamp);
                }
            } else {
                filePath = new File(folder.getPath() + "/" + timeStamp);
            }
        }

        try {
            document.writeTo(new FileOutputStream(filePath));
            // close the document
            document.close();
            progressDialog.dismiss();
            if (openPdf) {
                openGeneratedPDF(filePath, msg, msgColor, positiveBtnColor);
                createPdfListener.onDialogOpen();
            } else {
                progressDialog.dismiss();
                createPdfListener.onCreated(filePath.getPath(), "PDF is generated ! and saved in " + filePath.getPath());
            }
        } catch (IOException e) {
            progressDialog.dismiss();
            e.printStackTrace();
            document.close();
            createPdfListener.onError("Something wrong: " + e.toString());
        }
    }

    private Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        Bitmap newBitmap = Bitmap.createBitmap(resizedBitmap.getWidth(), resizedBitmap.getHeight(), resizedBitmap.getConfig());
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(resizedBitmap, 0, 0, null);
        bm.recycle();
        resizedBitmap.recycle();
        return newBitmap;
    }

    private void createPdf(String fileName, View v, boolean openPdf, boolean saveIntoCache, String app_name, String msg, int msgColor, int positiveBtnColor, CreatePdfListener createPdfListener) {

        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bitmap);
        v.draw(c);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
        ArrayList<Bitmap> arrayList = new ArrayList<>();
        int pageHeight = UtilityClass.getInstance().getScreenHeight();
        int bitmapHeight = bitmap.getHeight();
        int pageCount = bitmapHeight <= pageHeight ? 1 : bitmapHeight % pageHeight != 0 ? (bitmapHeight / pageHeight) + 1 : bitmapHeight / pageHeight;

        scaledBitmap = getResizedBitmap(scaledBitmap, scaledBitmap.getWidth(), pageHeight * pageCount);

        int yCrood = 0;
        for (int a = 0; a < pageCount; a++) {
            Bitmap genBitmap = Bitmap.createBitmap(scaledBitmap, 0, yCrood, scaledBitmap.getWidth(), pageHeight);
            arrayList.add(genBitmap);
            yCrood += pageHeight;
        }

        generatePdf(fileName, arrayList, openPdf, saveIntoCache , app_name, msg, msgColor, positiveBtnColor, createPdfListener);
    }

    public void convertViewToPdf(String fileName, View v, boolean openPdf, boolean saveIntoCache, String app_name, String msg, int msgColor, int positiveBtnColor, CreatePdfListener createPdfListener) {
        fileName = fileName.replace("/", "-");

        if (!isPermissionAllowed(activity)) {
            progressDialog.dismiss();
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 122);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
            }
        } else {

            if (openPdf){
                createPdf(fileName, v, openPdf, saveIntoCache, app_name, msg, msgColor, positiveBtnColor, createPdfListener);
            }else {
                createPdf(fileName, v, openPdf, saveIntoCache, app_name, "", 0, 0, createPdfListener);
            }

        }
    }

    public interface CreatePdfListener {
        void onCreated(String FilePath, String msg);

        void onError(String error);

        void onDialogOpen();
    }

    private boolean isPermissionAllowed(Activity activity) {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public void openGeneratedPDF(File file, String msg, int msgColor, int positiveBtnColor) {
        if (file.exists()) {
            progressDialog.show();
            CustomAlertDialog.DocGeneratedDialog(activity, msg, msgColor, positiveBtnColor, new CustomAlertDialog.DocGenerateListener() {
                @Override
                public void onShare() {
                    progressDialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    Uri uri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file);
                    intent.setType("application/pdf");
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    try {
                        activity.startActivity(Intent.createChooser(intent, "Share via "));
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(activity, "No Application available to share pdf", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onPreview() {
                    progressDialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri uri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", file);
                    intent.setDataAndType(uri, "application/pdf");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    try {
                        activity.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(activity, "No Application available to view pdf", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    public static String encodeFileToBase64Binary(File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String encoded = Base64.encodeToString(bytes, Base64.NO_WRAP);
        return encoded;
    }

    public static void savePDFBase64ToFile(Activity activity, String base64ImageData, String fileName, boolean saveInCache, onFileSavedListener2 savedListener) {
        FileOutputStream fos = null;
        try {
            if (base64ImageData != null) {
                File filePath;
                if (saveInCache) {
                    filePath = new File(activity.getCacheDir(), "/" + fileName);
                } else {
                    File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
                    folder.mkdirs();

                    // write the document content
                    filePath = new File(folder.getPath() + "/" + fileName);

                    if (!folder.exists()) {
                        if (!folder.mkdirs()) {
                            filePath = new File(folder.getPath() + "/" + fileName);
                        }
                    } else {
                        filePath = new File(folder.getPath() + "/" + fileName);
                    }

                }

                fos = new FileOutputStream(filePath);
                byte[] decodedString = Base64.decode(base64ImageData, Base64.DEFAULT);
                fos.write(decodedString);
                fos.flush();
                fos.close();
                String msg = "File saved in " + filePath.getAbsolutePath();
                savedListener.onSaved(filePath, msg, true);
            }

        } catch (Exception e) {
            savedListener.onError("File format not supported");
        } finally {
            if (fos != null) {
                fos = null;
            }
        }
    }

    public interface onFileSavedListener {
        void onSaved(String path, String msg, boolean isPdf);

        void onError(String error);
    }

    public interface onFileSavedListener2 {
        void onSaved(File file, String msg, boolean isPdf) throws IOException;

        void onError(String error);
    }

    public static void shareImage(Context context, Bitmap bitmap, String text, String APPLICATION_ID) {

        if (bitmap == null) {
//            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_share_image);
        }

        try {
            File file = new File(context.getExternalCacheDir(), File.separator + "banner.jpg");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Uri photoURI = FileProvider.getUriForFile(context, APPLICATION_ID + ".provider", file);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.putExtra(Intent.EXTRA_STREAM, photoURI);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("image/jpg");

            context.startActivity(Intent.createChooser(intent, "Share image via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void shareImage(Context context, File file, String text, String APPLICATION_ID) {
        try {
            final Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Uri photoURI = FileProvider.getUriForFile(context, APPLICATION_ID + ".provider", file);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, text);
            intent.putExtra(Intent.EXTRA_STREAM, photoURI);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("image/jpg");

            context.startActivity(Intent.createChooser(intent, "Share image via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Bitmap> getPdfAsBitmapList(File file) throws IOException {

        ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY));


        for (int a = 0; a < renderer.getPageCount(); a++) {
            PdfRenderer.Page mPage = renderer.openPage(a);
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            mPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            bitmapArrayList.add(bitmap);
            mPage.close();
        }

        progressDialog.dismiss();
        return bitmapArrayList;

    }

    /**
     * implementation 'com.github.elirehema:worksheet:0.0.1'
     * Add These filed to onCreate Function of launcher Activity
     * //For Creating Excel Sheets
     * System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
     * System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");
     * <p>
     * public static void exportExcel(Context context, ArrayList<InvoiceModel> arrayList, String orderId) {
     * FileOutputStream fos = null;
     * Workbook workbook = new XSSFWorkbook();
     * Sheet sheet = workbook.createSheet("Items");
     * try {
     * <p>
     * File folder = new File(Environment.getExternalStorageDirectory(), context.getString(R.string.app_name));
     * // write the document content
     * File filePath = null;
     * String fileName = orderId + ".xlsx";
     * <p>
     * if (!folder.exists()) {
     * if (!folder.mkdirs()) {
     * filePath = new File(folder.getPath() + "/" + fileName);
     * }
     * } else {
     * filePath = new File(folder.getPath() + "/" + fileName);
     * }
     * <p>
     * for (int i = 0; i < arrayList.size(); i++) {
     * <p>
     * Row row = sheet.createRow(i);
     * row.createCell(0).setCellValue(arrayList.get(i).UnitNo);
     * row.createCell(1).setCellValue(arrayList.get(i).TicketNumber);
     * row.createCell(2).setCellValue(arrayList.get(i).BaseRate);
     * row.createCell(3).setCellValue(arrayList.get(i).TripCount);
     * row.createCell(4).setCellValue(String.valueOf(NumberUtils.convertToInt(arrayList.get(i).TripCount) * NumberUtils.convertToInt(arrayList.get(i).BaseRate)));
     * }
     * <p>
     * <p>
     * fos = new FileOutputStream(filePath);
     * workbook.write(fos);
     * fos.flush();
     * fos.close();
     * String msg = "File saved in " + filePath.getAbsolutePath();
     * <p>
     * } catch (Exception e) {
     * e.printStackTrace();
     * }
     * }
     */

    public File getFileObject(Uri uri) {
        try {
            InputStream is = activity.getContentResolver().openInputStream(uri);
            byte[] bytesArray = new byte[is.available()];
            is.read(bytesArray);

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            byte[] buffer = new byte[0xFFFF];
            for (int len = is.read(buffer); len != -1; len = is.read(buffer)) {
                os.write(buffer, 0, len);
            }

            // write the document content
            File filePath = new File(activity.getCacheDir(), "/preview.pdf");

            long fileSizeInByte = filePath.length();

            FileOutputStream fos = new FileOutputStream(filePath.getPath());
            fos.write(bytesArray);
            fos.close();

            progressDialog.dismiss();
            return filePath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            progressDialog.dismiss();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            progressDialog.dismiss();
            return null;
        }
    }

    public void getBitmapFromBase64(String base64, BitmapListener bitmapListener) {
        final byte[] decodedBytes = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        bitmapListener.onBitmapGenerated(decodedBitmap);
        decodedBitmap.recycle();
    }


    public interface BitmapListener {
        void onBitmapGenerated(Bitmap bitmap);
    }

    public interface UriListener {
        void onURIGenerated(Uri uri);
    }

    public void getBitmapToUri(Activity activity, String fileName, String base64, boolean saveIntoCache, String appName, UriListener uriListener) {
        final byte[] decodedBytes = Base64.decode(base64, Base64.DEFAULT);

        String timeStamp = fileName;
        if (UtilityClass.getInstance().nullChecker(timeStamp).isEmpty()){
            timeStamp = new Date().getTime() +"";
        }

        timeStamp = timeStamp  + ".jpg";

        File filePath = null;

        if (saveIntoCache) {
            filePath = new File(activity.getCacheDir(), timeStamp);
        } else {
            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), appName);
            folder.mkdirs();
            if (!folder.exists()) {
                if (!folder.mkdirs()) {
                    filePath = new File(folder.getPath() + "/" + timeStamp);
                }
            } else {
                filePath = new File(folder.getPath() + "/" + timeStamp);
            }
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath.getPath());
            fos.write(decodedBytes);
            fos.close();
        } catch (IOException ignored) {
        }
        uriListener.onURIGenerated(Uri.fromFile(filePath));

    }
}
