package com.hrpl.android_core.Utility;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.hrpl.android_core.BaseApplicationClass;
import com.hrpl.android_core.CustomViews.CustomSnackBar;
import com.hrpl.android_core.Listeners.WidthHeightChangeListener;
import com.hrpl.android_core.R;
import com.hrpl.android_core.Utility.PushDownAnimation.ClickEvent;

import java.util.Locale;

import static android.os.Build.VERSION.SDK_INT;
import static com.hrpl.android_core.Utility.ConstantClass.REQUEST_CAMERA;
import static com.hrpl.android_core.Utility.ConstantClass.REQUEST_ExternalStorage;

public abstract class BaseFragment extends Fragment implements ClickEvent.onClickEnabledView {

    protected Context context;
    protected Activity activity;
    private ProgressDialog progressDialog;
    private String tempPhoneNo = "";
    private View _rootView;
    private PermissionListener _permissionListener;

    protected boolean doubleBackToExitPressedOnce = false;
    protected boolean isDoubleBackToExitPressedOnceActive = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        activity = (Activity) context;
        View view = inflater.inflate(setContentView(), container, false);
        _rootView = view;
        init(view, getArguments());


        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);

        bindClickListener();
        return view;
    }

    public int getScreenWidth(){
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public void getViewWidthHeight(View view, WidthHeightListener listener){
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                listener.onMeasure(view.getMeasuredWidth(), view.getMeasuredHeight());
            }
        });
    }

    public interface WidthHeightListener{
        void onMeasure(int width, int height);
    }

    public int getScreenHeight(){
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public int getScreenDensityDPI(){
        return Resources.getSystem().getDisplayMetrics().densityDpi;
    }

    protected abstract void init(View view, Bundle bundle);

    protected abstract int setContentView();

    public void showSnackBar(String error, boolean isWarning) {
        if (!((Activity) context).isFinishing() && !UtilityClass.getInstance().nullChecker(error).trim().isEmpty()) {
            CustomSnackBar.getInstance().showSnackToast((Activity) context, error, !isWarning, isWarning);
        }
    }

    public void showSnackBar(String msg) {
        if (!((Activity) context).isFinishing() && !UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            CustomSnackBar.getInstance().showSnackToast((Activity) context, msg);
        }
    }

    public void sendLocationToMaps(String location){

        String uri = "http://maps.google.co.in/maps?q=" + location;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(intent);
    }

    public void sendLocationToMaps(double latitude, double longitude){
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(intent);
    }

    protected abstract void bindClickListener();

    public void setClickEvent(View view) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, 3f, this);
    }

    public void setClickEvent(View view, onClickedEvent onClickedEventListener) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, 3f, onClickedEventListener::onClicked);
    }

    public void setClickEventWithoutScale(View view) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, 0f, this);
    }

    public void setClickEvent(View view, int scale) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, scale, this);
    }

    public void setClickEvent(View view, int scale, onClickedEvent onClickedEventListener) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, scale, onClickedEventListener::onClicked);
    }

    public void setClickEvent(View view, boolean enableDelay) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, enableDelay, 3f, this);
    }

    public void onDoubleBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ((Activity) context).finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(context, context.getResources().getString(R.string.back_again_to_exit_app), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == ConstantClass.REQUEST_PhoneCall && !UtilityClass.getInstance().nullChecker(tempPhoneNo).isEmpty() && isCallingPermissionGranted()) {
            callPhoneNO(tempPhoneNo);
        } else if (requestCode == ConstantClass.REQUEST_PhoneCall && !isCallingPermissionGranted()) {
            showSnackBar("Calling permission denied.");
        } else if (requestCode == ConstantClass.REQUEST_ExternalStorage && _permissionListener != null && isExternalStoragePermissionGranted()) {
            _permissionListener.onPermissionGranted(ConstantClass.REQUEST_ExternalStorage);
        } else if (requestCode == ConstantClass.REQUEST_ExternalStorage && _permissionListener != null && !isExternalStoragePermissionGranted()) {
            _permissionListener.onPermissionDenied(ConstantClass.REQUEST_ExternalStorage);
        } else if (requestCode == ConstantClass.REQUEST_CAMERA && _permissionListener != null && isCameraPermissionGranted()) {
            _permissionListener.onPermissionGranted(ConstantClass.REQUEST_CAMERA);
        } else if (requestCode == ConstantClass.REQUEST_CAMERA && _permissionListener != null && !isCameraPermissionGranted()) {
            _permissionListener.onPermissionDenied(ConstantClass.REQUEST_CAMERA);
        }
    }

    public void onPermissionGranted() {
    }

    protected void onPermissionGranted(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    }

    protected void requestStoragePermissions(PermissionListener permissionListener) {
        _permissionListener = permissionListener;
        requestInternalStoragePermissions(_permissionListener);
    }

    private void requestInternalStoragePermissions(PermissionListener permissionListener) {
        if (SDK_INT >= Build.VERSION_CODES.R && !isExternalStoragePermissionGranted()) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s", context.getPackageName())));
                startActivityForResult(intent, REQUEST_ExternalStorage);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, REQUEST_ExternalStorage);
            }
            permissionListener.onPermissionAndroid11(REQUEST_ExternalStorage);
        } else if (SDK_INT < Build.VERSION_CODES.R && !isExternalStoragePermissionGranted()) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_ExternalStorage);
        } else if (isExternalStoragePermissionGranted()) {
            permissionListener.onPermissionGranted(REQUEST_ExternalStorage);
        }
    }

    protected void requestCameraPermissions(PermissionListener permissionListener) {
        _permissionListener = permissionListener;
        requestInternalCameraPermissions(_permissionListener);
    }

    private void requestInternalCameraPermissions(PermissionListener permissionListener) {
        if (!isCameraPermissionGranted()) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        } else if (isCameraPermissionGranted()) {
            permissionListener.onPermissionGranted(REQUEST_CAMERA);
        }
    }

    private boolean isExternalStoragePermissionGranted() {
        boolean result = true;

        if (SDK_INT >= Build.VERSION_CODES.R && !Environment.isExternalStorageManager()) {
            result = false;
        } else if (SDK_INT < Build.VERSION_CODES.R && ContextCompat.checkSelfPermission((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            result = false;
        }
        return result;
    }

    private boolean isCallingPermissionGranted() {
        boolean result = true;

        if (context.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            result = false;
        }
        return result;
    }

    private boolean isCameraPermissionGranted() {
        boolean result = true;

        if (context.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            result = false;
        }
        return result;
    }

    public interface PermissionListener {
        void onPermissionGranted(int requestCode);

        void onPermissionDenied(int requestCode);

        void onPermissionAndroid11(int requestCode);

        void onError(int requestCode, String error);
    }

    @Override
    public void onClickEnabled(View v) {
        onClicked(v);
    }

    protected abstract void onClicked(View v);

    public void hideProgressDialog() {
        if (!((Activity) context).isFinishing() && progressDialog != null) {
            progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
            progressDialog.dismiss();
        }
    }

    public void showProgressDialog() {
        if (!((Activity) context).isFinishing() && progressDialog != null) {
            progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
            progressDialog.show();
        }
    }

    public void setProgressDialogMessage(@NonNull String message){
        if (!((Activity) context).isFinishing() && progressDialog != null && UtilityClass.getInstance().nullChecker(message).length() > 0) {
            progressDialog.setMessage(UtilityClass.getInstance().nullChecker(message));
        }
    }

    public void shareApp(String titleMsg, @NonNull String appName, @NonNull String BuildConfig_APPLICATION_ID) {
        Intent intent =new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_SUBJECT, appName);
        intent.putExtra(Intent.EXTRA_TEXT, UtilityClass.getInstance().nullChecker(titleMsg) + " https://play.google.com/store/apps/details?id=" + BuildConfig_APPLICATION_ID);
        intent.setType("text/plain");
        context.startActivity(intent);
    }

    public void callPhoneNO(String phoneNo) {
        if (!UtilityClass.getInstance().nullChecker(phoneNo).trim().isEmpty()) {
            String uri = "tel:" + phoneNo.trim();
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            if (isCallingPermissionGranted()) {
                context.startActivity(intent);
            } else {
                tempPhoneNo = phoneNo;
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, ConstantClass.REQUEST_PhoneCall);
            }
        }
    }

    public void sendEmail(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public interface onClickedEvent {
        void onClicked(View view);
    }

    public boolean validEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
        String emailPatternnew = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+\\.+[a-zA-Z]+";

        if (!UtilityClass.getInstance().nullChecker(email).isEmpty() && email.contains("@") && email.matches(emailPattern)) {
            String domain = email.substring(email.indexOf('@'), email.length());
            String last = domain.substring(domain.indexOf('.'), domain.length());
            if (email.matches(emailPattern) && (last.length() == 3 || last.length() == 4)) { // check @domain.nl or @domain.com or @domain.org
                return true;
            } else if (email.matches(emailPatternnew) && last.length() == 6 && email.charAt(email.length() - 3) == '.') { //check for @domain.co.in or @domain.co.uk
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getText(TextView textView) {
        if (textView != null) {
            return textView.getText().toString();
        } else {
            return "";
        }
    }

    public String getText(EditText editText) {
        if (editText != null) {
            return editText.getText().toString();
        } else {
            return "";
        }
    }

    public void setText(@NonNull TextView textView, @NonNull String text) {
        textView.setText(text);
    }

    public void setText(TextInputLayout textInputLayout, String text) {
        if (textInputLayout != null){
            setText(textInputLayout.getEditText(), text);
        }
    }

    public void getText(@NonNull EditText editText, @NonNull String text) {
        editText.setText(text);
    }

    public void getViewWidth(View view, WidthHeightChangeListener listener){
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                listener.onMeasure(view.getMeasuredWidth(), view.getMeasuredHeight());
            }
        });
    }

    public String getText(TextInputLayout textInputLayout){
        return getText(textInputLayout.getEditText());
    }


}
