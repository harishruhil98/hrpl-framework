package com.hrpl.android_core.Utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class NumberUtilsBase {

    public enum DecimalType {
        DecimalType1,
        DecimalType2,
        DecimalType3
    }

    private static String DecimalType1 = "#.#";
    private static String DecimalType2 = "#.##";
    private static String DecimalType3 = "#.###";

    public static int convertToInt(String number) {
        number = UtilityClass.getInstance().nullChecker(number).trim();
        int convertedNumber = 0;

        try {
            convertedNumber = (int) Math.round(Double.parseDouble(String.valueOf(getIntFromAlphaNumericString(number))));
        } catch (Exception ignored) {
        }
        return convertedNumber;
    }

    public static int convertToInt2(String number) {
        number = UtilityClass.getInstance().nullChecker(number).trim();
        int convertedNumber = 0;

        try {
            convertedNumber = (int) Math.round(Double.parseDouble(number));
        } catch (Exception ignored) {
        }
        return convertedNumber;
    }

    public static int convertToInt(Double number) {
        int convertedNumber = 0;

        try {
            convertedNumber = (int) Math.round(number);
        } catch (Exception ignored) {
        }
        return convertedNumber;
    }

    private static double doubleUpToDecimalPrecision(double value, int precision) {
        if (precision == 0) {
            int val = (int) value;
            return Double.parseDouble(String.valueOf(val));
        } else {
            String precisionValue = "#.";
            for (int a = 0; a < precision; a++) {
                precisionValue = precisionValue + "#";
            }
            DecimalFormat df = new DecimalFormat(precisionValue);
            df.setRoundingMode(RoundingMode.FLOOR);
            return Double.parseDouble(df.format(value));
        }
    }

    private static String doubleUpToDecimalPrecisionString(double value, int precision) {
        if (precision == 0) {
            return String.valueOf((int) doubleUpToDecimalPrecision(value, precision));
        } else {
            value = doubleUpToDecimalPrecision(value, precision);
            return String.format("%." + precision + "f", value);
        }
    }

    public static String doubleUpToDecimal(int decimalCount, double value) {
        return doubleUpToDecimalPrecisionString(value, decimalCount);
    }

    public static String doubleUpToDecimal(double value) {
        try {
            return doubleUpToDecimalPrecisionString(value, 2);
        } catch (Exception e) {
            return "0.0";
        }
    }

    public static String doubleUpToDecimalSingle(double value) {
        try {
            return doubleUpToDecimalPrecisionString(value, 1);
        } catch (Exception e) {
            return "0.0";
        }
    }

    public static String doubleUpToDecimal(int decimalCount, String value1) {
        value1 = UtilityClass.getInstance().nullChecker(value1).trim();
        double value = 0;

        if (decimalCount == 0) {
            return String.valueOf((int) value);
        } else {
            try {
                value = Double.parseDouble(value1);
            } catch (Exception e) {

            }
            return doubleUpToDecimalPrecisionString(value, decimalCount);
        }
    }

    public static double convertToDouble(String number) {
        number = UtilityClass.getInstance().nullChecker(number).trim();
        double convertedNumber = 0.0;
        try {
            BigDecimal a = new BigDecimal(number);
            BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            convertedNumber = Double.parseDouble(String.valueOf(roundOff));
        } catch (Exception ignored) {
        }
        return convertedNumber;
    }

    private static DecimalFormat getDecimalType(DecimalType decimalType) {
        DecimalFormat df;

        switch (decimalType) {

            case DecimalType1: {
                df = new DecimalFormat(DecimalType1);
                break;
            }

            case DecimalType2: {
                df = new DecimalFormat(DecimalType2);
                break;
            }

            case DecimalType3: {
                df = new DecimalFormat(DecimalType3);
                break;
            }
            default:
                df = new DecimalFormat(DecimalType1);
        }
        return df;
    }

    public static double convertToDouble(Double number) {
        return doubleUpToDecimalPrecision(number, 2);
    }

    public static int getIntFromAlphaNumericString(String str) {
        str = UtilityClass.getInstance().nullChecker(str).trim();
        StringBuffer alpha = new StringBuffer(), num = new StringBuffer(), special = new StringBuffer();

        if (str != null && str.length() > 0) {
            for (int i = 0; i < str.length(); i++) {
                if (Character.isDigit(str.charAt(i)))
                    num.append(str.charAt(i));
                else if (Character.isAlphabetic(str.charAt(i)))
                    alpha.append(str.charAt(i));
                else
                    special.append(str.charAt(i));
            }

            return convertToInt2(num.toString());
        }
        return convertToInt(num.toString().trim().length() == 0 ? "0" : num.toString());
    }

    public static double getDoubleFromAlphaNumericString(String str) {
        str = UtilityClass.getInstance().nullChecker(str).trim();
        StringBuffer num = new StringBuffer();

        if (str != null && str.length() > 0) {
            for (int i = 0; i < str.length(); i++) {
                if (Character.isDigit(str.charAt(i)))
                    num.append(str.charAt(i));
                else if (Character.toString(str.charAt(i)).equalsIgnoreCase("."))
                    num.append(str.charAt(i));
            }

            return convertToDouble(num.toString());
        } else {
            return 0.0;
        }
    }

    public static double roundOffUpToDecimalPlaces(Double value, int decimalPlaces) {
        String formatter = "#.";

        if (decimalPlaces > 0) {
            for (int a = 0; a < decimalPlaces; a++) {
                formatter = formatter.concat("#");
            }

            String convertedValue = new DecimalFormat(formatter).format(value);
            value = convertToDouble(convertedValue);
        }

        return value;
    }

    public static double roundOffUpToDecimalPlaces(String text, int decimalPlaces) {
        text = UtilityClass.getInstance().nullChecker(text).trim();
        String formatter = "#.";
        Double value;
        try {
            value = Double.parseDouble(text);
        }catch (Exception e){
            value = 0.0;
        }


        if (decimalPlaces > 0) {
            for (int a = 0; a < decimalPlaces; a++) {
                formatter = formatter.concat("#");
            }

            String convertedValue = new DecimalFormat(formatter).format(value);
            value = convertToDouble(convertedValue);
        }

        return value;
    }

    public static double truncateDouble(int precision, double value){
        double scale = Math.pow(10, precision);
        return Math.round(value * scale) / scale;
    }

    public static String truncateDoubleString(int precision, double value){
        double scale = Math.pow(10, precision);
        return Double.toString(Math.round(value * scale) / scale);
    }

    public static double truncateDouble(int precision, String value){
        value = UtilityClass.getInstance().nullChecker(value).trim();
        if (UtilityClass.getInstance().nullChecker(value).isEmpty()){
            value = "0.0";
        }

        return truncateDouble(precision, Double.parseDouble(value));
    }

    public static String truncateDoubleString(int precision, String value){
        value = UtilityClass.getInstance().nullChecker(value).trim();
        return Double.toString(truncateDouble(precision, value));
    }



}
