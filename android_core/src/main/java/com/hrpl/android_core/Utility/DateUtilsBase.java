package com.hrpl.android_core.Utility;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;

import androidx.annotation.NonNull;

import com.hrpl.android_core.Listeners.ISC_String;
import com.hrpl.android_core.Listeners.ISC_StringDouble;
import com.hrpl.android_core.Listeners.ISC_TimeListener;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtilsBase {

    public DateUtilsBase() {
    }

    private static DateUtilsBase dateUtilsBase;

    public static DateUtilsBase getInstance() {
        if (dateUtilsBase == null) {
            dateUtilsBase = new DateUtilsBase();
        }
        return dateUtilsBase;
    }


    public static final String dateFormat = "dd-MM-yyyy";
    public static final String dateFormatApp = "yyyy-MM-dd";
    public static final String dateFormatServer = "MM/dd/yyyy";
    public static final String dateFormatServer2 = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String dateFormatServer3 = "MMM dd, yyyy";
    public static final String dateFormatServer4 = "dd MMM yyyy";
    public static final String dateFormatServer5 = "HH:mm";
    public static final String dateFormatServer8 = "HH:mm:ss";
    public static final String dateFormatServer6 = "hh:mm a";
    public static final String dateFormatServer7 = "EEEE, MMM dd, yyyy";
    public static final String dateFormatServer9 = "EEEE, dd MMMM @ HH:mm";
    public static final String dateFormatServer10 = "dd MMM";
    public static final String dateFormatServer11 = "hh:mm a, MMM dd, yyyy";

    public String formatDate(@NotNull String date, @NonNull String inputFormat, @NonNull String outputFormat) {
        if (UtilityClass.getInstance().nullChecker(inputFormat).isEmpty()){
            inputFormat = dateFormat;
        }

        if (UtilityClass.getInstance().nullChecker(outputFormat).isEmpty()){
            outputFormat = dateFormat;
        }

        try {
            if (UtilityClass.getInstance().nullChecker(date).isEmpty() || date.equalsIgnoreCase("1900-01-01T00:00:00")) {
                return "";
            } else {
                Date initDate = null;
                try {
                    initDate = new SimpleDateFormat(inputFormat).parse(date);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
                String parsedDate = formatter.format(initDate);

                return parsedDate;
            }
        } catch (Exception e) {
            return "";
        }
    }

    public String formatDate_UTC_To_IST(@NotNull String date, @NonNull String inputFormat, @NonNull String outputFormat) {
        if (UtilityClass.getInstance().nullChecker(inputFormat).isEmpty()){
            inputFormat = dateFormat;
        }

        if (UtilityClass.getInstance().nullChecker(outputFormat).isEmpty()){
            outputFormat = dateFormat;
        }

        try {
            if (UtilityClass.getInstance().nullChecker(date).isEmpty() || date.equalsIgnoreCase("1900-01-01T00:00:00")) {
                return "";
            } else {
                Date initDate = null;
                try {
                    SimpleDateFormat UTCFormater = new SimpleDateFormat(inputFormat);
                    UTCFormater.setTimeZone(TimeZone.getTimeZone("UTC"));
                    initDate = UTCFormater.parse(date);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
                formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                String parsedDate = formatter.format(initDate);

                return parsedDate;
            }
        } catch (Exception e) {
            return "";
        }
    }

    public String formatDate(Date date, @NonNull String outputFormat) {

        if (UtilityClass.getInstance().nullChecker(outputFormat).isEmpty()){
            outputFormat = dateFormat;
        }

        try {
            if (date == null) {
                return "";
            } else {
                SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
                String parsedDate = formatter.format(date);

                return parsedDate;
            }
        } catch (Exception e) {
            return "";
        }
    }

    public Date formatDate(String date, @NonNull String inputFormat) {
        if (UtilityClass.getInstance().nullChecker(inputFormat).isEmpty()){
            inputFormat = dateFormat;
        }

        try {
            if (UtilityClass.getInstance().nullChecker(date).isEmpty() || date.equalsIgnoreCase("1900-01-01T00:00:00")) {
                return new Date();
            } else {
                Date initDate = null;
                try {
                    initDate = new SimpleDateFormat(inputFormat).parse(date);
                } catch (Exception e) {
                    e.printStackTrace();
                    initDate = new Date();
                }
                return initDate;
            }
        } catch (Exception e) {
            return new Date();
        }
    }

    public String getDateNextToDayCurrentDate(int daysCount, String outputFormat) {
        Calendar nextDayCalender = Calendar.getInstance();
        nextDayCalender.setTime(new Date());
        nextDayCalender.add(Calendar.DATE, daysCount);

        return new SimpleDateFormat(outputFormat, Locale.UK).format((nextDayCalender.getTime()).getTime());
    }

    public void showDatePickerDialog(final Context context, final String dateFormat, final ISC_String callBack) {

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    public void showDatePickerDialogMinDt(final Context context, final String dateFormat, final ISC_String callBack) {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.show();
    }

    public void showDatePickerDialogMinDt(final Context context, final String dateFormat, String previousDate, String previousDateFormat, final ISC_String callBack) {

        final Calendar myCalendar = Calendar.getInstance();
        Date previousDt = formatDate(previousDate, previousDateFormat);
        myCalendar.setTime(previousDt);

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(previousDt.getTime());
        datePickerDialog.show();
    }

    public void showDatePickerDialogMinDtWithNext30Day(final Context context, final String dateFormat, final ISC_String callBack) {

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };

        Calendar next30DayCalender = Calendar.getInstance();
        next30DayCalender.setTime(new Date());
        next30DayCalender.add(Calendar.DATE, 30);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.getDatePicker().setMaxDate((next30DayCalender.getTime()).getTime());
        datePickerDialog.show();
    }

    @SuppressLint("SimpleDateFormat")
    public void showDatePickerDialogMinDtWithNext30Day(final Context context, final String dateFormat, String minmumDate, final ISC_String callBack) {

        final Calendar myCalendar = Calendar.getInstance();
        if (UtilityClass.getInstance().nullChecker(minmumDate).isEmpty()){
            minmumDate = getCurrentDate();
        }

        Date minimumDate = null;
        try {
            minimumDate = new SimpleDateFormat("dd-MM-yyyy").parse(minmumDate);
        } catch (ParseException e) {
            e.printStackTrace();
            minimumDate = new Date();
        }

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };

        Calendar next30DayCalender = Calendar.getInstance();
        next30DayCalender.setTime(new Date());
        next30DayCalender.add(Calendar.DATE, 30);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(minimumDate.getTime());
        datePickerDialog.getDatePicker().setMaxDate((next30DayCalender.getTime()).getTime());
        datePickerDialog.show();
    }

    @SuppressLint("SimpleDateFormat")
    public void showDatePickerDialogMinDt(final Context context, final String dateFormat, String minmumDate, final ISC_String callBack) {
        final Calendar myCalendar = Calendar.getInstance();
        if (UtilityClass.getInstance().nullChecker(minmumDate).isEmpty()){
            minmumDate = getCurrentDate();
        }

        Date minimumDate = null;
        try {
            minimumDate = new SimpleDateFormat("dd-MM-yyyy").parse(minmumDate);
        } catch (ParseException e) {
            e.printStackTrace();
            minimumDate = new Date();
        }

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };

        Calendar next30DayCalender = Calendar.getInstance();
        next30DayCalender.setTime(minimumDate);
        next30DayCalender.add(Calendar.DATE, 30);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(minimumDate.getTime());
        datePickerDialog.getDatePicker().setMaxDate((next30DayCalender.getTime()).getTime());
        datePickerDialog.show();
    }

    @SuppressLint("SimpleDateFormat")
    public void getDateforFirstDayCurrentMonth(Context context, ISC_String callback){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date currentFirstDay = calendar.getTime();
        callback.onClicked(new SimpleDateFormat(dateFormat).format(currentFirstDay));
    }

    @SuppressLint("SimpleDateFormat")
    public void getDateforLastDayCurrentMonth(Context context, ISC_String callback){

        Date minimumDate = null;
        try {
            minimumDate = new SimpleDateFormat("dd-MM-yyyy").parse(getCurrentDate());
        } catch (ParseException e) {
            e.printStackTrace();
            minimumDate = new Date();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date currentLastDay = calendar.getTime();
        callback.onClicked(new SimpleDateFormat(dateFormat).format(currentLastDay));
    }

    public void showDatePickerDialogDOB(final Context context, final String dateFormat, final ISC_String callBack) {

        final Calendar myCalendar = Calendar.getInstance();
//        myCalendar.add(Calendar.YEAR, -18);
        myCalendar.add(Calendar.YEAR, -0);
        myCalendar.set(Calendar.MONTH, myCalendar.get(Calendar.MONTH));
        myCalendar.set(Calendar.DAY_OF_MONTH, myCalendar.get(Calendar.DAY_OF_MONTH));
        myCalendar.set(Calendar.HOUR_OF_DAY, 0);
        myCalendar.set(Calendar.MINUTE, 0);
        myCalendar.set(Calendar.SECOND, 0);
        myCalendar.set(Calendar.MILLISECOND, 0);

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);
            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTime().getTime());
        datePickerDialog.show();
    }

    public void showDatePickerDialogDOB18YearsBack(final Context context, final String dateFormat, final ISC_String callBack) {

        final Calendar myCalendar = Calendar.getInstance();
        myCalendar.add(Calendar.YEAR, -18);
        myCalendar.set(Calendar.MONTH, myCalendar.get(Calendar.MONTH));
        myCalendar.set(Calendar.DAY_OF_MONTH, myCalendar.get(Calendar.DAY_OF_MONTH));
        myCalendar.set(Calendar.HOUR_OF_DAY, 0);
        myCalendar.set(Calendar.MINUTE, 0);
        myCalendar.set(Calendar.SECOND, 0);
        myCalendar.set(Calendar.MILLISECOND, 0);

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);
            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTime().getTime());
        datePickerDialog.show();
    }

    public void showDatePickerDialogMaxDateUpTo(final Context context, final String dateFormat, String maxDate, String maxDateFormat, final ISC_String callBack) {
        final Calendar myCalendar = Calendar.getInstance();

        Date maxDateValues = formatDate(maxDate, maxDateFormat);
        myCalendar.setTime(maxDateValues);

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);
            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(maxDateValues.getTime());
        datePickerDialog.show();
    }

    public void showDatePickerDialogDOB(final Context context, final String dateFormat, String previousDate, String previousDateFormat, final ISC_String callBack) {
        final Calendar myCalendar = Calendar.getInstance();
        Date previousDt = formatDate(previousDate, previousDateFormat);
        myCalendar.add(Calendar.YEAR, -18);
        myCalendar.set(Calendar.MONTH, myCalendar.get(Calendar.MONTH));
        myCalendar.set(Calendar.DAY_OF_MONTH, myCalendar.get(Calendar.DAY_OF_MONTH));
        myCalendar.set(Calendar.HOUR_OF_DAY, 0);
        myCalendar.set(Calendar.MINUTE, 0);
        myCalendar.set(Calendar.SECOND, 0);
        myCalendar.set(Calendar.MILLISECOND, 0);

        myCalendar.setTime(previousDt);

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTime().getTime());
        datePickerDialog.show();
    }

    public void showDatePickerDialogMaxDt(final Context context, final String dateFormat, final ISC_String callBack) {

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    public void showDatePickerDialogMaxDt(final Context context, final String dateFormat, String previousDate, String previousDateFormat, final ISC_String callBack) {
        final Calendar myCalendar = Calendar.getInstance();
        Date previousDt = formatDate(previousDate, previousDateFormat);
        myCalendar.setTime(previousDt);

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(dateFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    public void showTimePickerDialog(final Context context, final ISC_StringDouble callBack) {

        Calendar calendar = Calendar.getInstance();
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> callBack.onclick(changeFormat(hourOfDay), changeFormat(minute)), hours, minutes, true);

        timePickerDialog.show();
    }

    public void showTimePickerWithSecondsDialog(final Context context, final ISC_TimeListener callBack) {

        Calendar now = Calendar.getInstance();
        MyTimePickerDialog mTimePicker = new MyTimePickerDialog(context, new MyTimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute, int seconds) {
                callBack.onclick(changeFormat(hourOfDay), changeFormat(minute), changeFormat(seconds));
            }
        }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), now.get(Calendar.SECOND), true);
        mTimePicker.show();
    }

    public void showDatePickerDialogMinDtWithDayCount(final Context context, int daysCount, String outputFormat, final ISC_String callBack) {

        final Calendar myCalendar = Calendar.getInstance();

        Calendar nextDayCalender = Calendar.getInstance();
        nextDayCalender.setTime(new Date());
        nextDayCalender.add(Calendar.DATE, daysCount);

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            myCalendar.set(Calendar.HOUR_OF_DAY, 0);
            myCalendar.set(Calendar.MINUTE, 0);
            myCalendar.set(Calendar.SECOND, 0);
            myCalendar.set(Calendar.MILLISECOND, 0);

            String dateString = new SimpleDateFormat(outputFormat, Locale.UK).format(myCalendar.getTime());
            callBack.onClicked(dateString);

        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate((nextDayCalender.getTime()).getTime());
        datePickerDialog.show();
    }

    public String changeFormat(int values){
        if (values < 10){
            return  "0" + values;
        }else {
            return String.valueOf(values);
        }
    }

    public String getDatePreviousToYear(int previousYearCount, @NonNull String outputFormat) {
        Calendar nextDayCalender = Calendar.getInstance();
        nextDayCalender.setTime(new Date());
        nextDayCalender.add(Calendar.YEAR, -previousYearCount);

        return new SimpleDateFormat(outputFormat, Locale.UK).format((nextDayCalender.getTime()).getTime());
    }

    public String getCurrentDate() {
        return new SimpleDateFormat(dateFormat, Locale.UK).format(new Date());
    }

    public String getCurrentDate(String outputFormat) {
        return formatDate(new Date(), outputFormat);
    }

    public String getCurrentTime() {
        return new SimpleDateFormat(dateFormatServer5, Locale.UK).format(new Date());
    }

    public Date getCurrentTimeDateObj() {
        return formatDate(new SimpleDateFormat(dateFormatServer5, Locale.UK).format(new Date()), dateFormatServer5);
    }

    public void showTimePickerDialog(final Context context, boolean is24Hours, final ISC_StringDouble callBack) {

        Calendar calendar = Calendar.getInstance();
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> {

            String hoursStr = "";
            String minStr = "";

            if (hourOfDay < 10) {
                hoursStr = "0" + hourOfDay;
            } else {
                hoursStr = hourOfDay+"";
            }

            if (minute < 10) {
                minStr = "0" + minute;
            } else {
                minStr = minute+"";
            }

            callBack.onclick(UtilityClass.getInstance().nullChecker(hoursStr), UtilityClass.getInstance().nullChecker(minStr));
        }, hours, minutes, is24Hours);

        timePickerDialog.show();
    }

    public void showTimePickerDialog2(final Context context, final ISC_StringDouble callBack) {

//        Calendar calendar = Calendar.getInstance();
//        int hours = calendar.get(Calendar.HOUR_OF_DAY);
//        int minutes = calendar.get(Calendar.MINUTE);
        int hours = 7;
        int minutes = 30;

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> {

            String hoursStr = "";
            String minStr = "";

            if (hourOfDay < 10) {
                hoursStr = "0" + hourOfDay;
            } else {
                hoursStr = hourOfDay+"";
            }

            if (minute < 10) {
                minStr = "0" + minute;
            } else {
                minStr = minute+"";
            }

            callBack.onclick(UtilityClass.getInstance().nullChecker(hoursStr), UtilityClass.getInstance().nullChecker(minStr));
        }, hours, minutes, false);

        timePickerDialog.show();
    }

    public String projectDateFormat(String date) {
       return formatDate(date,"yyyy-MM-dd","dd-MMM-yyyy");
    }
}
