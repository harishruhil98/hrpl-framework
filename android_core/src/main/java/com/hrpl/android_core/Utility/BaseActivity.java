package com.hrpl.android_core.Utility;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.app.LocaleChangerAppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.textfield.TextInputLayout;
import com.hrpl.android_core.BaseApplicationClass;
import com.hrpl.android_core.CustomViews.CustomAlertDialog;
import com.hrpl.android_core.CustomViews.CustomSnackBar;
import com.hrpl.android_core.Listeners.WidthHeightChangeListener;
import com.hrpl.android_core.LocaleChanger.LocaleChanger;
import com.hrpl.android_core.NetworkCalls.NetworkReceiver;
import com.hrpl.android_core.R;
import com.hrpl.android_core.Utility.PushDownAnimation.ClickEvent;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;

import static android.os.Build.VERSION.SDK_INT;
import static com.hrpl.android_core.Utility.ConstantClass.REQUEST_CAMERA;
import static com.hrpl.android_core.Utility.ConstantClass.REQUEST_ExternalStorage;

public abstract class BaseActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, NetworkReceiver.NetworkChangeListener, CustomAlertDialog.InternetConnectivityListener, ClickEvent.onClickEnabledView {

    protected Context context = BaseActivity.this;
    protected Activity activity = BaseActivity.this;
    private ProgressDialog progressDialog;

    private NetworkReceiver mNetworkReceiver;
    private androidx.appcompat.app.AlertDialog alertDialog;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String tempPhoneNo = "";

    private boolean doubleBackToExitPressedOnce = false;
    private PermissionListener _permissionListener;

    /*private LocaleChangerAppCompatDelegate localeChangerAppCompatDelegate;*/

    protected abstract Context setContext();

    public void onNetworkConfigurationReloaded() {
    }

   /* @NonNull
    @Override
    public AppCompatDelegate getDelegate() {
        if (localeChangerAppCompatDelegate == null) {
            localeChangerAppCompatDelegate = new LocaleChangerAppCompatDelegate(super.getDelegate());
        }
        return localeChangerAppCompatDelegate;
    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialization();
    }

    protected void initialization() {
        alertDialog = CustomAlertDialog.showDialogForInternetConnectivity(context, this);
        setContentView(setContentView());
        context = setContext();

        activity = (Activity) setContext();

        try {
            getSupportActionBar().hide();
        }catch (Exception e){}

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(false);

        try {
            swipeRefreshLayout = findViewById(setSwipeRefreshView());
            swipeRefreshLayout.setOnRefreshListener(this);
        } catch (Exception e) {

        }

        init();
        bindClickListener();

        if (!NetworkReceiver.isConnected(context)) {
            try {
                alertDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected abstract void init();

    public int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public int getScreenDensityDPI() {
        return Resources.getSystem().getDisplayMetrics().densityDpi;
    }

    public void getViewWidthHeight(View view, BaseFragment.WidthHeightListener listener) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                listener.onMeasure(view.getMeasuredWidth(), view.getMeasuredHeight());
            }
        });
    }

    public interface WidthHeightListener {
        void onMeasure(int width, int height);
    }

    public interface CustomBackPressedListener {
        void onCustomBackPressed(View view);
    }

    protected abstract int setContentView();

    protected abstract int setSwipeRefreshView();

    public void showNoInternetDialog() {
        alertDialog.show();
    }

    public void showSnackBar(String error, boolean isWarning) {
        if (!((Activity) context).isFinishing() && !UtilityClass.getInstance().nullChecker(error).trim().isEmpty()) {
            CustomSnackBar.getInstance().showSnackToast(this, error, !isWarning, isWarning);
        }
    }

    public void showSnackBar(String msg) {
        if (!((Activity) context).isFinishing() && !UtilityClass.getInstance().nullChecker(msg).trim().isEmpty()) {
            CustomSnackBar.getInstance().showSnackToast(this, msg);
        }
    }

    protected abstract void bindClickListener();

    public void setClickEvent(View view) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, 3f, this);
    }

    public void setClickEvent(View view, boolean enableDelay) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, enableDelay, 3f, this);
    }

    public void setClickEvent(View view, onClickedEvent onClickedEventListener) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, 3f, onClickedEventListener::onClicked);
    }

    public void setClickEventWithoutScale(View view) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, 0f, this);
    }

    public void setClickEvent(View view, int scale) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, scale, this);
    }

    public void setClickEvent(View view, int scale, onClickedEvent onClickedEventListener) {
        BaseApplicationClass.getClickEventClient().setClickListener(view, scale, onClickedEventListener::onClicked);
    }

    @Override
    protected void onResume() {

        if (alertDialog == null) {
            alertDialog = CustomAlertDialog.showDialogForInternetConnectivity(context, this);
        }

        if (mNetworkReceiver == null) {
            mNetworkReceiver = new NetworkReceiver(this);
        }
        registerNetworkBroadcastForNougat();
        super.onResume();
    }

    public void onDoubleBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(context, context.getResources().getString(R.string.back_again_to_exit_app), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    public void onDoubleBackPressed(onDoubleClickedListener clickedListener) {
        if (doubleBackToExitPressedOnce) {
            clickedListener.onPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(context, context.getResources().getString(R.string.back_again_to_exit_app), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    public void showExitPopup() {
        CustomAlertDialog.showAlertMessageDialog(context, getString(R.string.are_you_sure_want_to_exit), CustomAlertDialog.AlertType.Error, CustomAlertDialog.ButtonType.CancelWithConfirm, new CustomAlertDialog.AlertDialogListener() {
            @Override
            public void onConfirm() {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }

            @Override
            public void onCancel() {

            }
        });
    }

    public void showExitPopup(onDoubleClickedListener clickedListener) {
        CustomAlertDialog.showAlertMessageDialog(context, getString(R.string.are_you_sure_want_to_exit), CustomAlertDialog.AlertType.Error, CustomAlertDialog.ButtonType.CancelWithConfirm, new CustomAlertDialog.AlertDialogListener() {
            @Override
            public void onConfirm() {
                clickedListener.onPressed();
            }

            @Override
            public void onCancel() {

            }
        });
    }

    public void onDoubleBackPressedWithPopup() {
        if (doubleBackToExitPressedOnce) {
            showExitPopup();
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(context, context.getResources().getString(R.string.back_again_to_exit_app), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    public void onDoubleBackPressedWithPopup(onDoubleClickedListener clickedListener) {
        if (doubleBackToExitPressedOnce) {
            showExitPopup(clickedListener);
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(context, context.getResources().getString(R.string.back_again_to_exit_app), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    public void sendLocationToMaps(String location) {

        String uri = "http://maps.google.co.in/maps?q=" + location;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(intent);
    }

    public void sendLocationToMaps(double latitude, double longitude) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (Exception e) {

        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isFinishing()) {
            if (!isConnected) {
                alertDialog.show();
            } else {
                alertDialog.dismiss();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReloadClick() {
        if (!isFinishing()) {
            if (NetworkReceiver.isConnected(context)) {
                alertDialog.dismiss();
                initialization();
            }
        }
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            context.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION), RECEIVER_EXPORTED);
        }else {
            context.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == ConstantClass.REQUEST_PhoneCall && !UtilityClass.getInstance().nullChecker(tempPhoneNo).isEmpty() && isCallingPermissionGranted()) {
            callPhoneNO(tempPhoneNo);
        } else if (requestCode == ConstantClass.REQUEST_PhoneCall && !isCallingPermissionGranted()) {
           /* CustomSnackBar.getInstance().showSnackBar(activity, "Please allow calling permission to call on " + tempPhoneNo, "Allow here", Snackbar.LENGTH_LONG, new CustomSnackBar.onCLickListener() {
                @Override
                public void onCLick() {
                    callPhoneNO(tempPhoneNo);
                }
            }, context.getResources().getColor(R.color.colorWhite));*/
            showSnackBar("Calling permission denied.");
        } else if (requestCode == ConstantClass.REQUEST_ExternalStorage && _permissionListener != null && isExternalStoragePermissionGranted()) {
            _permissionListener.onPermissionGranted(ConstantClass.REQUEST_ExternalStorage);
        } else if (requestCode == ConstantClass.REQUEST_ExternalStorage && _permissionListener != null && !isExternalStoragePermissionGranted()) {
            _permissionListener.onPermissionDenied(ConstantClass.REQUEST_ExternalStorage);
        } else if (requestCode == ConstantClass.REQUEST_CAMERA && _permissionListener != null && isCameraPermissionGranted()) {
            _permissionListener.onPermissionGranted(ConstantClass.REQUEST_CAMERA);
        } else if (requestCode == ConstantClass.REQUEST_CAMERA && _permissionListener != null && !isCameraPermissionGranted()) {
            _permissionListener.onPermissionDenied(ConstantClass.REQUEST_CAMERA);
        } else {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    protected void onPermissionGranted() {
    }

    protected void onPermissionGranted(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    }

    protected void requestStoragePermissions(PermissionListener permissionListener) {
        _permissionListener = permissionListener;
        requestInternalStoragePermissions(_permissionListener);
    }

    private void requestInternalStoragePermissions(PermissionListener permissionListener) {
        if (SDK_INT >= Build.VERSION_CODES.R && !isExternalStoragePermissionGranted()) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s", getApplicationContext().getPackageName())));
                startActivityForResult(intent, REQUEST_ExternalStorage);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, REQUEST_ExternalStorage);
            }
            permissionListener.onPermissionAndroid11(REQUEST_ExternalStorage);
        } else if (SDK_INT < Build.VERSION_CODES.R && !isExternalStoragePermissionGranted()) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_ExternalStorage);
        } else if (isExternalStoragePermissionGranted()) {
            permissionListener.onPermissionGranted(REQUEST_ExternalStorage);
        }
    }

    protected void requestCameraPermissions(PermissionListener permissionListener) {
        _permissionListener = permissionListener;
        requestInternalCameraPermissions(_permissionListener);
    }

    private void requestInternalCameraPermissions(PermissionListener permissionListener) {
        if (!isCameraPermissionGranted()) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        } else if (isCameraPermissionGranted()) {
            permissionListener.onPermissionGranted(REQUEST_CAMERA);
        }
    }

    private boolean isExternalStoragePermissionGranted() {
        boolean result = true;

        if (SDK_INT >= Build.VERSION_CODES.R && !Environment.isExternalStorageManager()) {
            result = false;
        } else if (SDK_INT < Build.VERSION_CODES.R && ContextCompat.checkSelfPermission((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            result = false;
        }
        return result;
    }

    private boolean isCallingPermissionGranted() {
        boolean result = true;

        if (context.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            result = false;
        }
        return result;
    }

    private boolean isCameraPermissionGranted() {
        boolean result = true;

        if (context.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            result = false;
        }
        return result;
    }

    public interface PermissionListener {
        void onPermissionGranted(int requestCode);

        void onPermissionDenied(int requestCode);

        void onPermissionAndroid11(int requestCode);

        void onError(int requestCode, String error);
    }

    @Override
    public void onClickEnabled(View v) {
        onClicked(v);
    }

    protected abstract void onClicked(View v);

    public void hideProgressDialog() {
        if (!((Activity) context).isFinishing() && progressDialog != null) {
            progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
            progressDialog.dismiss();
        }
    }

    public void showProgressDialog() {
        if (!((Activity) context).isFinishing() && progressDialog != null) {
            progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
            progressDialog.show();
        }
    }

    public void setProgressDialogMessage(@NonNull String message) {
        if (!((Activity) context).isFinishing() && progressDialog != null && UtilityClass.getInstance().nullChecker(message).length() > 0) {
            progressDialog.setMessage(UtilityClass.getInstance().nullChecker(message));
        }
    }

    public void shareApp(@NonNull String BuildConfig_APPLICATION_ID) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig_APPLICATION_ID);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void gotoPlayStore(@NonNull String BuildConfig_APPLICATION_ID) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig_APPLICATION_ID)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig_APPLICATION_ID)));
        }
    }

    public void shareApp(String titleMsg, @NonNull String appName, @NonNull String BuildConfig_APPLICATION_ID) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_SUBJECT, appName);
        intent.putExtra(Intent.EXTRA_TEXT, UtilityClass.getInstance().nullChecker(titleMsg) + " https://play.google.com/store/apps/details?id=" + BuildConfig_APPLICATION_ID);
        intent.setType("text/plain");
        context.startActivity(intent);
    }

    public void callPhoneNO(String phoneNo) {
        if (!UtilityClass.getInstance().nullChecker(phoneNo).trim().isEmpty()) {
            String uri = "tel:" + phoneNo.trim();
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            if (isCallingPermissionGranted()) {
                context.startActivity(intent);
            } else {
                tempPhoneNo = phoneNo;
                ActivityCompat.requestPermissions(((Activity) context), new String[]{Manifest.permission.CALL_PHONE}, ConstantClass.REQUEST_PhoneCall);
            }
        }
    }

    public void sendEmail(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        onSwipeRefreshView();
    }

    protected abstract void onSwipeRefreshView();

    public interface onClickedEvent {
        void onClicked(View view);
    }

    public synchronized String removeDuplicateWords(String sText) {
        String[] strWords = sText.split("\\s+");

        //convert String array to LinkedHashSet to remove duplicates
        LinkedHashSet<String> lhSetWords = new LinkedHashSet<String>(Arrays.asList(strWords));

        //join the words again by space
        StringBuilder sbTemp = new StringBuilder();
        int index = 0;

        for (String s : lhSetWords) {

            if (index > 0)
                sbTemp.append(" ");

            sbTemp.append(s);
            index++;
        }

        sText = sbTemp.toString();

        return sText;
    }

    public boolean validEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
        String emailPatternnew = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+\\.+[a-zA-Z]+";

        if (!UtilityClass.getInstance().nullChecker(email).isEmpty() && email.contains("@") && email.matches(emailPattern)) {
            String domain = email.substring(email.indexOf('@'), email.length());
            String last = domain.substring(domain.indexOf('.'), domain.length());
            if (email.matches(emailPattern) && (last.length() == 3 || last.length() == 4)) { // check @domain.nl or @domain.com or @domain.org
                return true;
            } else if (email.matches(emailPatternnew) && last.length() == 6 && email.charAt(email.length() - 3) == '.') { //check for @domain.co.in or @domain.co.uk
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String getText(TextView textView) {
        if (textView != null) {
            return textView.getText().toString();
        } else {
            return "";
        }
    }

    public String getText(TextInputLayout textInputLayout) {
        return getText(textInputLayout.getEditText());
    }

    public String getText(EditText editText) {
        if (editText != null) {
            return editText.getText().toString();
        } else {
            return "";
        }
    }

    public void setText(TextView textView, String text) {
        if (textView != null) {
            textView.setText(text);
        }
    }

    public void setText(EditText editText, String text) {
        if (editText != null) {
            editText.setText(text);
        }
    }

    public void setText(TextInputLayout textInputLayout, String text) {
        if (textInputLayout != null) {
            setText(textInputLayout.getEditText(), text);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        if (BaseApplicationClass.getLibSharedPrefClient().getUserJsonViewerEnabled()) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_VOLUME_UP:
                    if (action == KeyEvent.ACTION_UP && BaseApplicationClass.getLibSharedPrefClient().getUserJsonViewerEnabled()) {
                        BaseApplicationClass.getLibSharedPrefClient().setJSONViewerDisplayStatus(true);
                        Toast.makeText(context, "JSON Viewer enabled", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    if (action == KeyEvent.ACTION_DOWN && BaseApplicationClass.getLibSharedPrefClient().getUserJsonViewerEnabled()) {
                        BaseApplicationClass.getLibSharedPrefClient().setJSONViewerDisplayStatus(false);
                        Toast.makeText(context, "JSON Viewer disabled.", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                default:
                    return super.dispatchKeyEvent(event);
            }
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

    public void getViewWidth(View view, WidthHeightChangeListener listener) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                listener.onMeasure(view.getMeasuredWidth(), view.getMeasuredHeight());
            }
        });
    }

    public interface onDoubleClickedListener {
        void onPressed();
    }


  /*  public void showDialogForLanguageChange(LanguageChangeListener languageChangeListener) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
        View view = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_dialog_change_language, null, false);

        TextView tvTitle = view.findViewById(R.id.CustomChangeLanguage_tvTitle);

        LinearLayout llEngLayout = view.findViewById(R.id.CustomChangeLanguage_llEngLayout);
        RadioButton rbEng = view.findViewById(R.id.CustomChangeLanguage_rbEng);
        ImageView ivEng = view.findViewById(R.id.CustomChangeLanguage_ivEng);

        LinearLayout llHindiLayout = view.findViewById(R.id.CustomChangeLanguage_llHindiLayout);
        RadioButton rbHindi = view.findViewById(R.id.CustomChangeLanguage_rbHindi);
        ImageView ivHindi = view.findViewById(R.id.CustomChangeLanguage_ivHindi);

        BaseApplicationClass.getClickEventClient().setClickListener(llEngLayout, v -> {
            llEngLayout.setBackground(activity.getResources().getDrawable(R.drawable.border_primary_round));
            rbEng.setChecked(true);
            rbEng.setButtonTintList(activity.getResources().getColorStateList(R.color.colorPrimary));
            rbEng.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            ivEng.setImageResource(R.drawable.img_icon_eng_selected);

            llHindiLayout.setBackground(activity.getResources().getDrawable(R.drawable.border_round_grey));
            rbHindi.setChecked(false);
            rbHindi.setButtonTintList(activity.getResources().getColorStateList(R.color.colorGray2));
            rbHindi.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            ivHindi.setImageResource(R.drawable.img_icon_hindi_unselected);
        });

        BaseApplicationClass.getClickEventClient().setClickListener(llHindiLayout, v -> {
            llEngLayout.setBackground(activity.getResources().getDrawable(R.drawable.border_round_grey));
            rbEng.setChecked(false);
            rbEng.setButtonTintList(activity.getResources().getColorStateList(R.color.colorGray2));
            rbEng.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            ivEng.setImageResource(R.drawable.img_icon_eng_unselected);

            llHindiLayout.setBackground(activity.getResources().getDrawable(R.drawable.border_primary_round));
            rbHindi.setChecked(true);
            rbHindi.setButtonTintList(activity.getResources().getColorStateList(R.color.colorPrimary));
            rbHindi.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
            ivHindi.setImageResource(R.drawable.img_icon_hindi_selected);
        });

        Locale locale = activity.getResources().getConfiguration().getLocales().get(0);
        String languageCode = locale.getLanguage();

        if (languageCode.equalsIgnoreCase("hi")) {
            llHindiLayout.performClick();
        } else {
            llEngLayout.performClick();
        }


        TextView tvConfirm = view.findViewById(R.id.CustomChangeLanguage_tvConfirmLayout);
        TextView tvDismiss = view.findViewById(R.id.CustomChangeLanguage_tvDismissLayout);

        tvTitle.setText(activity.getResources().getString(R.string.select_language));

        builder.setView(view);

        builder.setCancelable(false);
        final androidx.appcompat.app.AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationUpToDown;

        alertDialog.show();

        Rect displayRectangle = new Rect();
        Window window = activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        int width = (int) (displayRectangle.width() * 0.95);

        alertDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        *//*ArrayList<LanguageModel> modelArrayList = new ArrayList<>();
        modelArrayList.add(new LanguageModel(1, "English", "en", !languageCode.equalsIgnoreCase("hi")));
        modelArrayList.add(new LanguageModel(2, "हिन्दी", "hi", languageCode.equalsIgnoreCase("hi")));

        final LanguageModel[] areaModel = {modelArrayList.get(0)};

        LanguageListAdapter adapetr = new LanguageListAdapter(activity, modelArrayList, (areaModel1, Position) -> {
            areaModel[0] = areaModel1;
        });*//*


        BaseApplicationClass.getClickEventClient().setClickListener(tvDismiss, v -> {
            alertDialog.dismiss();
            languageChangeListener.onDismiss();
        });
        BaseApplicationClass.getClickEventClient().setClickListener(tvConfirm, v -> {
            alertDialog.dismiss();

            LocaleChanger.setLocale(BaseApplicationClass.SUPPORTED_LOCALES.get(rbEng.isChecked() ? 0 : 1));

            languageChangeListener.onLanguageChanged(rbEng.isChecked() ? "en" : "hi");
        });
    }

    public interface LanguageChangeListener {
        void onLanguageChanged(String lngCode);

        void onDismiss();
    }*/
}
