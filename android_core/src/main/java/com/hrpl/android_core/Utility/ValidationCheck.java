package com.hrpl.android_core.Utility;

import android.content.Context;
import android.widget.EditText;

import com.hrpl.android_core.R;

public class ValidationCheck {

    public static final int Name = 1;
    public static final int Address = 3;
    public static final int PhoneNo = 4;
    public static final int Email = 8;
    public static final int Password = 19;


    public static final int passMaxLength = 20;
    public static final int passMinLength = 8;


    //returns true when data is not good and return false when data is good

    public static boolean addressCheck(Context context, EditText editText, String errorMsg) {
        boolean responce = Check(context, editText, ValidationCheck.Address);
        if (responce) {
            editText.setError(errorMsg);
            editText.requestFocus();
            return true;

        } else {
            return false;
        }

    }

    public static boolean Check(Context context, EditText editText, int validationType) {
        boolean response = false;
        String dataString = editText.getText().toString().trim();
        switch (validationType) {

            case Name: {
                if (dataString.length() < 2 || !dataString.matches(ConstantClass.regexValidName)) {
                    editText.setError(context.getResources().getString(R.string.name_error));
                    editText.requestFocus();
                    response = true;
                } else {
                    editText.setError(null);
                }
                break;
            }

            case Address: {
//                if (dataString.length() < 2 || !dataString.matches(ConstantClass.regexValidAddress)) {
                if (dataString.isEmpty()) {
                    editText.setError(context.getResources().getString(R.string.address_error));
                    editText.requestFocus();
                    response = true;
                } else {
                    editText.setError(null);
                }
                break;
            }

            case PhoneNo: {
//                if (dataString.length() < 10 || !dataString.matches(ConstantClass.regexValidMobileNo)) {
                if (dataString.length() < 10) {
                    editText.setError(context.getResources().getString(R.string.phone_error));
                    editText.requestFocus();
                    response = true;
                } else {
                    editText.setError(null);
                }
                break;
            }

            case Email: {
                if (dataString.length() < 5 || !validEmail(dataString.toLowerCase())) {
                    editText.setError(context.getResources().getString(R.string.email_error));
                    editText.requestFocus();
                    response = true;
                } else {
                    editText.setError(null);
                }
                break;
            }

            case Password: {
                if (dataString.length() < passMinLength || !dataString.matches(ConstantClass.regexValidAlphaNumericOnly)) {
                    editText.setError(context.getResources().getString(R.string.password_error));
                    editText.requestFocus();
                    response = true;
                } else {
                    editText.setError(null);
                }
                break;
            }

        }

        return response;
    }

    protected static boolean validEmail(String email) {
        // TODO Auto-generated method stub

        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+";
        String emailPatternnew = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-zA-Z]+\\.+[a-zA-Z]+";

        if (!UtilityClass.getInstance().nullChecker(email).isEmpty() && email.contains("@") && email.matches(emailPattern)) {
            String domain = email.substring(email.indexOf('@'), email.length());
            String last = domain.substring(domain.indexOf('.'), domain.length());
            if (email.matches(emailPattern) && (last.length() == 3 || last.length() == 4)) { // check @domain.nl or @domain.com or @domain.org
                return true;
            } else if (email.matches(emailPatternnew) && last.length() == 6 && email.charAt(email.length() - 3) == '.') { //check for @domain.co.in or @domain.co.uk
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean Check(EditText editText, String errorMsg) {
        boolean response = false;
        String dataString = editText.getText().toString();

        if (dataString.isEmpty()) {
            editText.setError(errorMsg);
            editText.requestFocus();
            response = true;
            editText.requestFocus();
        }
        return response;
    }

    public interface getString {
        void onReturnString(String HNo, String StreetAdd, String City, String Province, String ProvinceCode);
    }

    public static void checkPhoneNo(Context context, EditText etPhoneNO, EditText nextView) {
        nextView.setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus && etPhoneNO.getText().toString().trim().length() > 0) {
                Check(context, etPhoneNO, PhoneNo);
            }
        });
    }


}
